package by.epam.javatr.check;

import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class CheckValidTest {

    @Test
    public void checkForPositiveValues_Should_Be_True() throws NotPositiveNumberException {
        double x = 1.1;
        boolean actual = CheckValid.checkForPositiveValues(x);
        Assert.assertTrue(actual);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void checkForPositiveValues_Not_Positive_Value() throws NotPositiveNumberException {
        double x = -1.1;
        boolean actual = CheckValid.checkForPositiveValues(x);
        Assert.assertFalse(actual);
    }

    @Test
    public void checkNotNull_Should_Be_True() throws NullReferenceException {
        Object x = 1.1;
        boolean actual = CheckValid.checkNotNull(x);
        Assert.assertTrue(actual);
    }

    @Test(expected = NullReferenceException.class)
    public void checkNotNull_Null_Reference() throws NullReferenceException {
        Object x = null;
        boolean actual = CheckValid.checkNotNull(x);
        Assert.assertFalse(actual);
    }

    @Test
    public void checkIllegalValues_Should_Be_True() throws IllegalValueException {
        int a = 2;
        int b = 1;
        boolean actual = CheckValid.checkIllegalValues(a, b);
        Assert.assertTrue(actual);
    }

    @Test(expected = IllegalValueException.class)
    public void checkIllegalValues_Illegal_Value() throws IllegalValueException {
        int a = 1;
        int b = 2;
        boolean actual = CheckValid.checkIllegalValues(a, b);
        Assert.assertFalse(actual);
    }

    @Test
    public void checkIllegalValues1_Should_Be_True() throws IllegalValueException {
        double a = 2.1;
        double b = 1.1;
        boolean actual = CheckValid.checkIllegalValues(a, b);
        Assert.assertTrue(actual);
    }

    @Test(expected = IllegalValueException.class)
    public void checkIllegalValues1_Illegal_Value() throws IllegalValueException {
        double a = 1.1;
        double b = 2.1;
        boolean actual = CheckValid.checkIllegalValues(a, b);
        Assert.assertFalse(actual);
    }
}