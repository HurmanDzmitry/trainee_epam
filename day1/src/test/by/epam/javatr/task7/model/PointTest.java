package by.epam.javatr.task7.model;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class PointTest {

    @Test
    public void setPoint_Should_Be_Valid() throws NullReferenceException {
        Point expected = new Point(1, 1);
        Point actual = new Point();
        actual.setX(1);
        actual.setY(1);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullReferenceException.class)
    public void setPoint_Should_Null_Value() throws NullReferenceException {
        Point expected = new Point(1, 1);
        Point actual = new Point();
        Integer x = null;
        CheckValid.checkNotNull(x);
        actual.setX(1);
        actual.setY(x);
        Assert.assertEquals(expected, actual);
    }
}