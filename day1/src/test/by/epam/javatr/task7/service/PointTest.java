package by.epam.javatr.task7.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task7.model.Point;
import org.junit.Assert;
import org.junit.Test;

public class PointTest {

    @Test
    public void nearPoint_Should_Be_Valid() throws NullReferenceException {
        Point expected = new Point(1, 1);
        Point point = new Point(0, -5);
        Point actual = PointParameters.nearPoint(expected, point);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullReferenceException.class)
    public void nearPoint_Null_Value() throws NullReferenceException {
        Point expected = new Point(1, 1);
        Integer x = null;
        CheckValid.checkNotNull(x);
        Point b = new Point(x, -5);
        Point actual = PointParameters.nearPoint(expected, b);
        Assert.assertEquals(expected, actual);
    }
}