package by.epam.javatr.task8.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task8.service.Functions;
import org.junit.Assert;
import org.junit.Test;

public class FunctionsTest {

    @Test
    public void function_Should_Be_Valid_More3() throws NullReferenceException {
        double expected = 9;
        double x = 3;
        double actual = Functions.function(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test
    public void function_Should_Be_Valid_Less3() throws NullReferenceException {
        double expected = 0.5;
        double x = 2;
        double actual = Functions.function(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NullReferenceException.class)
    public void function_Null_Reference() throws NullReferenceException {
        double expected = 0;
        Double x = null;
        CheckValid.checkNotNull(x);
        double actual = Functions.function(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = ArithmeticException.class)
    public void function_Infinity_Possibility() throws NullReferenceException {
        double expected = 0;
        double x = Math.pow(6, 1.0 / 3.0);
        double actual = Functions.function(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }
}