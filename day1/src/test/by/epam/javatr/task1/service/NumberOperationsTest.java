package by.epam.javatr.task1.service;

import org.junit.Assert;
import org.junit.Test;

public class NumberOperationsTest {

    @Test
    public void getLastDigitOfNumberPowerOf2_Positive_Numbers_And_Zero() {
        int expected = 0;
        int x = 210;
        int actual = NumberOperations.getLastDigitOfNumberPowerOf2(x);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullPointerException.class)
    public void getLastDigitOfNumberPowerOf2_Null_Reference() {
        int expected = 0;
        Integer i = null;
        int x = i;
        int actual = NumberOperations.getLastDigitOfNumberPowerOf2(x);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getLastDigitOfNumberPowerOf2_Negative_Numbers() {
        int expected = 1;
        int x = -211;
        int actual = NumberOperations.getLastDigitOfNumberPowerOf2(x);
        Assert.assertEquals(expected, actual);
    }
}