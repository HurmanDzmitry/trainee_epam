package by.epam.javatr.task2.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class TimeTest {

    @Test
    public void numberDaysOfMonth_Normal_Values() throws IllegalValueException, NotPositiveNumberException, NullReferenceException {
        int expected = 29;
        int year = 2020;
        int month = 2;
        int actual = Time.numberDaysOfMonth(year, month);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalValueException.class)
    public void numberDaysOfMonth_Illegal_Values() throws IllegalValueException, NotPositiveNumberException, NullReferenceException {
        int expected = -32;
        int year = 2019;
        int month = 13;
        int actual = Time.numberDaysOfMonth(year, month);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void numberDaysOfMonth_Negative_Values() throws IllegalValueException, NotPositiveNumberException, NullReferenceException {
        int expected = 29;
        int year = 2018;
        int month = -2;
        int actual = Time.numberDaysOfMonth(year, month);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullReferenceException.class)
    public void numberDaysOfMonth_Null_Reference() throws IllegalValueException, NotPositiveNumberException, NullReferenceException {
        int expected = 0;
        Integer year = null;
//        int year = i;
//        int x = Integer.parseDouble(null);
        int month = 0;
        CheckValid.checkNotNull(year);
        CheckValid.checkNotNull(month);
        int actual = Time.numberDaysOfMonth(year, month);
        Assert.assertEquals(expected, actual);
    }
}