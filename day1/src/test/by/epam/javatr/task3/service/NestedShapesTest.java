package by.epam.javatr.task3.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class NestedShapesTest {

    @Test
    public void areaSquareNestedInCircle_Normal_Values() throws NullReferenceException, NotPositiveNumberException {
        double expected = 15;
        double x = 30;
        double actual = NestedShapes.areaSquareNestedInCircle(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void areaSquareNestedInCircle_Negative_Values() throws NullReferenceException, NotPositiveNumberException {
        double expected = 0;
        double x = -30;
        double actual = NestedShapes.areaSquareNestedInCircle(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NullReferenceException.class)
    public void areaSquareNestedInCircle_Null_Reference() throws NullReferenceException, NotPositiveNumberException {
        double expected = 0;
        Double x = null;
        CheckValid.checkNotNull(x);
        double actual = NestedShapes.areaSquareNestedInCircle(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test
    public void coefficientAreaReduction_Normal_Values() throws NullReferenceException, NotPositiveNumberException {
        double expected = 2;
        double x = 30;
        double actual = NestedShapes.coefficientAreaReduction(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void coefficientAreaReduction_Negative_Values() throws NullReferenceException, NotPositiveNumberException {
        double expected = 2;
        double x = -30;
        double actual = NestedShapes.coefficientAreaReduction(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NullReferenceException.class)
    public void coefficientAreaReduction_Null_Reference() throws NullReferenceException, NotPositiveNumberException {
        double expected = 2;
        Double x = null;
        CheckValid.checkNotNull(x);
        double actual = NestedShapes.coefficientAreaReduction(x);
        Assert.assertEquals(expected, actual, 0.00001);
    }
}