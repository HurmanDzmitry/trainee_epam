package by.epam.javatr.task6.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class TimeTest {

    @Test
    public void getHours_Positive_Value() throws IllegalValueException, NullReferenceException {
        int expected = 1;
        int x = 3661;
        int actual = Time.getHours(x);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getHours_Zero_Value() throws IllegalValueException, NullReferenceException {
        int expected = 0;
        int x = 0;
        int actual = Time.getHours(x);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalValueException.class)
    public void getHours_Negative_Value() throws IllegalValueException, NullReferenceException {
        int expected = 0;
        int x = -3661;
        int actual = Time.getHours(x);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullReferenceException.class)
    public void getHours_Null_Value() throws IllegalValueException, NullReferenceException {
        int expected = 0;
        Integer x = null;
        CheckValid.checkNotNull(x);
        int actual = Time.getHours(x);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getMinutes_Positive_Value() throws IllegalValueException, NullReferenceException {
        int expected = 1;
        int x = 3661;
        int actual = Time.getMinutes(x);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getSeconds_Positive_Value() throws IllegalValueException, NullReferenceException {
        int expected = 1;
        int x = 3661;
        int actual = Time.getSeconds(x);
        Assert.assertEquals(expected, actual);
    }
}