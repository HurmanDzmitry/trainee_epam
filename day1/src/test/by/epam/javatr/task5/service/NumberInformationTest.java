package by.epam.javatr.task5.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class NumberInformationTest {

    @Test
    public void isPerfectNumber_Normal_Values_Should_Be_True() throws NullReferenceException, NotPositiveNumberException {
        int x = 6;
        boolean actual = NumberInformation.isPerfectNumber(x);
        Assert.assertTrue(actual);
    }

    @Test
    public void isPerfectNumber_Normal_Values_Should_Be_False() throws NullReferenceException, NotPositiveNumberException {
        int x = 5;
        boolean actual = NumberInformation.isPerfectNumber(x);
        Assert.assertFalse(actual);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void isPerfectNumber_Negative_Numbers() throws NullReferenceException, NotPositiveNumberException {
        int x = -6;
        boolean actual = NumberInformation.isPerfectNumber(x);
        Assert.assertFalse(actual);
    }

    @Test(expected = NullReferenceException.class)
    public void isPerfectNumber_Null_Reference() throws NullReferenceException, NotPositiveNumberException {
        Integer x = null;
        CheckValid.checkNotNull(x);
        boolean actual = NumberInformation.isPerfectNumber(x);
        Assert.assertFalse(actual);
    }
}