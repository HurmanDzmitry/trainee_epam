package by.epam.javatr.task4.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class NumberInformationTest {

    @Test
    public void isEvenSeveralNumber_Normal_Values_Should_Be_True() throws NullReferenceException {
        int a = 0;
        int b = 2;
        int c = 3;
        int d = 4;
        boolean actual = NumberInformation.isEvenSeveralNumber(a, b, c, d);
        Assert.assertTrue(actual);
    }

    @Test
    public void isEvenSeveralNumber_Normal_Values_Should_Be_False() throws NullReferenceException {
        int a = 1;
        int b = 3;
        int c = -3;
        int d = 4;
        boolean actual = NumberInformation.isEvenSeveralNumber(a, b, c, d);
        Assert.assertFalse(actual);
    }

    @Test
    public void isEvenSeveralNumber_Normal_Values_With_Negative_Numbers_Should_Be_True() throws NullReferenceException {
        int a = -1;
        int b = 2;
        int c = 3;
        int d = -4;
        boolean actual = NumberInformation.isEvenSeveralNumber(a, b, c, d);
        Assert.assertTrue(actual);
    }

    @Test(expected = NullReferenceException.class)
    public void isEvenSeveralNumber__Null_Reference() throws NullReferenceException {
        int a = -1;
        int b = 2;
        int c = 3;
        Integer d = null;
        CheckValid.checkNotNull(d);
        boolean actual = NumberInformation.isEvenSeveralNumber(a, b, c, d);
        Assert.assertFalse(actual);
    }
}