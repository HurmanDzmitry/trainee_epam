package by.epam.javatr.task10.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

public class FunctionsTest {
    private Map<Double, Double> expected = new TreeMap<>();
    private Map<Double, Double> actual = new TreeMap<>();

    @Before
    public void before() {
        expected.put(0.0, 0.0);
        expected.put(0.5, 0.54630);
        expected.put(1.0, 1.55741);
    }

    @After
    public void after() {
        expected.clear();
        actual.clear();
    }

    @Test
    public void functionInSegment_Should_Be_Valid() throws NotPositiveNumberException, IllegalValueException, NullReferenceException {
        actual = Functions.functionInSegment(0.0, 1.0, 0.5);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullReferenceException.class)
    public void functionInSegment_Null() throws NotPositiveNumberException, IllegalValueException, NullReferenceException {
        Double x = null;
        CheckValid.checkNotNull(x);
        actual = Functions.functionInSegment(x, 1.0, 0.5);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void functionInSegment_Step_Less_Zero() throws NotPositiveNumberException, IllegalValueException, NullReferenceException {
        actual = Functions.functionInSegment(0.0, 1.0, -0.5);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = IllegalValueException.class)
    public void functionInSegment_Start_Less_Finish() throws NotPositiveNumberException, IllegalValueException, NullReferenceException {
        actual = Functions.functionInSegment(1.0, 0.0, 0.5);
        Assert.assertEquals(expected, actual);
    }
}