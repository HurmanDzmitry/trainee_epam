package by.epam.javatr.task9.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task9.model.Circle;
import org.junit.Assert;
import org.junit.Test;

public class CircleTest {

    @Test
    public void circumference_Should_Be_Valid() throws NullReferenceException, NotPositiveNumberException {
        double expected = 62.83185;
        double radius = 10;
        CircleParameters circleParameters = new CircleParameters(new Circle(radius));
        double actual = circleParameters.circumference();
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void circumference_Negative_Radius() throws NullReferenceException, NotPositiveNumberException {
        double expected = 0;
        double radius = -10;
        CircleParameters circleParameters = new CircleParameters(new Circle(radius));
        double actual = circleParameters.circumference();
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NullReferenceException.class)
    public void circumference_Null() throws NullReferenceException, NotPositiveNumberException {
        double expected = 0;
        Double radius = null;
        CheckValid.checkNotNull(radius);
        CircleParameters circleParameters = new CircleParameters(new Circle(radius));
        double actual = circleParameters.circumference();
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test
    public void areaCircle_Should_Be_Valid() throws NullReferenceException, NotPositiveNumberException {
        double expected = 314.15926;
        double radius = 10;
        CircleParameters circleParameters = new CircleParameters(new Circle(radius));
        double actual = circleParameters.areaCircle();
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void areaCircle_Negative_Radius() throws NullReferenceException, NotPositiveNumberException {
        double expected = 0;
        double radius = -10;
        CircleParameters circleParameters = new CircleParameters(new Circle(radius));
        double actual = circleParameters.areaCircle();
        Assert.assertEquals(expected, actual, 0.00001);
    }

    @Test(expected = NullReferenceException.class)
    public void areaCircle_Null() throws NullReferenceException, NotPositiveNumberException {
        double expected = 0;
        Double radius = null;
        CheckValid.checkNotNull(radius);
        CircleParameters circleParameters = new CircleParameters(new Circle(radius));
        double actual = circleParameters.areaCircle();
        Assert.assertEquals(expected, actual, 0.00001);
    }
}