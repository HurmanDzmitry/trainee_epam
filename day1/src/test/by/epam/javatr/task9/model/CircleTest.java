package by.epam.javatr.task9.model;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import org.junit.Assert;
import org.junit.Test;

public class CircleTest {

    @Test
    public void create_Circle_Should_Be_Valid() throws NullReferenceException, NotPositiveNumberException {
        Circle expected = new Circle(5);
        Circle actual = new Circle(5);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NullReferenceException.class)
    public void create_Circle_Null_Reference() throws NullReferenceException, NotPositiveNumberException {
        Circle expected = new Circle(5);
        Integer x = null;
        CheckValid.checkNotNull(x);
        Circle actual = new Circle(x);
        Assert.assertEquals(expected, actual);
    }

    @Test(expected = NotPositiveNumberException.class)
    public void create_Circle_Not_Positive_Value() throws NullReferenceException, NotPositiveNumberException {
        Circle expected = new Circle(5);
        Circle actual = new Circle(-5);
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void create_Circle_Empty() {
        double expected = 0;
        Circle circle = new Circle();
        double actual = circle.getRadius();
        Assert.assertEquals(expected, actual, 0.00001);
    }
}