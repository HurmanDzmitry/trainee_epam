package by.epam.javatr.check;

import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

public class CheckValid {

    public static boolean checkForPositiveValues(double value) throws NotPositiveNumberException {
        if (value > 0)
            return true;
        else throw new NotPositiveNumberException("Number is not positive!");
    }

    public static boolean checkNotNull(Object value) throws NullReferenceException {
        if (value != null)
            return true;
        else throw new NullReferenceException("Reference to null!");
    }

    public static boolean checkIllegalValues(int border, int values) throws IllegalValueException {
        if (border >= values)
            return true;
        else throw new IllegalValueException("Illegal value, check condition!");
    }

    public static boolean checkIllegalValues(double border, double values) throws IllegalValueException {
        if (border >= values)
            return true;
        else throw new IllegalValueException("Illegal value, check condition!");
    }
}