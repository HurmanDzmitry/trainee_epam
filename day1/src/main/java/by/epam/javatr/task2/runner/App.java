package by.epam.javatr.task2.runner;

import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task2.service.Time;

public class App {

    public static void main(String[] args) {
        try {
            System.out.println("Enter year and month(1-12 range)");
            System.out.println(Time.numberDaysOfMonth(InputValues.SCANNER.nextInt(), InputValues.SCANNER.nextInt()));
            System.out.println("is days in enter month");
        } catch (IllegalValueException | NotPositiveNumberException | NullReferenceException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}