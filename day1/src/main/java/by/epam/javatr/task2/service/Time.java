package by.epam.javatr.task2.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Time {

    private Time() {
    }

    public static int numberDaysOfMonth(int year, int month) throws IllegalValueException, NotPositiveNumberException, NullReferenceException {
        CheckValid.checkNotNull(year);
        CheckValid.checkNotNull(month);
        CheckValid.checkIllegalValues(12, month);
        CheckValid.checkForPositiveValues(month);
        Calendar calendar;
        calendar = new GregorianCalendar(year, month, 0);
        return calendar.get(Calendar.DAY_OF_MONTH);
    }
}

