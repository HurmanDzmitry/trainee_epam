package by.epam.javatr.scanner;

import java.util.Scanner;

public class InputValues {

    private InputValues() {
    }

    public static final Scanner SCANNER = new Scanner(System.in);
}
