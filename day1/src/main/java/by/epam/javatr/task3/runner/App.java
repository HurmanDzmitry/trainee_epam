package by.epam.javatr.task3.runner;

import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task3.service.NestedShapes;

public class App {

    public static void main(String[] args) {
        try {
            System.out.println("Enter square area around circle");
            double input = InputValues.SCANNER.nextDouble();
            System.out.println("It is square area nested in circle");
            System.out.printf("%.3f" + '\n', NestedShapes.areaSquareNestedInCircle(input));
            System.out.println("It is coefficient area reduction");
            System.out.printf("%.3f" + '\n', NestedShapes.coefficientAreaReduction(input));
        } catch (NullReferenceException | NotPositiveNumberException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}