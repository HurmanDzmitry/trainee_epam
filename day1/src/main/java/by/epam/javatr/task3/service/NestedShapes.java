package by.epam.javatr.task3.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

public class NestedShapes {

    private NestedShapes() {
    }

    private static double radiusCircleNestedInSquare(double areaSquareAroundCircle) throws NotPositiveNumberException, NullReferenceException {
        CheckValid.checkNotNull(areaSquareAroundCircle);
        CheckValid.checkForPositiveValues(areaSquareAroundCircle);
        return Math.pow(areaSquareAroundCircle, 0.5) / 2;

    }

    public static double areaSquareNestedInCircle(double areaSquareAroundCircle) throws NullReferenceException, NotPositiveNumberException {
        double radiusCircle = radiusCircleNestedInSquare(areaSquareAroundCircle);
        return Math.pow((radiusCircle * Math.sqrt(2)), 2);
    }

    public static double coefficientAreaReduction(double areaSquareAroundCircle) throws NullReferenceException, NotPositiveNumberException {
        double areaSquareNestedInCircle = areaSquareNestedInCircle(areaSquareAroundCircle);
        return areaSquareAroundCircle / areaSquareNestedInCircle;
    }
}