package by.epam.javatr.task9.service;

import by.epam.javatr.task9.model.Circle;

public class CircleParameters {
    private double radius;

    public CircleParameters() {
    }

    public CircleParameters(Circle circle) {
        this.radius = circle.getRadius();
    }

    public double circumference() {
        return 2 * Math.PI * radius;
    }

    public double areaCircle() {
        return Math.PI * Math.pow(radius, 2);
    }

}
