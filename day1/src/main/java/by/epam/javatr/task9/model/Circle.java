package by.epam.javatr.task9.model;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

import java.util.Objects;

public class Circle {
    private double radius;

    public Circle() {
    }

    public Circle(double radius) throws NullReferenceException, NotPositiveNumberException {
        setRadius(radius);
    }


    public double getRadius() {
        return radius;
    }

    private void setRadius(double radius) throws NullReferenceException, NotPositiveNumberException {
        CheckValid.checkNotNull(radius);
        CheckValid.checkForPositiveValues(radius);
        this.radius = radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return Double.compare(circle.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "radius=" + radius +
                '}';
    }
}