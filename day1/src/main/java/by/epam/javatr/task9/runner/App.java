package by.epam.javatr.task9.runner;

import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task9.model.Circle;
import by.epam.javatr.task9.service.CircleParameters;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Enter radius circle");
            CircleParameters circleParameters = new CircleParameters(new Circle(InputValues.SCANNER.nextDouble()));
            System.out.printf("Circumference: %.3f" + '\n', circleParameters.circumference());
            System.out.printf("Area circle: %.3f" + '\n', circleParameters.areaCircle());
        } catch (NullReferenceException | NotPositiveNumberException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
