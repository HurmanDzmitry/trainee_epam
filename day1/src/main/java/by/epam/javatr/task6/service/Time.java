package by.epam.javatr.task6.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;

public class Time {

    private Time() {
    }

    public static int getHours(int seconds) throws IllegalValueException, NullReferenceException {
        CheckValid.checkNotNull(seconds);
        CheckValid.checkIllegalValues(86400, seconds);
        CheckValid.checkIllegalValues(seconds, 0);
        return seconds / 3600;
    }

    public static int getMinutes(int seconds) throws IllegalValueException, NullReferenceException {
        return seconds / 60 - getHours(seconds) * 60;
    }

    public static int getSeconds(int seconds) throws IllegalValueException, NullReferenceException {
        return seconds - getHours(seconds) * 3600 - getMinutes(seconds) * 60;
    }
}