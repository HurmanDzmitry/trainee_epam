package by.epam.javatr.task6.runner;

import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task6.service.Time;


public class App {
    public static void main(String[] args) {
        System.out.println("Enter seconds(0-86400 range)");
        int input = InputValues.SCANNER.nextInt();
        try {
            System.out.println("Hours: " + Time.getHours(input));
            System.out.println("Minutes: " + Time.getMinutes(input));
            System.out.println("Seconds: " + Time.getSeconds(input));
        } catch (IllegalValueException | NullReferenceException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

    }
}
