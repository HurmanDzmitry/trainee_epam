package by.epam.javatr.task8.runner;

import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task8.service.Functions;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Enter variable");
            System.out.println(Functions.function(InputValues.SCANNER.nextDouble()));
            System.out.println("is result.");
        } catch (NullReferenceException | ArithmeticException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}