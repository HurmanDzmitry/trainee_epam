package by.epam.javatr.task8.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;

public class Functions {

    private Functions() {
    }

    public static double function(double variable) throws ArithmeticException, NullReferenceException {
        CheckValid.checkNotNull(variable);
        if (variable == (Math.pow(6, 1.0 / 3.0))) {
            throw new ArithmeticException();
        }
        if (variable >= 3) {
            return -(Math.pow(variable, 2)) + 3 * variable + 9;
        } else
            return 1 / (Math.pow(variable, 3) - 6);
    }
}