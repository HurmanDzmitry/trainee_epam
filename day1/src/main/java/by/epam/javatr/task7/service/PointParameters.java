package by.epam.javatr.task7.service;

import by.epam.javatr.task7.model.Point;

public class PointParameters {
    private PointParameters() {
    }

    public static double distanceToCenterCoordinates(Point point) {
        return Math.sqrt(Math.pow(point.getX(), 2) + Math.pow(point.getY(), 2));
    }

    public static Point nearPoint(Point a, Point b) {
        return ((distanceToCenterCoordinates(a) >= distanceToCenterCoordinates(b)) ? b : a);
    }
}
