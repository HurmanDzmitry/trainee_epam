package by.epam.javatr.task7.runner;

import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task7.model.Point;
import by.epam.javatr.task7.service.PointParameters;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Enter values point");
            Point a = new Point(InputValues.SCANNER.nextDouble(), InputValues.SCANNER.nextDouble());
            System.out.println("Enter values point");
            Point b = new Point(InputValues.SCANNER.nextDouble(), InputValues.SCANNER.nextDouble());
            System.out.println("Near point is " + PointParameters.nearPoint(a, b));
        } catch (NullReferenceException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
