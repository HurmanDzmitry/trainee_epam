package by.epam.javatr.task7.model;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;

import java.util.Objects;

public class Point {
    private double x;
    private double y;

    public Point() {
    }

    public Point(double x, double y) throws NullReferenceException {
        setX(x);
        setY(y);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) throws NullReferenceException {
        CheckValid.checkNotNull(x);
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) throws NullReferenceException {
        CheckValid.checkNotNull(x);
        this.y = y;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return Double.compare(point.x, x) == 0 &&
                Double.compare(point.y, y) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public String toString() {
        return "point{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
