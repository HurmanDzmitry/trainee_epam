package by.epam.javatr.task1.runner;

import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task1.service.NumberOperations;

public class App {

    public static void main(String[] args) {
        try {
            System.out.println("Enter number or last digit of number:");
            System.out.println(NumberOperations.getLastDigitOfNumberPowerOf2(InputValues.SCANNER.nextInt()));
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}