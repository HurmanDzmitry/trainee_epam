package by.epam.javatr.task1.service;

public class NumberOperations {

    private NumberOperations() {
    }

    public static int getLastDigitOfNumberPowerOf2(int number) throws NullPointerException {
        return (number * number) % 10;
    }
}
