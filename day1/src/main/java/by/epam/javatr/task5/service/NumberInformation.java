package by.epam.javatr.task5.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

public class NumberInformation {

    private NumberInformation() {
    }

    private static int sumDividers(int number) throws NullReferenceException, NotPositiveNumberException {
        CheckValid.checkNotNull(number);
        CheckValid.checkForPositiveValues(number);
        int sum = 0;
        for (double divider = 1; divider < number; divider++) {
            if (number % divider == 0)
                sum += divider;
        }
        return sum;
    }

    public static boolean isPerfectNumber(int number) throws NotPositiveNumberException, NullReferenceException {
        return sumDividers(number) == number;
    }
}