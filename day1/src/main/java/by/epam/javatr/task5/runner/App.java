package by.epam.javatr.task5.runner;

import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task5.service.NumberInformation;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Enter number");
            boolean perfect = NumberInformation.isPerfectNumber(InputValues.SCANNER.nextInt());
            String message = perfect ? "perfect" : "not perfect";
            System.out.println("This number is " + message);
        } catch (NotPositiveNumberException | NullReferenceException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
