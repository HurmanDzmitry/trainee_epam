package by.epam.javatr.task10.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.TreeMap;

public class Functions {

    private Functions() {
    }

    public static Map<Double, Double> functionInSegment(double startSegment, double finishSegment, double step)
            throws NullReferenceException, NotPositiveNumberException, IllegalValueException {
        CheckValid.checkNotNull(startSegment);
        CheckValid.checkNotNull(finishSegment);
        CheckValid.checkNotNull(step);
        CheckValid.checkIllegalValues(finishSegment, startSegment);
        CheckValid.checkForPositiveValues(step);
        Map<Double, Double> map = new TreeMap<>();
        for (double i = startSegment; i < finishSegment; i += step) {
            map.put(i, roundDouble(Math.tan(i)));
            map.put(finishSegment, roundDouble(Math.tan(finishSegment)));
        }
        return map;
    }

    private static double roundDouble(double d) {
        return new BigDecimal(d).setScale(5, RoundingMode.HALF_UP).doubleValue();
    }
}