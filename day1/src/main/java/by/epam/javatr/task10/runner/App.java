package by.epam.javatr.task10.runner;

import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task10.service.Functions;

import java.util.HashMap;
import java.util.Map;

public class App {
    public static void main(String[] args) {
        Map<Double, Double> map = new HashMap<>();
        try {
            System.out.println("Enter values: startSegment, finishSegment, step");
            map = Functions.functionInSegment(InputValues.SCANNER.nextDouble(),
                    InputValues.SCANNER.nextDouble(),
                    InputValues.SCANNER.nextDouble());
        } catch (NullReferenceException | NotPositiveNumberException | IllegalValueException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        if (!map.isEmpty()) {
            System.out.println("Result:");
            for (Double d : map.keySet()) {
                System.out.printf("%.3f" + " : ", d);
                System.out.printf("%.3f" + "\n", map.get(d));
            }
        }
    }
}
