package by.epam.javatr.task4.runner;

import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.scanner.InputValues;
import by.epam.javatr.task4.service.NumberInformation;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Enter 4 number");
            boolean several = NumberInformation.isEvenSeveralNumber(InputValues.SCANNER.nextInt(),
                    InputValues.SCANNER.nextInt(),
                    InputValues.SCANNER.nextInt(),
                    InputValues.SCANNER.nextInt());
            String message = several ? "several" : "not several";
            System.out.println("This number is " + message);
        } catch (NullReferenceException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
