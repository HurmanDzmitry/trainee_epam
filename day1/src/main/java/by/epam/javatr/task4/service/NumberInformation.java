package by.epam.javatr.task4.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NullReferenceException;

public class NumberInformation {

    private NumberInformation() {
    }

    private static boolean isEven(int number) throws NullReferenceException {
        CheckValid.checkNotNull(number);
        return (number % 2) == 0;
    }

    public static boolean isEvenSeveralNumber(int a, int b, int c, int d) throws NullReferenceException {
        int[] arrayNumber = {a, b, c, d};
        return percentageEvenNumbers(arrayNumber) >= 0.5;
    }

    private static double percentageEvenNumbers(int[] arrayNumber) throws NullReferenceException {
        double counter = 0;
        for (int i : arrayNumber) {
            if (isEven(i))
                counter++;
        }
        return counter / arrayNumber.length;
    }
}
