package by.epam.javatr.exception;

public class NullReferenceException extends Exception {

    public NullReferenceException() {
    }

    public NullReferenceException(String message) {
        super(message);
    }

    public NullReferenceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NullReferenceException(Throwable cause) {
        super(cause);
    }
}
