package by.epam.hurman_dzmitry.controller.command.util;

public class ParameterName {

    public static final String SAX = "SAX";
    public static final String StAX = "StAX";
    public static final String DOM = "DOM";
    public static final String PARSING = "parsing";
    public static final String BOOKS = "books";
    public static final String MESSAGE = "message";

    private ParameterName() {
    }
}
