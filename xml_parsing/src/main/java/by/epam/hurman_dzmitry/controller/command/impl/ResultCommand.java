package by.epam.hurman_dzmitry.controller.command.impl;

import by.epam.hurman_dzmitry.controller.command.Command;
import by.epam.hurman_dzmitry.controller.command.Router;
import by.epam.hurman_dzmitry.controller.command.Transition;
import by.epam.hurman_dzmitry.controller.command.util.JspPath;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ResultCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) {
        return new Router(JspPath.RESULT, Transition.FORWARD);
    }
}
