package by.epam.hurman_dzmitry.controller.command.util;

public class JspPath {

    public static final String ERROR = "WEB-INF/jsp/error.jsp";
    public static final String MAIN = "WEB-INF/jsp/main.jsp";
    public static final String RESULT = "WEB-INF/jsp/result.jsp";

    private JspPath() {
    }
}
