package by.epam.hurman_dzmitry.controller.command;

public enum Transition {

    FORWARD, REDIRECT
}