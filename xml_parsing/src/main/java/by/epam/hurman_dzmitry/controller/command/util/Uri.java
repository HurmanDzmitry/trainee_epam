package by.epam.hurman_dzmitry.controller.command.util;

public class Uri {

    private static final String CONTEXT = "/library";
    public static final String MAIN = CONTEXT + "/main";
    public static final String RESULT = CONTEXT + "/result";
    public static final String ERROR = CONTEXT + "/error";
    public static final String PARSE = CONTEXT + "/parse";

    private Uri() {
    }
}