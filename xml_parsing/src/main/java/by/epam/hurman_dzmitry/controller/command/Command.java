package by.epam.hurman_dzmitry.controller.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {

    Router execute(HttpServletRequest req, HttpServletResponse resp);
}
