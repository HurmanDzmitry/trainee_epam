package by.epam.hurman_dzmitry.controller;

import by.epam.hurman_dzmitry.controller.command.Command;
import by.epam.hurman_dzmitry.controller.command.Router;
import by.epam.hurman_dzmitry.controller.command.Transition;
import by.epam.hurman_dzmitry.controller.command.impl.ErrorCommand;
import by.epam.hurman_dzmitry.controller.command.impl.MainPageCommand;
import by.epam.hurman_dzmitry.controller.command.impl.ParsingCommand;
import by.epam.hurman_dzmitry.controller.command.impl.ResultCommand;
import by.epam.hurman_dzmitry.controller.command.util.Uri;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 3,
        maxRequestSize = 1024 * 1024 * 3 * 2)
@WebServlet(name = "LibraryServlet",
        urlPatterns = {"/main", "/result", "/error", "/parse"})
public class LibraryServlet extends HttpServlet {

    public static final long serialVersionUID = 20L;

    private static final Logger LOGGER = LogManager.getLogger(LibraryServlet.class);

    private Map<String, Command> commands = new HashMap<>();

    {
        commands.put(Uri.MAIN, new MainPageCommand());
        commands.put(Uri.RESULT, new ResultCommand());
        commands.put(Uri.ERROR, new ErrorCommand());
        commands.put(Uri.PARSE, new ParsingCommand());
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) {
        Command command = commands.get(req.getRequestURI());
        try {
            Router router = command.execute(req, resp);
            if (router.getTransition() == Transition.FORWARD) {
                req.getRequestDispatcher(router.getAddress()).forward(req, resp);
            } else {
                resp.sendRedirect(router.getAddress());
            }
        } catch (Exception e) {
            LOGGER.error("Servlet command doesn't execute.", e);
            try {
                resp.sendRedirect(Uri.ERROR);
            } catch (IOException ex) {
                LOGGER.error("Response doesn't redirect to error page.", e, ex);
            }
        }
    }
}
