package by.epam.hurman_dzmitry.controller.command.impl;

import by.epam.hurman_dzmitry.controller.command.Command;
import by.epam.hurman_dzmitry.controller.command.Router;
import by.epam.hurman_dzmitry.controller.command.Transition;
import by.epam.hurman_dzmitry.controller.command.util.JspPath;
import by.epam.hurman_dzmitry.controller.command.util.ParameterName;
import by.epam.hurman_dzmitry.controller.command.util.Uri;
import by.epam.hurman_dzmitry.entity.Book;
import by.epam.hurman_dzmitry.service.ServiceException;
import by.epam.hurman_dzmitry.service.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ParsingCommand implements Command {

    private static final Logger LOGGER = LogManager.getLogger(ParsingCommand.class);

    private final String MESSAGE_UPLOAD_FAIL = "Upload file fail.";
    private final String EMPTY_STR = "";

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) {
        String kindParsing = req.getParameter(ParameterName.PARSING);
        List<Book> books;
        String fileName = null;
        try {
            String uploadPath = req.getServletContext().getRealPath(EMPTY_STR);
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            for (Part part : req.getParts()) {
                String submittedFileName = part.getSubmittedFileName();
                if (submittedFileName != null) {
                    fileName = uploadPath + File.separator + submittedFileName;
                }
                part.write(uploadPath + File.separator + submittedFileName);
            }
            books = ServiceFactory.getInstance().getBookService().getResult(fileName, kindParsing.toLowerCase());
            Arrays.stream(uploadDir.listFiles()).forEach(File::delete);
        } catch (ServletException | IOException e) {
            LOGGER.error(MESSAGE_UPLOAD_FAIL, e);
            req.setAttribute(ParameterName.MESSAGE, MESSAGE_UPLOAD_FAIL);
            return new Router(JspPath.MAIN, Transition.FORWARD);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
            req.setAttribute(ParameterName.MESSAGE, e.getMessage());
            return new Router(JspPath.MAIN, Transition.FORWARD);
        }
        req.getSession().setAttribute(ParameterName.BOOKS, books);
        return new Router(Uri.RESULT, Transition.REDIRECT);
    }
}
