package by.epam.hurman_dzmitry.entity;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Book implements Serializable {

    private int id;
    private String title;
    private int year;
    private List<Author> authors;

    public Book() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return id == book.id &&
                year == book.year &&
                Objects.equals(title, book.title) &&
                Objects.equals(authors, book.authors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, title, year, authors);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "id=" + id +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", authors=" + authors +
                '}';
    }
}