package by.epam.hurman_dzmitry.service;

import by.epam.hurman_dzmitry.entity.Book;

import java.util.List;

public interface BookService {

    List<Book> getResult(String fileName, String key) throws ServiceException;
}
