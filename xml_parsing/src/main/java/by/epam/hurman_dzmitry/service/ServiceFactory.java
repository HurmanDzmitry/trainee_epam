package by.epam.hurman_dzmitry.service;

import by.epam.hurman_dzmitry.service.impl.BookServiceImpl;

public class ServiceFactory {

    private static ServiceFactory INSTANCE;

    private final BookService bookService = new BookServiceImpl();

    private ServiceFactory() {
    }

    public static synchronized ServiceFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ServiceFactory();
        }
        return INSTANCE;
    }

    public BookService getBookService() {
        return bookService;
    }
}
