package by.epam.hurman_dzmitry.service.impl;

import by.epam.hurman_dzmitry.entity.Book;
import by.epam.hurman_dzmitry.service.BookService;
import by.epam.hurman_dzmitry.service.ServiceException;
import by.epam.hurman_dzmitry.service.parsing.ParsingKind;
import by.epam.hurman_dzmitry.service.parsing.XMLParser;
import by.epam.hurman_dzmitry.service.parsing.impl.DOMParser;
import by.epam.hurman_dzmitry.service.parsing.impl.SAXParser;
import by.epam.hurman_dzmitry.service.parsing.impl.StaXParser;
import by.epam.hurman_dzmitry.service.validation.XMLValidation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BookServiceImpl implements BookService {

    private Map<String, XMLParser> parsers = new HashMap<>();

    {
        parsers.put(ParsingKind.DOM, new DOMParser());
        parsers.put(ParsingKind.SAX, new SAXParser());
        parsers.put(ParsingKind.StAX, new StaXParser());
    }

    @Override
    public List<Book> getResult(String fileName, String key) throws ServiceException {
        XMLParser xmlParser = parsers.get(key);
        XMLValidation.validateXMLSchema(fileName);
        return xmlParser.parse(fileName);
    }
}
