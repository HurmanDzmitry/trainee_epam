package by.epam.hurman_dzmitry.service.validation;

import by.epam.hurman_dzmitry.service.ServiceException;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;

public class XMLValidation {

    private static final String XSD_FILE = "/book.xsd";

    private XMLValidation() {
    }

    public static boolean validateXMLSchema(String xmlPath) throws ServiceException {
        try {
            String xsdPath = XMLValidation.class.getClassLoader().getResource("").getPath() + XSD_FILE;
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(xsdPath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlPath)));
        } catch (IOException e) {
            throw new ServiceException("File not available.", e);
        } catch (SAXException e) {
            throw new ServiceException("Invalid file.", e);
        }
        return true;
    }
}
