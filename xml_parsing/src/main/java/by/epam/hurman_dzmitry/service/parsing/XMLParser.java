package by.epam.hurman_dzmitry.service.parsing;

import by.epam.hurman_dzmitry.entity.Book;
import by.epam.hurman_dzmitry.service.ServiceException;

import java.util.List;

public interface XMLParser {

    List<Book> parse(String fileName) throws ServiceException;
}
