package by.epam.hurman_dzmitry.service.parsing;

public class ParsingKind {

    public static final String DOM = "dom";
    public static final String SAX = "sax";
    public static final String StAX = "stax";

    private ParsingKind() {
    }
}
