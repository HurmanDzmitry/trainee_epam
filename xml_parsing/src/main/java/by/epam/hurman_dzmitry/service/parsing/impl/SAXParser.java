package by.epam.hurman_dzmitry.service.parsing.impl;

import by.epam.hurman_dzmitry.dao.DaoException;
import by.epam.hurman_dzmitry.dao.DaoFactory;
import by.epam.hurman_dzmitry.dao.XMLParserDao;
import by.epam.hurman_dzmitry.entity.Book;
import by.epam.hurman_dzmitry.service.ServiceException;
import by.epam.hurman_dzmitry.service.parsing.XMLParser;

import java.util.List;

public class SAXParser implements XMLParser {
    @Override
    public List<Book> parse(String fileName) throws ServiceException {
        XMLParserDao xmlParserSAX = DaoFactory.getInstance().getXmlParserSAX();
        try {
            return xmlParserSAX.parse(fileName);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer fail.", e);
        }
    }
}