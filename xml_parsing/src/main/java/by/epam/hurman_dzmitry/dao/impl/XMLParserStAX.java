package by.epam.hurman_dzmitry.dao.impl;

import by.epam.hurman_dzmitry.dao.DaoException;
import by.epam.hurman_dzmitry.dao.XMLParserDao;
import by.epam.hurman_dzmitry.dao.util.TagName;
import by.epam.hurman_dzmitry.entity.Author;
import by.epam.hurman_dzmitry.entity.Book;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public class XMLParserStAX implements XMLParserDao {

    public XMLParserStAX() {
    }

    @Override
    public List<Book> parse(String fileName) throws DaoException {
        List<Book> books = new ArrayList<>();
        Book book = null;
        List<Author> authors = new ArrayList<>();
        Author author = null;
        TagName elementName = null;
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        try {
            InputStream input = new FileInputStream(fileName);
            XMLStreamReader reader = inputFactory.createXMLStreamReader(input);
            while (reader.hasNext()) {
                int type = reader.next();
                switch (type) {
                    case XMLStreamConstants.START_ELEMENT:
                        elementName = TagName.valueOf(reader.getLocalName().toUpperCase());
                        switch (elementName) {
                            case BOOK:
                                book = new Book();
                                int id = Integer.parseInt(reader.getAttributeValue(null,
                                        TagName.ID.getTag()).replace(TagName.BOOK.getTag(), ""));
                                book.setId(id);
                                break;
                            case AUTHOR:
                                author = new Author();
                                break;
                        }
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        String text = reader.getText().trim();
                        if (text.isEmpty()) {
                            break;
                        }
                        switch (elementName) {
                            case TITLE:
                                book.setTitle(text);
                                break;
                            case YEAR:
                                book.setYear(Integer.parseInt(text));
                                break;
                            case NAME:
                                author.setName(text);
                                break;
                        }
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        elementName = TagName.valueOf(reader.getLocalName().toUpperCase());
                        switch (elementName) {
                            case AUTHOR:
                                authors.add(author);
                                break;
                            case BOOK:
                                book.setAuthors(new ArrayList<>(authors));
                                books.add(book);
                                authors.clear();
                                book = null;
                                break;
                        }
                }
            }
        } catch (XMLStreamException | FileNotFoundException e) {
            throw new DaoException("StAX parsing fail.", e);
        }
        return books;
    }
}
