package by.epam.hurman_dzmitry.dao.impl;

import by.epam.hurman_dzmitry.dao.DaoException;
import by.epam.hurman_dzmitry.dao.XMLParserDao;
import by.epam.hurman_dzmitry.dao.util.TagName;
import by.epam.hurman_dzmitry.entity.Author;
import by.epam.hurman_dzmitry.entity.Book;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLParserSAX extends DefaultHandler implements XMLParserDao {

    private final String REGEX_REPLACE_PREFIX = "[\\w-]:";
    private final String EMPTY_STR = "";

    private List<Book> books = new ArrayList<>();
    private Book book = null;
    private List<Author> authors = new ArrayList<>();
    private Author author = null;
    private StringBuilder text = null;

    public XMLParserSAX() {
    }

    @Override
    public List<Book> parse(String fileName) throws DaoException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            saxParserFactory.setFeature("http://xml.org/sax/features/validation", true);
            SAXParser saxParser = saxParserFactory.newSAXParser();
            saxParser.parse(new InputSource(fileName), this);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new DaoException("SAX parsing fail.", e);
        }
        return books;
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        if (qName.equals(TagName.BOOK.getTag())) {
            book = new Book();
            book.setId((Integer.parseInt(attributes.getValue(TagName.ID.getTag())
                    .replace(TagName.BOOK.getTag(), EMPTY_STR))));
        }
        if (qName.equals(TagName.AUTHOR.getTag())) {
            author = new Author();
        }
        text = new StringBuilder();
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        TagName tagName = TagName.valueOf(qName.toUpperCase().replaceFirst(REGEX_REPLACE_PREFIX, EMPTY_STR));
        switch (tagName) {
            case TITLE:
                book.setTitle(text.toString());
                break;
            case YEAR:
                book.setYear(Integer.parseInt(text.toString()));
                break;
            case AUTHOR:
                authors.add(author);
                break;
            case NAME:
                author.setName(text.toString());
                break;
            case BOOK:
                book.setAuthors(new ArrayList<>(authors));
                books.add(book);
                authors.clear();
                book = null;
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        text.append(new String(ch, start, length));
    }
}