package by.epam.hurman_dzmitry.dao.impl;

import by.epam.hurman_dzmitry.dao.DaoException;
import by.epam.hurman_dzmitry.dao.XMLParserDao;
import by.epam.hurman_dzmitry.dao.util.TagName;
import by.epam.hurman_dzmitry.entity.Author;
import by.epam.hurman_dzmitry.entity.Book;
import org.apache.xerces.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XMLParserDOM implements XMLParserDao {

    public XMLParserDOM() {
    }

    private List<Author> getAuthors(NodeList list) {
        Author author;
        List<Author> authors = new ArrayList<>();
        for (int i = 0; i < list.getLength(); i++) {
            author = new Author();
            Element assortmentElement = (Element) list.item(i);
            author.setName(getSingleChild(assortmentElement, TagName.NAME.getTag()).getTextContent().trim());
            authors.add(author);
        }
        return authors;
    }

    private Element getSingleChild(Element element, String childName) {
        NodeList nlist = element.getElementsByTagName(childName);
        return (Element) nlist.item(0);
    }

    @Override
    public List<Book> parse(String fileName) throws DaoException {
        DOMParser parser = new DOMParser();
        try {
            parser.parse(fileName);
        } catch (SAXException | IOException e) {
            throw new DaoException("DOM parsing fail.", e);
        }
        Document document = parser.getDocument();
        Element root = document.getDocumentElement();
        List<Book> books = new ArrayList<>();
        Book book;
        NodeList bookNodes = root.getElementsByTagName(TagName.BOOK.getTag());
        for (int i = 0; i < bookNodes.getLength(); i++) {
            book = new Book();
            Element bookElement = (Element) bookNodes.item(i);
            book.setId(Integer.parseInt(bookElement.getAttribute(TagName.ID.getTag())
                    .replace(TagName.BOOK.getTag(), "")));
            book.setTitle(getSingleChild(bookElement, TagName.TITLE.getTag()).getTextContent().trim());
            book.setYear(Integer.parseInt(getSingleChild(bookElement, TagName.YEAR.getTag()).getTextContent().trim()));
            book.setAuthors(getAuthors(bookElement.getElementsByTagName(TagName.AUTHOR.getTag())));
            books.add(book);
        }
        return books;
    }
}

