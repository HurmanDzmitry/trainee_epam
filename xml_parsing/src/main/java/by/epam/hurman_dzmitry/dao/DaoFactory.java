package by.epam.hurman_dzmitry.dao;

import by.epam.hurman_dzmitry.dao.impl.XMLParserDOM;
import by.epam.hurman_dzmitry.dao.impl.XMLParserSAX;
import by.epam.hurman_dzmitry.dao.impl.XMLParserStAX;

public class DaoFactory {

    private static DaoFactory INSTANCE;

    private final XMLParserDao xmlParserSAX = new XMLParserSAX();
    private final XMLParserDao xmlParserStAX = new XMLParserStAX();
    private final XMLParserDao xmlParserDOM = new XMLParserDOM();

    private DaoFactory() {
    }

    public static synchronized DaoFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DaoFactory();
        }
        return INSTANCE;
    }

    public XMLParserDao getXmlParserSAX() {
        return xmlParserSAX;
    }

    public XMLParserDao getXmlParserStAX() {
        return xmlParserStAX;
    }

    public XMLParserDao getXmlParserDOM() {
        return xmlParserDOM;
    }
}
