package by.epam.hurman_dzmitry.dao.util;

public enum TagName {

    ID("id"),
    TITLE("title"),
    YEAR("year"),
    BOOK("book"),
    NAME("name"),
    AUTHOR("author"),
    BOOKS("books");

    private String tag;

    TagName(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
