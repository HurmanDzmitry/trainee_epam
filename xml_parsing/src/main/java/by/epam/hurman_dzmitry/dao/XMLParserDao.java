package by.epam.hurman_dzmitry.dao;

import by.epam.hurman_dzmitry.entity.Book;

import java.util.List;

public interface XMLParserDao {

    List<Book> parse(String fileName) throws DaoException;
}
