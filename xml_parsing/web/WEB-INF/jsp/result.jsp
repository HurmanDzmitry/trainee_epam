<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.hurman_dzmitry.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta content="text/html" charset="UTF-8">
    <title>Result</title>
</head>
<body>
<table border="1">
    <caption><h3>Result</h3></caption>
    <tr>
        <td>ID</td>
        <td>Title</td>
        <td>Year</td>
        <td>Author(s)</td>
    </tr>
    <c:forEach var="book" items="${books}">
        <tr>
            <td>
                    ${book.id}
            </td>
            <td>
                    ${book.title}
            </td>
            <td>
                    ${book.year}
            </td>
            <td>
                <c:forEach var="author" items="${book.authors}" varStatus="status">
                    ${author.name}
                    <c:if test="${!status.last}">,</c:if>
                </c:forEach>
            </td>
        </tr>
    </c:forEach>
</table>
<br>
<form action="${Uri.MAIN}">
    <button>Back to main</button>
</form>
</body>
</html>
