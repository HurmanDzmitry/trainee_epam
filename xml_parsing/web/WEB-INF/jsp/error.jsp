<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.hurman_dzmitry.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta content="text/html" charset="UTF-8">
    <title>Error</title>
</head>
<body>
<h3>Error</h3>
<p>Try again.</p>
<form action="${Uri.MAIN}">
    <button>Back to main</button>
</form>
</body>
</html>
