<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.hurman_dzmitry.controller.command.util.ParameterName" %>
<%@ page import="by.epam.hurman_dzmitry.controller.command.util.Uri" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <meta content="text/html" charset="UTF-8">
    <title>Main</title>
</head>
<body>
<h3>XML parsing</h3>
<form method="post" action="${Uri.PARSE}" enctype='multipart/form-data'>
    <label>Choose kind of parsing:
        <input type="radio" id="1" name="${ParameterName.PARSING}" value="${ParameterName.SAX}"/>
        <label for="1">${ParameterName.SAX}</label>
        <input type="radio" id="2" name="${ParameterName.PARSING}" value="${ParameterName.StAX}"/>
        <label for="2">${ParameterName.StAX}</label>
        <input type="radio" id="3" checked name="${ParameterName.PARSING}" value="${ParameterName.DOM}"/>
        <label for="3">${ParameterName.DOM}</label><br>
    </label>
    <br>
    <label>Put XML file:
        <input type="file" name="xml" accept="text/xml" required><br>
    </label>
    <br>
    <button type="submit">Submit</button>
</form>
<div style="color: red">
    <c:if test="${message != null}">
        ${message}
    </c:if>
</div>
</body>
</html>
