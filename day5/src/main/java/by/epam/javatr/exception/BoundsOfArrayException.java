package by.epam.javatr.exception;

public class BoundsOfArrayException extends Exception {

    public BoundsOfArrayException() {
        super("Value goes beyond!");
    }

    public BoundsOfArrayException(String message) {
        super(message);
    }

    public BoundsOfArrayException(String message, Throwable cause) {
        super(message, cause);
    }

    public BoundsOfArrayException(Throwable cause) {
        super(cause);
    }
}
