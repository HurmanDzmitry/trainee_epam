package by.epam.javatr.task2.service.comporator;

import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task1.service.Arrays;

import java.util.Comparator;

public class SumsOfRowElementsComparator implements Comparator<int[]> {

    @Override
    public int compare(int[] o1, int[] o2) {
        int i1;
        int i2;
        try {
            i1 = Arrays.sum(o1);
            i2 = Arrays.sum(o2);
        } catch (NullReferenceException e) {
            return -1;
        }
        return Integer.compare(i1, i2);
    }
}