package by.epam.javatr.task2.runner;

import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task2.service.JaggedArray;
import by.epam.javatr.task2.service.comporator.MaxElementsComparator;
import by.epam.javatr.task2.service.comporator.MinElementsComparator;
import by.epam.javatr.task2.service.comporator.SumsOfRowElementsComparator;

public class Main {
    public static void main(String[] args) {

        int[][] array = {{1, 2, 5, 5, 5}, {5, 4, 6, 0}};
        try {
            show(array);
            System.out.println("------------");
            JaggedArray.sort(array, new SumsOfRowElementsComparator());
            show(array);
            System.out.println("------------");
            JaggedArray.sort(array, new MaxElementsComparator());
            show(array);
            System.out.println("------------");
            JaggedArray.sort(array, new MinElementsComparator());
            show(array);
//            System.out.println("+++++++++++++");
        } catch (NullReferenceException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }

//        try {
//            show(array);
//            System.out.println("------------");
//            JaggedArray.sortBySumsOfRowElements(array);
//            show(array);
//            System.out.println("------------");
//            JaggedArray.sortByMaxElements(array);
//            show(array);
//            System.out.println("------------");
//            JaggedArray.sortByMinElements(array);
//            show(array);
//        } catch (NullReferenceException | BoundsOfArrayException | IllegalValueException e) {
//            System.out.println(e.getMessage());
//            e.printStackTrace();
//        }
    }

    private static void show(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
    }
}
