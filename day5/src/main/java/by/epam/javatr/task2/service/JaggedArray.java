package by.epam.javatr.task2.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.BoundsOfArrayException;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task1.service.Arrays;

import java.util.Comparator;

public class JaggedArray {

    private JaggedArray() {
    }

    public static void sort(int[][] array, Comparator<int[]> comparator) throws NullReferenceException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = 0; j < array.length - i - 1; j++) {
                if (comparator.compare(array[j], array[j + 1]) > 0) {
                    int[] buffLine = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = buffLine;
                }
            }
        }
    }


//    public static void sortBySumsOfRowElements(int[][] array) throws NullReferenceException {
//        if (CheckValid.checkForNull(array))
//            throw new NullReferenceException();
//        for (int i = 0; i < array.length - 1; i++) {
//            for (int j = 0; j < array.length - i - 1; j++) {
//                if (Arrays.sum(array[j]) > Arrays.sum(array[j + 1])) {
//                    int[] buffLine = array[j];
//                    array[j] = array[j + 1];
//                    array[j + 1] = buffLine;
//                }
//            }
//        }
//    }
//
//    public static void sortByMaxElements(int[][] array) throws BoundsOfArrayException, IllegalValueException, NullReferenceException {
//        if (CheckValid.checkForNull(array))
//            throw new NullReferenceException();
//        for (int i = 0; i < array.length - 1; i++) {
//            for (int j = 0; j < array.length - i - 1; j++) {
//                if (Arrays.maxElement(array[j]) > Arrays.maxElement(array[j + 1])) {
//                    int[] buffLine = array[j];
//                    array[j] = array[j + 1];
//                    array[j + 1] = buffLine;
//                }
//            }
//        }
//    }
//
//    public static void sortByMinElements(int[][] array) throws BoundsOfArrayException, IllegalValueException, NullReferenceException {
//        if (CheckValid.checkForNull(array))
//            throw new NullReferenceException();
//        for (int i = 0; i < array.length - 1; i++) {
//            for (int j = 0; j < array.length - i - 1; j++) {
//                if (Arrays.minElement(array[j]) > Arrays.minElement(array[j + 1])) {
//                    int[] buffLine = array[j];
//                    array[j] = array[j + 1];
//                    array[j + 1] = buffLine;
//                }
//            }
//        }
//    }
}
