package by.epam.javatr.task2.service.comporator;

import by.epam.javatr.exception.BoundsOfArrayException;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task1.service.Arrays;

import java.util.Comparator;

public class MinElementsComparator implements Comparator<int[]> {

    @Override
    public int compare(int[] o1, int[] o2) {
        int i1;
        int i2;
        try {
            i1 = Arrays.minElement(o1);
            i2 = Arrays.minElement(o2);
        } catch (NullReferenceException | BoundsOfArrayException | IllegalValueException e) {
            return -1;
        }
        return Integer.compare(i1, i2);
    }
}