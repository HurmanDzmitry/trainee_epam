package by.epam.javatr.task3.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;

public class Text {

    private Text() {
    }

    public static String replaceLetterGivenCharAndLetterNumberByStringMethod(String str, char newLetter, int letterNumber) throws NullReferenceException, NotPositiveNumberException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        if (CheckValid.checkNotPositive(letterNumber))
            throw new NotPositiveNumberException();
        if (str.isEmpty())
            return str;
        else {
            str = str.trim();
            String[] words = str.split(" ");
            StringBuilder sB = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    if ((words[i].charAt(0) == '\"' && numberLetter(words[i].substring(1)) >= letterNumber
                            && words[i].charAt(letterNumber) >= 'A' && words[i].charAt(letterNumber) <= 'z')) {
                        words[i] = words[i].substring(0, letterNumber) + newLetter + words[i].substring(letterNumber + 1);
                    }
                    if ((words[i].charAt(0) != '\"' && numberLetter(words[i]) >= letterNumber
                            && words[i].charAt(letterNumber - 1) >= 'A' && words[i].charAt(letterNumber - 1) <= 'z')) {
                        if (words[i].length() > letterNumber) {
                            words[i] = words[i].substring(0, letterNumber - 1) + newLetter + words[i].substring(letterNumber);
                        } else {
                            words[i] = words[i].substring(0, letterNumber - 1) + newLetter;
                        }
                    }
                }
                sB.append(words[i]).append(' ');
            }
            return sB.toString().trim();
        }
    }

//    public static void letterReplacementByRegex(String str, char newLetter, int letterNumber) {
//        String[] words = str.split(" +");
//        String regex = "(^| |\")[a-zA-z]{" + (letterNumber) + "}[a-zA-z\"]*";
//        Pattern p = Pattern.compile(regex);
//        for (int i = 0; i < words.length; i++) {
//            if (words[i].length() >= letterNumber) {
//                Matcher m = p.matcher(words[i]);
//                if (m.find()) {
//                    String s = m.group();
//                    System.out.println(s);
//                }
//            }
//        }
//    }

    public static String replaceAtoOAfterPByStringMethod(String str) throws NullReferenceException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        if (str.isEmpty())
            return str;
        else {
            char[] chars = str.toCharArray();
            StringBuilder sB = new StringBuilder();
            for (int i = 0; i < chars.length - 1; i++) {
                if (String.valueOf(chars[i]).toLowerCase().equals("p") && chars[i + 1] == 'a') {
                    chars[i + 1] = 'o';
                }
                if (String.valueOf(chars[i]).toLowerCase().equals("p") && chars[i + 1] == 'A') {
                    chars[i + 1] = 'O';
                }
                sB.append(chars[i]);
            }
            return sB.append(chars[chars.length - 1]).toString().trim();
        }
    }

    public static String replaceAtoOAfterPByRegex(String str) throws NullReferenceException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        str = str.replaceAll("pa", "po");
        str = str.replaceAll("Pa", "Po");
        str = str.replaceAll("PA", "PO");
        return str.trim();
    }

    public static String replaceAllWordsGivenLengthAndSubstringByStringMethod(String str, String substring, int lengthWord) throws NullReferenceException, NotPositiveNumberException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        if (CheckValid.checkForNull(substring))
            throw new NullReferenceException();
        if (CheckValid.checkNotPositive(lengthWord))
            throw new NotPositiveNumberException();
        if (str.isEmpty())
            return str;
        else {
            str = str.trim();
            String[] words = str.split(" ");
            StringBuilder sB = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    if (words[i].charAt(0) == '\"' && numberLetter(words[i].substring(1)) == lengthWord
                            && words[i].charAt(1) >= 'A' && words[i].charAt(1) <= 'z') {
                        words[i] = "\"" + substring + "\"";
                    }
                    if (words[i].charAt(0) != '\"' && numberLetter(words[i]) == lengthWord
                            && words[i].charAt(0) >= 'A' && words[i].charAt(0) <= 'z') {
                        if (numberLetter(words[i]) == words[i].length())
                            words[i] = substring;
                        if (numberLetter(words[i]) < words[i].length())
                            words[i] = substring + words[i].substring(lengthWord);
                    }
                }
                sB.append(words[i]).append(" ");
            }
            return sB.toString().trim();
        }
    }

    private static int numberLetter(String word) {
        int length = 0;
        for (int i = 0; i < word.length(); i++) {
            if (word.charAt(i) >= 'A' && word.charAt(i) <= 'z') {
                length++;
            } else break;
        }
        return length;
    }

    public static String replaceAllWordsGivenLengthAndSubstringByRegex(String str, String substring, int lengthWord) throws NullReferenceException, NotPositiveNumberException {
        if (CheckValid.checkNotPositive(lengthWord))
            throw new NotPositiveNumberException();
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        if (CheckValid.checkForNull(substring))
            throw new NullReferenceException();
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "} ", " " + substring + " ");
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "}!", " " + substring + "!");
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "},", " " + substring + ",");
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "}\\.", " " + substring + ".");
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "};", " " + substring + ";");
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "}:", " " + substring + ":");
        str = str.replaceAll("(^| )[a-zA-z]{" + lengthWord + "}\\?", " " + substring + "?");
        str = str.replaceAll("\"[a-zA-z]{" + lengthWord + "}\"", "\"" + substring + "\"");
        return str.trim();
    }

    public static String deleteAllCharactersExceptSpacesThatAreNotLettersByStringMethod(String str) throws NullReferenceException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        char[] chars = str.toCharArray();
        StringBuilder sB = new StringBuilder();
        for (char ch : chars) {
            if (ch >= 'A' && ch <= 'z' || (ch == ' ' || ch == '\n'))
                sB.append(ch);
        }
        return sB.toString().trim();
    }

    public static String deleteAllCharactersExceptSpacesThatAreNotLettersByRegex(String str) throws NullReferenceException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        str = str.replaceAll("[^a-zA-z\\s]", "");
        return str.trim();
    }

    public static String deleteAllWordsGivenLengthStartingWithAConsonantByStringMethod(String str, int lengthWord) throws NullReferenceException, NotPositiveNumberException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        if (CheckValid.checkNotPositive(lengthWord))
            throw new NotPositiveNumberException();
        if (str.isEmpty())
            return str;
        else {
            str = str.trim();
            String[] words = str.split(" ");
            StringBuilder sB = new StringBuilder();
            for (int i = 0; i < words.length; i++) {
                if (!words[i].isEmpty()) {
                    char ch;
                    if (words[i].charAt(0) == '\"' && numberLetter(words[i].substring(1)) == lengthWord) {
                        ch = words[i].toLowerCase().charAt(1);
                        if (ch != 'a' && ch != 'e' && ch != 'i' && ch != 'o' && ch != 'u') {
                            words[i] = "";
                        }
                    } else if (words[i].charAt(0) != '\"' && numberLetter(words[i]) == lengthWord) {
                        ch = words[i].toLowerCase().charAt(0);
                        if (ch != 'a' && ch != 'e' && ch != 'i' && ch != 'o' && ch != 'u') {
                            if (numberLetter(words[i]) == words[i].length())
                                words[i] = "";
                            if (numberLetter(words[i]) < words[i].length())
                                words[i] = "" + words[i].substring(lengthWord);
                        }
                    }
                }
                sB.append(words[i]).append(" ");
            }
            return sB.toString().trim();
        }
    }

    public static String deleteAllWordsGivenLengthStartingWithAConsonantByRegex(String str, int lengthWord) throws NullReferenceException, NotPositiveNumberException {
        if (CheckValid.checkForNull(str))
            throw new NullReferenceException();
        if (CheckValid.checkNotPositive(lengthWord))
            throw new NotPositiveNumberException();
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "} ", "  ");
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "}!", " !");
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "},", " ,");
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "}\\.", " .");
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "};", " ;");
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "}:", " :");
        str = str.replaceAll("(^| )[^AEIOUaeiou\\d\\s]\\w{" + (lengthWord - 1) + "}\\?", " ?");
        str = str.replaceAll("(^| )\"[^AEIOUaeiou\\d]\\w{" + (lengthWord - 1) + "}\"", " ");
        return str.trim();
    }
}
