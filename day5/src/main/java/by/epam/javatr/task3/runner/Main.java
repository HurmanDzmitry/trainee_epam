package by.epam.javatr.task3.runner;

import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task3.service.Text;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {

//        String text = Receiver.readerFile("resourceText.txt");
//        String text = Receiver.SCANNER.nextLine().trim();
        String text = " s ss sss \"s\" \"ss\" \"sss\" s! ss!! sss!!! PAss pass ssp  ";

        String byStringMethod;
        String byRegex;
        System.out.println("Original:\n" + text);

        try {
            byStringMethod = Text.replaceLetterGivenCharAndLetterNumberByStringMethod(text, ' ', 2);
            System.out.println("Replace letter:\n" + byStringMethod);

            byStringMethod = Text.replaceAtoOAfterPByRegex(text);
            byRegex = Text.replaceAtoOAfterPByStringMethod(text);
            System.out.println("Replace A to O:\n" + byStringMethod);
            System.out.println(byRegex);
            System.out.println(byStringMethod.equals(byRegex));

            byStringMethod = Text.replaceAllWordsGivenLengthAndSubstringByStringMethod(text, "XXX", 2);
            byRegex = Text.replaceAllWordsGivenLengthAndSubstringByRegex(text, "XXX", 2);
            System.out.println("Replace word to substring:\n" + byStringMethod);
            System.out.println(byRegex);
            System.out.println(byStringMethod.equals(byRegex));

            byStringMethod = Text.deleteAllCharactersExceptSpacesThatAreNotLettersByStringMethod(text);
            byRegex = Text.deleteAllCharactersExceptSpacesThatAreNotLettersByRegex(text);
            System.out.println("Delete excess symbols:\n" + byStringMethod);
            System.out.println(byRegex);
            System.out.println(byStringMethod.equals(byRegex));

            byStringMethod = Text.deleteAllWordsGivenLengthStartingWithAConsonantByStringMethod(text, 2);
            byRegex = Text.deleteAllWordsGivenLengthStartingWithAConsonantByRegex(text, 2);
            System.out.println("Delete words starting with a consonant:\n" + byStringMethod);
            System.out.println(byRegex);
            System.out.println(byStringMethod.equals(byRegex));

        } catch (NullReferenceException | NotPositiveNumberException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
