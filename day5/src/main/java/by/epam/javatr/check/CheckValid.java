package by.epam.javatr.check;

public class CheckValid {

    private CheckValid() {
    }

    public static boolean checkNotPositive(int value) {
        return value <= 0;
    }

    public static boolean checkBeyondhdOfBounds(int value, int length) {
        return value >= length || value < 0;
    }

    public static boolean checkForNull(Object value) {
        return value == null;
    }
}