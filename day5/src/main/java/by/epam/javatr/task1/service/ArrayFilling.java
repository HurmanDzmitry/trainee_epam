package by.epam.javatr.task1.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.receiving_data.Receiver;
import by.epam.javatr.task1.model.Array;

import java.io.IOException;

public class ArrayFilling {

    private ArrayFilling() {
    }

    public static int[] fillingFromConsole(Array arr) throws NullReferenceException {
        if (CheckValid.checkForNull(arr)) {
            throw new NullReferenceException();
        }
        int[] array = arr.getArray();
        for (int i = 0; i < array.length; i++) {
            array[i] = Receiver.SCANNER.nextInt();
        }
        return array;
    }

    public static int[] fillingFromFile(Array arr, String fileName) throws IOException, NullReferenceException, IllegalValueException {
        if (CheckValid.checkForNull(arr)) {
            throw new NullReferenceException();
        }
        int[] array = arr.getArray();
        String readerFile = Receiver.readerFile(fileName);
        String[] numbers = readerFile.split(", ");
        for (int i = 0; i < Math.min(array.length, numbers.length); i++) {
            try {
                array[i] = Integer.parseInt(numbers[i]);
            } catch (NumberFormatException e) {
                throw new IllegalValueException("Value from file is not integer!");
            }
        }
        return array;
    }

    public static int[] fillingRandomNumber(Array arr) throws NullReferenceException {
        if (CheckValid.checkForNull(arr))
            throw new NullReferenceException();
        int[] array = arr.getArray();
        int min = -100;
        int max = +100;
        int diff = max - min;
        for (int i = 0; i < array.length; i++) {
            array[i] = Receiver.RANDOM.nextInt(diff + 1) + min;
        }
        return array;
    }
}
