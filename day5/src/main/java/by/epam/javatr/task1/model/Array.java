package by.epam.javatr.task1.model;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.BoundsOfArrayException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task1.service.Arrays;


public class Array {
    private final int length;
    private int[] array;

    public Array(int length) throws NotPositiveNumberException {
        if (CheckValid.checkNotPositive(length))
            throw new NotPositiveNumberException();
        this.length = length;
        this.array = new int[length];
    }

    public Array(int[] array) throws NullReferenceException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        this.length = array.length;
        this.array = Arrays.copyAll(array);
    }

    public int getLength() {
        return length;
    }

    public int[] getArray() throws NullReferenceException {
        return Arrays.copyAll(array);
    }

    public void setArray(int[] array) throws NullReferenceException, BoundsOfArrayException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        if (length == array.length)
            this.array = Arrays.copyAll(array);
        else
            throw new BoundsOfArrayException("Different length arrays");
    }

    public int getElement(int index) throws BoundsOfArrayException {
        if (CheckValid.checkBeyondhdOfBounds(index, length))
            throw new BoundsOfArrayException();
        return array[index];
    }

    public void changeElement(int index, int element) throws BoundsOfArrayException {
        if (CheckValid.checkBeyondhdOfBounds(index, length))
            throw new BoundsOfArrayException();
        array[index] = element;
    }

    public void selectionSort() {
        for (int i = 0; i < array.length; i++) {
            int min = array[i];
            int min_i = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    min_i = j;
                }
            }
            if (i != min_i) {
                int tmp = array[i];
                array[i] = array[min_i];
                array[min_i] = tmp;
            }
        }
    }

    public void bubbleSort() {
        for (int i = array.length - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (array[j] > array[j + 1]) {
                    int tmp = array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = tmp;
                }
            }
        }
    }

    public void insertionSort() {
        for (int left = 0; left < array.length; left++) {
            int value = array[left];
            int i = left - 1;
            for (; i >= 0; i--) {
                if (value < array[i]) {
                    array[i + 1] = array[i];
                } else {
                    break;
                }
            }
            array[i + 1] = value;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Array array1 = (Array) o;
        return length == array1.length &&
                ((array == array1.array) || (array != null && Arrays.equals(array, array1.array)));
    }

    @Override
    public int hashCode() {
        return (31 * length + ((array == null) ? 0 : Arrays.hashCode(array)));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "length=" + length +
                ", array=" + Arrays.toString(array) +
                '}';
    }
}
