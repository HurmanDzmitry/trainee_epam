package by.epam.javatr.task1.service;

import by.epam.javatr.check.CheckValid;
import by.epam.javatr.exception.BoundsOfArrayException;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NullReferenceException;

public class Arrays {
    private Arrays() {
    }

    public static int getIndexFromSortedArray(int[] array, int element) throws NullReferenceException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int first = 0;
        int last = array.length - 1;
        while (first <= last) {
            int mid = (first + last) / 2;
            int midVal = array[mid];
            if (midVal < element)
                first = mid + 1;
            else if (midVal > element)
                last = mid - 1;
            else
                return mid;
        }
        return -1;
    }

    public static int indexOf(int[] array, int element) {
        for (int i = 0; i < array.length; i++) {
            if (element == array[i])
                return i;
        }
        return -1;
    }

    public static int maxElement(int[] array) throws NullReferenceException, BoundsOfArrayException, IllegalValueException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int[] newArray = copyAll(array);
        quickSort(newArray, 0, newArray.length - 1);
        return newArray[array.length - 1];
    }

    private static void quickSort(int[] array, int leftBorder, int rightBorder) throws NullReferenceException, BoundsOfArrayException, IllegalValueException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        if (rightBorder > leftBorder) {
            if (CheckValid.checkBeyondhdOfBounds(leftBorder, array.length) ||
                    CheckValid.checkBeyondhdOfBounds(rightBorder, array.length))
                throw new BoundsOfArrayException();
        } else
            throw new IllegalValueException("Left border less than right border!");
        int leftMarker = leftBorder;
        int rightMarker = rightBorder;
        int pivot = array[(leftMarker + rightMarker) / 2];
        do {
            while (array[leftMarker] < pivot) {
                leftMarker++;
            }
            while (array[rightMarker] > pivot) {
                rightMarker--;
            }
            if (leftMarker <= rightMarker) {
                if (leftMarker < rightMarker) {
                    int tmp = array[leftMarker];
                    array[leftMarker] = array[rightMarker];
                    array[rightMarker] = tmp;
                }
                leftMarker++;
                rightMarker--;
            }
        } while (leftMarker <= rightMarker);
        if (leftMarker < rightBorder) {
            quickSort(array, leftMarker, rightBorder);
        }
        if (leftBorder < rightMarker) {
            quickSort(array, leftBorder, rightMarker);
        }
    }

    public static int minElement(int[] array) throws NullReferenceException, BoundsOfArrayException, IllegalValueException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int[] newArray = copyAll(array);
        quickSort(newArray, 0, newArray.length - 1);
        return newArray[0];
    }

    public static int[] copyAll(int[] array) throws NullReferenceException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int[] newArray = new int[array.length];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

    public static int[] getSimpleNumber(int[] array) throws NullReferenceException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int[] newArray = copyAll(array);
        int amountOfSimpleNumbers = newArray.length;
        for (int i = 0; i < newArray.length; i++) {
            if (newArray[i] < 2) {
                newArray[i] = -1;
                amountOfSimpleNumbers--;
            }
            for (int j = 2; j < newArray[i]; j++) {
                if (newArray[i] % j == 0 && newArray[i] != 2) {
                    newArray[i] = -1;
                    amountOfSimpleNumbers--;
                    break;
                }
            }
        }
        int[] simpleNumber = new int[amountOfSimpleNumbers];
        int index = 0;
        for (int i1 : newArray) {
            if (i1 != -1) {
                simpleNumber[index] = i1;
                index++;
            }
        }
        return simpleNumber;
    }

    public static int[] getFibonacciNumber(int[] array) throws NullReferenceException, BoundsOfArrayException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int[] newArray = copyAll(array);
        int index = 0;
        for (int i : newArray) {
            if (i >= 0 && (isPerfectSquare(5 * i * i + 4)
                    || isPerfectSquare(5 * i * i - 4))) {
                newArray[index] = i;
                index++;
            }
        }
        return getPartOfArray(newArray, index);
    }

    public static int[] getThree_digitNumberWithDifferentDigits(int[] array) throws NullReferenceException, BoundsOfArrayException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int[] newArray = copyAll(array);
        int index = 0;
        for (int i : newArray) {
            if (isThree_digitNumber(i) && isDifferentDigitsInNumber(i)) {
                newArray[index] = i;
                index++;
            }
        }
        return getPartOfArray(newArray, index);
    }

    private static boolean isPerfectSquare(int num) {
        int n = (int) (Math.sqrt(num));
        return (n * n == num);
    }

    private static int[] getPartOfArray(int[] array, int length) throws NullReferenceException, BoundsOfArrayException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        if (CheckValid.checkBeyondhdOfBounds(length, array.length + 1))
            throw new BoundsOfArrayException();
        int[] partOfArray = new int[length];
        for (int i = 0; i < partOfArray.length; i++) {
            partOfArray[i] = array[i];
        }
        return partOfArray;
    }

    private static boolean isThree_digitNumber(int value) {
        return (value > 99 && value < 1000);
    }

    private static boolean isDifferentDigitsInNumber(int value) {
        String number = String.valueOf(value);
        char[] digits = number.toCharArray();
        boolean isDifferent = true;
        for (int i = 0; i < digits.length; i++) {
            for (int j = 0; j < digits.length; j++) {
                if (digits[i] == digits[j] && i != j) {
                    isDifferent = false;
                    break;
                }
            }
        }
        return isDifferent;
    }

    private static boolean equalsSupport(int[] a, int[] a2) {
        boolean b = true;
        for (int i = 0; i < a.length; i++) {
            if (a[i] != a2[i])
                b = false;
        }
        return b;
    }

    public static int sum(int[] array) throws NullReferenceException {
        if (CheckValid.checkForNull(array))
            throw new NullReferenceException();
        int sum = 0;
        for (int i : array) {
            sum += i;
        }
        return sum;
    }

    public static boolean equals(int[] a, int[] a2) {
        if (a == null || a2 == null)
            return false;
        if (a == a2) {
            return true;
        }
        if (a.length != a2.length)
            return false;
        return equalsSupport(a, a2);
    }

    public static int hashCode(int[] a) {
        if (a == null)
            return 0;
        int result = 1;
        for (int element : a)
            result = 31 * result + element;

        return result;
    }

    public static String toString(int[] a) {
        if (a == null)
            return "null";
        if (a.length == 0)
            return "[]";
        StringBuilder b = new StringBuilder();
        b.append('[');
        for (int i = 0; ; i++) {
            b.append(a[i]);
            if (i == a.length - 1)
                return b.append(']').toString();
            b.append(", ");
        }
    }
}