package by.epam.javatr.task1.runner;

import by.epam.javatr.exception.BoundsOfArrayException;
import by.epam.javatr.exception.IllegalValueException;
import by.epam.javatr.exception.NotPositiveNumberException;
import by.epam.javatr.exception.NullReferenceException;
import by.epam.javatr.task1.model.Array;
import by.epam.javatr.task1.service.ArrayFilling;
import by.epam.javatr.task1.service.Arrays;

public class Main {
    public static void main(String[] args) {
        try {
            /*Init Array, set only length*/
            Array array = new Array(10);
            /*Print*/
            System.out.println("Array: " + Arrays.toString(array.getArray()));
            /*Filling*/
            array.setArray(ArrayFilling.fillingRandomNumber(array));
//            ArrayFilling.fillingFromConsole(array);
//            ArrayFilling.fillingFromConsole(array);
            /*Print*/
            System.out.println("Array after filling: " + Arrays.toString(array.getArray()));
            /*Get length*/
            System.out.println("Length: " + array.getLength());
            /*Get element by index, here is the first element*/
            System.out.println("First element before change: " + array.getElement(0));
            /*Change element by index, here is the first element*/
            array.changeElement(0, 0);
            /*Get element by index, here is the first element again*/
            System.out.println("First element after change: " + array.getElement(0));
            /*Print*/
            System.out.println("Array: " + Arrays.toString(array.getArray()));
            /*Get index by value*/
            System.out.println("Index by value: " + Arrays.indexOf(array.getArray(), 0));
            /*Get max value*/
            System.out.println("Max value: " + Arrays.maxElement(array.getArray()));
            /*Get min value*/
            System.out.println("Min value: " + Arrays.minElement(array.getArray()));
            /*Print*/
            System.out.println("Array: " + Arrays.toString(array.getArray()));
            /*Get simple numbers*/
            System.out.println("Simple numbers: " + Arrays.toString(Arrays.getSimpleNumber(array.getArray())));
            /*Get fibonacci numbers*/
            System.out.println("Fibonacci numbers: " + Arrays.toString(Arrays.getFibonacciNumber(array.getArray())));
            /*Get three-digit number with different digits*/
            System.out.println("Three-digit number with different digits: " + Arrays.toString(Arrays.getThree_digitNumberWithDifferentDigits(array.getArray())));
            /*Sort array*/
            array.bubbleSort();
            /*Print*/
            System.out.println("Array: " + Arrays.toString(array.getArray()));
            /*Get index by value from sorted array*/
            System.out.println("Index by value: " + Arrays.getIndexFromSortedArray(array.getArray(), 0));

        } catch (NotPositiveNumberException | NullReferenceException | BoundsOfArrayException | IllegalValueException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
