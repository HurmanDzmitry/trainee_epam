package by.epam.javatr.receiving_data;

import java.io.FileReader;
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class Receiver {

    public static final Scanner SCANNER = new Scanner(System.in);
    public static final Random RANDOM = new Random();

    private Receiver() {
    }

    public static String readerFile(String fileName) throws IOException {
        StringBuilder sB = new StringBuilder();
        try (FileReader reader = new FileReader(fileName)) {
            int c;
            while ((c = reader.read()) != -1) {
                sB.append((char) c);
            }
        }
        return sB.toString().trim();
    }
}
