package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.AuthorDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.Author;
import org.dbunit.Assertion;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.DataSetException;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.ITable;
import org.dbunit.dataset.filter.DefaultColumnFilter;
import org.dbunit.util.fileloader.DataFileLoader;
import org.dbunit.util.fileloader.FlatXmlDataFileLoader;
import org.junit.Assert;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.TestInstance.Lifecycle;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@TestInstance(Lifecycle.PER_CLASS)
class AuthorDaoImplTest {

    private final String CLEAN = "DELETE FROM authors;";
    private final String FILL = "INSERT INTO authors VALUES (1, 'A'),(2, 'B');";

    private final String PATH = "/dao/impl/author/";
    private final String NAME_TABLE = "authors";

    private final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();
    private final AuthorDao AUTHOR_DAO = DaoFactory.getInstance().getAuthorDao();
    private final DataFileLoader LOADER = new FlatXmlDataFileLoader();

    @BeforeAll
    void setUp() {
        try {
            CONNECTION_POOL.initPoolData();
        } catch (ConnectionPoolException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    @AfterAll
    void tearDown() {
        CONNECTION_POOL.dispose();
    }

    @BeforeEach
    void fillDB() throws ConnectionPoolException, SQLException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            ps = cn.prepareStatement(FILL);
            ps.executeUpdate();
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @AfterEach
    void cleanDB() throws ConnectionPoolException, SQLException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            ps = cn.prepareStatement(CLEAN);
            ps.executeUpdate();
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Test
    void save() throws DaoException, DatabaseUnitException, ConnectionPoolException {
        Author actualAuthor = new Author("C");
        ITable expectedTable = getExpectedTable(PATH + "save.xml");
        Author expectedAuthor = AUTHOR_DAO.save(actualAuthor);
        ITable actualTable = getActualTable(expectedTable);

        Assertion.assertEquals(expectedTable, actualTable);
        Assert.assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    void findAll() throws DaoException {
        List<Author> expectedAuthors = new ArrayList<>();
        {
            expectedAuthors.add(new Author(1, "A"));
            expectedAuthors.add(new Author(2, "B"));
        }
        List<Author> actualAuthors = AUTHOR_DAO.findAll();

        Assert.assertEquals(expectedAuthors, actualAuthors);
    }

    @Test
    void update() throws DatabaseUnitException, DaoException, ConnectionPoolException {
        Author actualAuthor = new Author(2, "Z");
        ITable expectedTable = getExpectedTable(PATH + "update.xml");
        Author expectedAuthor = AUTHOR_DAO.update(actualAuthor);
        ITable actualTable = getActualTable(expectedTable);

        Assertion.assertEquals(expectedTable, actualTable);
        Assert.assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    void deleteById() throws DatabaseUnitException, ConnectionPoolException, DaoException {
        ITable expectedTable = getExpectedTable(PATH + "deleteById.xml");
        AUTHOR_DAO.deleteById(2);
        ITable actualTable = getActualTable(expectedTable);

        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    void deleteByName() throws DatabaseUnitException, ConnectionPoolException, DaoException {
        ITable expectedTable = getExpectedTable(PATH + "deleteByName.xml");
        AUTHOR_DAO.deleteByName("B");
        ITable actualTable = getActualTable(expectedTable);

        Assertion.assertEquals(expectedTable, actualTable);
    }

    @Test
    void searchByPartName() throws DaoException {
        List<Author> expectedAuthor = new ArrayList<>();
        {
            expectedAuthor.add(new Author(1, "A"));
        }
        List<Author> actualAuthor = AUTHOR_DAO.searchByPartName("a");

        Assert.assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    void findById() throws DaoException {
        int id = 1;
        Author expectedAuthor = new Author(id, "A");
        Author actualAuthor = AUTHOR_DAO.findById(id);

        Assert.assertEquals(expectedAuthor, actualAuthor);
    }

    @Test
    void findByName() throws DaoException {
        String name = "A";
        Author expectedAuthor = new Author(1, name);
        Author actualAuthor = AUTHOR_DAO.findByName(name);

        Assert.assertEquals(expectedAuthor, actualAuthor);
    }

    private IDataSet getDataSet() throws ConnectionPoolException, DatabaseUnitException {
        Connection jdbcConnection = CONNECTION_POOL.takeConnection();
        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection);

        QueryDataSet databaseDataSet = new QueryDataSet(connection);
        databaseDataSet.addTable(NAME_TABLE);

        return databaseDataSet;
    }

    private ITable getExpectedTable(String file) throws DataSetException {
        IDataSet expectedDataSet = LOADER.load(file);
        return expectedDataSet.getTable(NAME_TABLE);
    }

    private ITable getActualTable(ITable expectedTable) throws DatabaseUnitException, ConnectionPoolException {
        IDataSet actualDataSet = getDataSet();
        ITable actualTable = actualDataSet.getTable(NAME_TABLE);
        actualTable = DefaultColumnFilter.includedColumnsTable(actualTable,
                expectedTable.getTableMetaData().getColumns());
        return actualTable;
    }
}