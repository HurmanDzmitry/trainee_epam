CREATE TABLE roles
(
  id_role   INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_role VARCHAR(30) NOT NULL UNIQUE
);


CREATE TABLE users
(
  id_user       INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_user     VARCHAR(30) NOT NULL,
  surname_user  VARCHAR(30) NOT NULL,
  date_of_birth DATE        NOT NULL,
  login         VARCHAR(30) NOT NULL UNIQUE,
  password      VARCHAR(50) NOT NULL,
  email         VARCHAR(30) NOT NULL UNIQUE,
  id_role       INT         NOT NULL,
  FOREIGN KEY (id_role) REFERENCES roles (id_role)
);


CREATE TABLE librarians
(
  id_librarian           INT    NOT NULL primary key,
  date_of_beginning_work DATE   NOT NULL,
  salary                 DOUBLE NOT NULL,
  FOREIGN KEY (id_librarian) REFERENCES users (id_user)
);


CREATE TABLE subscriber_statuses
(
  id_subscriber_status INT                               NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_status          ENUM ('usual','blocked','luxury') NOT NULL UNIQUE,
  limit_reservation    INT                               NOT NULL,
  limit_taking         INT                               NOT NULL,
  days_for_reservation INT                               NOT NULL,
  days_for_return      INT                               NOT NULL
);


CREATE TABLE subscribers
(
  id_subscriber        INT NOT NULL primary key,
  id_subscriber_status INT NOT NULL,
  FOREIGN KEY (id_subscriber) REFERENCES users (id_user),
  FOREIGN KEY (id_subscriber_status) REFERENCES subscriber_statuses (id_subscriber_status)
);


CREATE TABLE books
(
  id_book     INT                                          NOT NULL AUTO_INCREMENT PRIMARY KEY,
  title       VARCHAR(100)                                 NOT NULL,
  year        SMALLINT                                     NOT NULL,
  quantity    INT                                          NOT NULL,
  book_status ENUM ('only_here','everywhere','not_issued') NOT NULL,
  image_link  VARCHAR(255)
);


CREATE TABLE authors
(
  id_author   INT         NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name_author VARCHAR(30) NOT NULL UNIQUE
);


CREATE TABLE books_has_authors
(
  id_author INT NOT NULL,
  id_book   INT NOT NULL,
  FOREIGN KEY (id_author) REFERENCES authors (id_author),
  FOREIGN KEY (id_book) REFERENCES books (id_book)
);


CREATE TABLE book_deliveries
(
  id_delivery             INT                                    NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_subscriber           INT                                    NOT NULL,
  id_librarian            INT                                    NOT NULL,
  id_book                 INT                                    NOT NULL,
  date_of_issue           DATE                                   NOT NULL,
  date_of_return_actual   DATE,
  date_of_return_expected DATE                                   NOT NULL,
  delivery_status         ENUM ('in_action','on_time','expired') NOT NULL,
  kind_place              ENUM ('here','home')                   NOT NULL,
  FOREIGN KEY (id_subscriber) REFERENCES subscribers (id_subscriber),
  FOREIGN KEY (id_librarian) REFERENCES librarians (id_librarian),
  FOREIGN KEY (id_book) REFERENCES books (id_book)
);


CREATE TABLE book_reservations
(
  id_reservation     INT                                       NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_subscriber      INT                                       NOT NULL,
  id_book            INT                                       NOT NULL,
  date_of_begin      DATE                                      NOT NULL,
  date_of_reset      DATE                                      NOT NULL,
  reservation_status ENUM ('in_action','performed','not_done') NOT NULL,
  FOREIGN KEY (id_subscriber) REFERENCES subscribers (id_subscriber),
  FOREIGN KEY (id_book) REFERENCES books (id_book)
);


INSERT INTO roles
VALUES (1, 'librarian'),
       (2, 'subscriber');


INSERT INTO users
VALUES (1, 'Дмитрий', 'Гурман', '1992-05-24', 'hurman_log', 'atYx4uv-KxbLlfnH9MyorRVmdsNfJerHtVMnya7RlKU',
        'hurman@mail.com', 1),
       (2, 'Илья', 'Муромец', '1980-01-01', 'murom_rus', '54lSgImWymLT7vmSeXwHwr2W0m7BzOFEoLVviS_EGZw',
        'i.murom777@gmail.com', 2),
       (3, 'Алеша', 'Попович', '1999-07-07', 'popcorn', 'TwqTJDnOTXE6-ayoROnvz1LVBCQ3B9HpgIfoDIaGmGE',
        'a_popovich@mail.ru', 2),
       (4, 'Добрыня', 'Никитич', '1990-09-30', 'bogatir', 'LzcMYCKLbgO4Om4T9CkZOjLU0eAiCThfZqc-SvYPys0',
        'd-nikitich@tut.by', 2);
# "Qwerty12345",
# "12345_PAROL",
# "Abcde98765",
# "Bogatir0"


INSERT INTO subscriber_statuses
VALUES (1, 'usual', 1, 3, 1, 15),
       (2, 'blocked', 0, 0, 0, 0),
       (3, 'luxury', 2, 5, 3, 30);


INSERT INTO subscribers
VALUES (2, 1),
       (3, 1),
       (4, 3);


INSERT INTO librarians
VALUES (1, '2019-11-01', 1000);


INSERT INTO authors
VALUES (1, 'Дэн Браун'),
       (2, 'Джоан Роулинг'),
       (3, 'Брюс Эккель'),
       (4, 'Герберт Шилдт'),
       (5, 'Кэти Сьерра'),
       (6, 'Берт Бейтс'),
       (7, 'Эрик Фримен');


INSERT INTO books
VALUES (1, 'Ангелы и Демоны', 2005, 10, '2', 'resources/img/book/1.jpg'),
       (2, 'Код ДаВинчи', 2010, 10, '2', 'resources/img/book/2.jpg'),
       (3, 'Инферно', 2015, 10, '2', 'resources/img/book/3.jpg'),
       (4, 'Гарри Поттер и философский камень', 2000, 5, '2', 'resources/img/book/4.jpg'),
       (5, 'Философия Java', 2019, 2, '2', 'resources/img/book/5.jpg'),
       (6, 'Java. Полное руководство', 2018, 3, '2', 'resources/img/book/6.jpg'),
       (7, 'Изучаем Java', 2015, 3, '2', 'resources/img/book/7.jpg'),
       (8, 'Head First. Паттерны проектирования', 2019, 5, '3', 'resources/img/book/8.jpg');


INSERT INTO books_has_authors
VALUES (1, 1),
       (1, 2),
       (1, 3),
       (2, 4),
       (3, 5),
       (4, 6),
       (5, 7),
       (6, 7),
       (5, 8),
       (6, 8),
       (7, 8);


INSERT INTO book_reservations
VALUES (1, 2, 1, '2020-01-01', '2020-01-03', '2');


INSERT INTO book_deliveries
VALUES (1, 2, 1, 1, '2020-01-01', '2020-01-01', '2020-01-01', '2', '1'),
       (2, 3, 1, 4, '2020-02-01', '2020-02-25', '2020-02-25', '2', '2'),
       (3, 4, 1, 5, '2020-02-01', '2020-02-10', '2020-02-15', '2', '2'),
       (4, 3, 1, 1, '2020-03-01', '2020-03-10', '2020-03-30', '2', '2');
