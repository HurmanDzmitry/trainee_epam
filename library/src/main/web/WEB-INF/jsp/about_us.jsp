<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.about_us.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.about_us.info.address" var="address"/>
    <fmt:message bundle="${loc}" key="local.about_us.info.chief" var="chief"/>
    <fmt:message bundle="${loc}" key="local.about_us.info.phone" var="phone"/>
    <fmt:message bundle="${loc}" key="local.about_us.info.mail" var="mail"/>
    <fmt:message bundle="${loc}" key="local.about_us.info.site" var="site"/>
    <title>${title}</title>
    <c:set var="map_address" value="https://api-maps.yandex.ru/2.1/?apikey=test&lang=${sessionScope.locale}"/>
    <script src="${map_address}"></script>
    <script src="resources/js/map.js"></script>
</head>
<body>
<header>
    <jsp:include page="part/header.jsp"/>
</header>
<main>
    <div id="map" style="width: 600px; height: 400px"></div>
    <br>
    <div>
        ${address}<br>
        ${chief}<br>
        ${phone} +375-17-123-456-789<br>
        ${mail} library@mail.by<br>
        ${site} <a href="${Uri.MAIN}">library.by</a>
    </div>
</main>
<jsp:include page="part/footer.jsp"/>
</body>
</html>
