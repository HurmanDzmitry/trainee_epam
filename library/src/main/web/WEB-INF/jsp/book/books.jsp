<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.BookStatus" %>
<%@ page import="by.epam.javatr.entity.enumeration.Role" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.books.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.books.sort_by.by_title" var="by_title"/>
    <fmt:message bundle="${loc}" key="local.books.sort_by.by_year" var="by_year"/>
    <fmt:message bundle="${loc}" key="local.books.sort_by.by_quantity" var="by_quantity"/>
    <fmt:message bundle="${loc}" key="local.books.sort_how.ascending" var="ascending"/>
    <fmt:message bundle="${loc}" key="local.books.sort_how.descending" var="descending"/>
    <fmt:message bundle="${loc}" key="local.books.status.all" var="all"/>
    <fmt:message bundle="${loc}" key="local.books.status.home" var="home"/>
    <fmt:message bundle="${loc}" key="local.books.status.spot" var="spot"/>
    <fmt:message bundle="${loc}" key="local.books.status.not_issued" var="not_issued"/>
    <fmt:message bundle="${loc}" key="local.books.button.apply" var="apply"/>
    <fmt:message bundle="${loc}" key="local.books.button.add_book" var="add_book"/>
    <fmt:message bundle="${loc}" key="local.books.table.name" var="table_name"/>
    <fmt:message bundle="${loc}" key="local.books.form.name" var="form_name"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <form method="get" action="${Uri.BOOKS}">
        <p>${form_name}</p>
        <p><label>
            <select name="${ParameterName.SORT_BY}">
                <option selected value="${ParameterName.TITLE}">${by_title}</option>
                <option value="${ParameterName.YEAR}">${by_year}</option>
                <option value="${ParameterName.QUANTITY}">${by_quantity}</option>
            </select>
        </label>
            <label>
                <select name="${ParameterName.SORT_HOW}">
                    <option selected value="${ParameterName.ASC}">${ascending}</option>
                    <option value="${ParameterName.DESC}">${descending}</option>
                </select>
            </label></p>
        <p><label>
            <input type="radio" id="0" checked name="${ParameterName.STATUS}"
                   value="${ParameterName.ALL}"/><label for="0">${all}</label>
            <input type="radio" id="1" name="${ParameterName.STATUS}"
                   value="${BookStatus.EVERYWHERE.name().toLowerCase()}"/><label for="1">${home}</label>
            <input type="radio" id="2" name="${ParameterName.STATUS}"
                   value="${BookStatus.ONLY_HERE.name().toLowerCase()}"/><label for="2">${spot}</label>
            <input type="radio" id="3" name="${ParameterName.STATUS}"
                   value="${BookStatus.NOT_ISSUED.name().toLowerCase()}"/><label for="3">${not_issued}</label>
        </label></p>
        <p><input type="submit" value="${apply}"></p>
    </form>
    <br>
    <c:if test="${sessionScope.get(ParameterName.ROLE) == Role.LIBRARIAN}">
        <form action="${Uri.ADD_BOOK_BLANK}">
            <button>${add_book}</button>
        </form>
    </c:if>
    <c:set var="numCols" value="3"/>
    <c:set var="numRows" value="3"/>
    <c:set var="rowCount" value="0"/>
    <table width='100%' cellspacing="10px" style="table-layout: fixed;">
        <caption><h3>${table_name}</h3></caption>
        <colgroup>
            <col width="10%">
            <col width="23%">
            <col width="10%">
            <col width="23%">
            <col width="10%">
            <col width="23%">
        </colgroup>
        <tr>
            <c:forEach var="book" items="${books}" varStatus="status" end="${on_page-1}">
            <c:if test="${rowCount lt numRows}">
            <td>
                <a href="${Uri.BOOK}?${ParameterName.ID}=${book.idBook}">
                    <img src="${book.imageLink}" alt="${book.title}"></a>
            </td>
            <td>
                    ${book.title}<br><br>
                <c:forEach var="author" items="${book.authors}">
                    <a href="${Uri.AUTHOR}?${ParameterName.ID}=${author.idAuthor}"
                       style="color:black; text-decoration:underline">${author.nameAuthor}, </a>
                </c:forEach>
                    ${book.year}
            </td>
            <c:if test="${status.count ne 0 && status.count % numCols == 0}">
            <c:set var="rowCount" value="${rowCount + 1}"/>
        </tr>
        <tr>
            </c:if>
            </c:if>
            </c:forEach>
        </tr>
    </table>
    <span>
<c:forEach begin="1" end="${pages}" varStatus="loop">
    | <a
        href="${Uri.BOOKS}?${ParameterName.PAGE}=${loop.current}&${requestScope['javax.servlet.forward.query_string'].replaceFirst("page=[\\d]+&?","")}"
        style="color: black; font-size: 1.5em;">${loop.current}</a>
</c:forEach>|
</span>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
