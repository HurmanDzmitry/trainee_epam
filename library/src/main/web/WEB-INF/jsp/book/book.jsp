<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.Role" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.book.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.book.add_reservation" var="add_reservation"/>
    <fmt:message bundle="${loc}" key="local.account.message.reserve_fail" var="reserve_fail"/>
    <fmt:message bundle="${loc}" key="local.account.message.wrong_quantity" var="wrong_quantity"/>
    <fmt:message bundle="${loc}" key="local.account.message.not_issued" var="not_issued"/>
    <fmt:message bundle="${loc}" key="local.account.message.confirm" var="confirm"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <br>
    <table width='50%' cellspacing="10px" style="table-layout: fixed;">
        <colgroup>
            <col width="35%">
            <col width="65%">
        </colgroup>
        <tr>
            <c:set var="book" value="${book}"/>
            <td>
                <p><img src="${book.imageLink}" alt="${book.title}" width="30%"></p>
            </td>
            <td>
                ${book.title}<br><br>
                <c:forEach var="author" items="${book.authors}">
                    <a href="${Uri.AUTHOR}?${ParameterName.ID}=${author.idAuthor}"
                       style="color:black; text-decoration:underline">${author.nameAuthor}, </a>
                </c:forEach>
                ${book.year}
            </td>
        </tr>
    </table>
    <br>
    <c:if test="${sessionScope.get(ParameterName.ROLE) == Role.SUBSCRIBER}">
        <form action="${Uri.ADD_RESERVATION}">
            <input type="hidden" name="${ParameterName.ID}" value="${requestScope.book.idBook}"/>
            <button onclick="return confirm('${confirm}' + '\n' +
                    '<datefmt:dateFormatTag value="${requestScope.reset_date}"/>')">${add_reservation}</button>
        </form>
    </c:if>
    <c:if test="${not empty requestScope.message}">
        ${reserve_fail}
        <c:choose>
            <c:when test="${requestScope.message eq 'wrong_quantity'}">
                ${wrong_quantity}
            </c:when>
            <c:when test="${requestScope.message eq 'not_issued'}">
                ${not_issued}
            </c:when>
        </c:choose>
    </c:if>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
