<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.BookStatus" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.add_book.title" var="title_book"/>
    <fmt:message bundle="${loc}" key="local.add_book.head" var="head"/>
    <fmt:message bundle="${loc}" key="local.add_book.message.not_add" var="not_add"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.title" var="title_book"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.year" var="year"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.quantity" var="quantity"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.status" var="status"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.image" var="image"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.author" var="author"/>
    <fmt:message bundle="${loc}" key="local.books.status.home" var="home"/>
    <fmt:message bundle="${loc}" key="local.books.status.spot" var="spot"/>
    <fmt:message bundle="${loc}" key="local.books.status.not_issued" var="not_issued"/>
    <fmt:message bundle="${loc}" key="local.add_book.button.new_author" var="new_author"/>
    <fmt:message bundle="${loc}" key="local.add_book.button.submit" var="submit"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.message.invalid_title" var="invalid_title"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.message.invalid_year" var="invalid_year"/>
    <fmt:message bundle="${loc}" key="local.add_book.form.message.invalid_quantity" var="invalid_quantity"/>
    <title>${title}</title>
    <script src="${pageContext.request.contextPath}/resources/js/form_add_some_authors_for_book.js"></script>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${head}</h3>
    <c:if test="${not empty requestScope.message}">
        ${not_add}<br>
        ${requestScope.message}
    </c:if>
    <br>
    <form method="post" action="${Uri.ADD_BOOK}" enctype="multipart/form-data" onclick="setQuantityAuthors()">
        <label>${title_book}:
            <input type="text" name="${ParameterName.TITLE}" pattern="^([A-Za-z\w .,:;!?]|[а-яА-Я\w .,:;!?]){1,100}$"
                   maxlength="100"
                   oninvalid="this.setCustomValidity('${invalid_title}')"
                   oninput="this.setCustomValidity('')" required><br>
        </label>
        <label>${year}:
            <input type="number" name="${ParameterName.YEAR}"
                   min="1900" max="${LocalDate.now().getYear()}"
                   oninvalid="this.setCustomValidity('${invalid_year}')"
                   oninput="this.setCustomValidity('')" required><br>
        </label>
        <label>${quantity}:
            <input type="number" name="${ParameterName.QUANTITY}"
                   min="1" max="100"
                   oninvalid="this.setCustomValidity('${invalid_quantity}')"
                   oninput="this.setCustomValidity('')" required><br>
        </label>
        <label>${status}:
            <input type="radio" id="1" name="${ParameterName.STATUS}"
                   value="${BookStatus.EVERYWHERE.name().toLowerCase()}" required/><label for="1">${home}</label>
            <input type="radio" id="2" name="${ParameterName.STATUS}"
                   value="${BookStatus.ONLY_HERE.name().toLowerCase()}"/><label for="2">${spot}</label>
            <input type="radio" id="3" name="${ParameterName.STATUS}"
                   value="${BookStatus.NOT_ISSUED.name().toLowerCase()}"/><label for="3">${not_issued}</label><br>
        </label>
        <label>${image}:
            <input type="file" name="image" accept="image/*"><br>
        </label>
        <div id="parentId">
            <label>${author}:
                <nobr><input list="authors" name="${ParameterName.ID}1" type="text" pattern="\d+" required/>
                    <datalist id="authors">
                        <c:forEach var="author" items="${authors}">
                        <option id="${author.idAuthor}" value="${author.idAuthor}"><label
                                for="${author.idAuthor}">${author.nameAuthor}</label>
                            </c:forEach>
                    </datalist>
                    <a style="color:green;" onclick="return addField()" href="#">[+]</a>
                    <a style="color:black; border: 1px solid grey; text-decoration:none;"
                       href="${Uri.ADD_AUTHOR_BLANK}">${new_author}</a>
                </nobr>
            </label>
        </div>
        <input id="js_var" type="hidden" name="${ParameterName.QUANTITY_AUTHORS}"/>
        <br>
        <button type="submit">${submit}</button>
    </form>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
