<%@ page contentType="text/html;charset=UTF-8" isELIgnored="false" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.DeliveryPlace" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.head" var="head"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.subscriber" var="subscriber"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.librarian" var="librarian"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.book" var="book"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.issue_date" var="issue_date"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.return_date" var="return_date"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.place" var="place"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.submit" var="submit"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.here" var="here"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.home" var="home"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.confirm" var="confirm"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.message.invalid_input" var="invalid_input"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.message.delivery_fail" var="delivery_fail"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.message.blocked" var="blocked"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.message.wrong_quantity" var="wrong_quantity"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.message.not_issued" var="not_issued"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.message.only_here" var="only_here"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.message.limit" var="limit"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${head}</h3>
    <br>
    <c:if test="${not empty requestScope.message}">
        ${delivery_fail}
        <c:choose>
            <c:when test="${requestScope.message eq 'blocked'}">
                ${blocked}
            </c:when>
            <c:when test="${requestScope.message eq 'wrong_quantity'}">
                ${wrong_quantity}${requestScope.quantity}
            </c:when>
            <c:when test="${requestScope.message eq 'not_issued'}">
                ${not_issued}
            </c:when>
            <c:when test="${requestScope.message eq 'only_here'}">
                ${only_here}
            </c:when>
            <c:when test="${requestScope.message eq 'limit'}">
                ${limit}
            </c:when>
        </c:choose>
    </c:if>
    <form method="post" action="${Uri.ADD_DELIVERY_CONFIRM_BLANK}">
        <label>${subscriber}
            <c:choose>
            <c:when test="${empty requestScope.subscriber}">
            <input list="subscriber" name="${ParameterName.ID_SUBSCRIBER}" type="text" pattern="\d+"
                   oninvalid="this.setCustomValidity('${invalid_input}')"
                   oninput="this.setCustomValidity('')" required/>
            <datalist id="subscriber">
                <c:forEach var="subscriber" items="${subscribers}">
                <option id="${subscriber.idUser}" value="${subscriber.idUser}"><label
                        for="${subscriber.idUser}">${subscriber.nameUser} ${subscriber.surnameUser}</label>
                    </c:forEach>
            </datalist>
        </label>
        </c:when>
        <c:otherwise>
            <input type="hidden" value="${requestScope.subscriber.idUser}" name="${ParameterName.ID_SUBSCRIBER}">
            <c:out value="${requestScope.subscriber.nameUser} ${requestScope.subscriber.surnameUser}"/>
        </c:otherwise>
        </c:choose>
        <br>
        <label>${book}
            <c:choose>
            <c:when test="${empty requestScope.book}">
            <input list="book" name="${ParameterName.ID_BOOK}" type="text" pattern="\d+"
                   oninvalid="this.setCustomValidity('${invalid_input}')"
                   oninput="this.setCustomValidity('')" required
                   value="${requestScope.book.idBook}"/>
            <datalist id="book">
                <c:forEach var="book" items="${books}">
                <option id="${book.idBook}" value="${book.idBook}"><label
                        for="${book.idBook}">${book.title}</label>
                    </c:forEach>
            </datalist>
        </label>
        </c:when>
        <c:otherwise>
            <input type="hidden" value="${requestScope.book.idBook}" name="${ParameterName.ID_BOOK}">
            <c:out value="${requestScope.book.title}"/>
        </c:otherwise>
        </c:choose>
        <br>
        ${issue_date} <datefmt:dateFormatTag value="${LocalDate.now()}"/><br>
        <label>${place}
            <input type="radio" id="here" name="${ParameterName.PLACE}" value="${DeliveryPlace.HERE.name()}" required/>
            <label for="here">${here}</label>
            <input type="radio" id="home" name="${ParameterName.PLACE}" value="${DeliveryPlace.HOME.name()}"/>
            <label for="home">${home}</label>
        </label>
        <br><br>
        <button type="submit">${submit}</button>
    </form>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
