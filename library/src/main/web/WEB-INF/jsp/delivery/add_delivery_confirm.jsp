<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <c:set var="delivery" value="${sessionScope.delivery}"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.head_confirm" var="head"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.subscriber" var="subscriber"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.librarian" var="librarian"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.book" var="book"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.issue_date" var="issue_date"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.return_date" var="return_date"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.place" var="place"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.submit" var="submit"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.here" var="here"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.form.home" var="home"/>
    <fmt:message bundle="${loc}" key="local.add_delivery.confirm" var="confirm"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${head}</h3>
    <br>
    <form method="post" action="${Uri.ADD_DELIVERY}">
        ${subscriber} ${delivery.subscriber.nameUser} ${delivery.subscriber.surnameUser}<br>
        ${librarian} ${delivery.librarian.nameUser} ${delivery.librarian.surnameUser}<br>
        ${book} ${delivery.book.title}<br>
        ${issue_date} <datefmt:dateFormatTag value="${LocalDate.now()}"/><br>
        <c:choose>
            <c:when test="${!delivery.dateOfIssue.equals(delivery.dateOfReturnExpected)}">
                <label>${return_date}
                    <input type="date" name="${ParameterName.EXPECTED_DATE}"
                           value="${delivery.dateOfReturnExpected}"
                           max="${delivery.dateOfReturnExpected.plusDays(delivery.subscriber.subscriberStatus.daysForReturn)}"
                           min="${LocalDate.now()}" required><br>
                </label>
            </c:when>
            <c:otherwise>
                ${return_date} <datefmt:dateFormatTag value="${LocalDate.now()}"/><br>
                <input type="hidden" name="${ParameterName.EXPECTED_DATE}"
                       value="${delivery.dateOfReturnExpected}"
                       required/>
            </c:otherwise>
        </c:choose>
        ${place} ${delivery.deliveryPlace.name().toLowerCase()}
        <br>
        <p>
            <button type="submit">${confirm}</button>
        </p>
    </form>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
