<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.deliveries.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.deliveries.add_delivery" var="add_delivery"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.name_table" var="name_table"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.idDelivery" var="idDelivery"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.id_subscriber" var="id_subscriber"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.name_subscriber" var="name_subscriber"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.id_librarian" var="id_librarian"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.name_librarian" var="name_librarian"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.idBook" var="idBook"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.book_title" var="book_title"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.issue_date" var="issue_date"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.expected_return_date" var="expected_return_date"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.actual_return_date" var="actual_return_date"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.place" var="place"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.status" var="status"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <form action="${Uri.ADD_DELIVERY_BLANK}">
        <button>${add_delivery}</button>
    </form>
    <table border="1" width='100%'>
        <caption><h3>${name_table}</h3></caption>
        <tr>
            <td>${idDelivery}</td>
            <td>${id_subscriber}</td>
            <td>${name_subscriber}</td>
            <td>${id_librarian}</td>
            <td>${name_librarian}</td>
            <td>${idBook}</td>
            <td>${book_title}</td>
            <td>${issue_date}</td>
            <td>${actual_return_date}</td>
            <td>${expected_return_date}</td>
            <td>${place}</td>
            <td>${status}</td>
        </tr>
        <c:forEach var="delivery" items="${deliveries}" end="${on_page-1}">
            <tr>
                <td>
                    <a href="${Uri.DELIVERY}?${ParameterName.ID}=${delivery.idDelivery}">${delivery.idDelivery}</a>
                </td>
                <td>
                    <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${delivery.subscriber.idUser}">${delivery.subscriber.idUser}</a>
                </td>
                <td>
                    <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${delivery.subscriber.idUser}">${delivery.subscriber.nameUser} ${delivery.subscriber.surnameUser}</a>
                </td>
                <td>
                        ${delivery.librarian.idUser}
                </td>
                <td>
                        ${delivery.librarian.nameUser} ${delivery.librarian.surnameUser}
                </td>
                <td>
                    <a href="${Uri.BOOK}?${ParameterName.ID}=${delivery.book.idBook}">${delivery.book.idBook}</a>
                </td>
                <td>
                    <a href="${Uri.BOOK}?${ParameterName.ID}=${delivery.book.idBook}">${delivery.book.title}</a>
                </td>
                <td>
                    <datefmt:dateFormatTag value="${delivery.dateOfIssue}"/>
                </td>
                <td>
                    <datefmt:dateFormatTag value="${delivery.dateOfReturnActual}"/>
                </td>
                <td>
                    <datefmt:dateFormatTag value="${delivery.dateOfReturnExpected}"/>
                </td>
                <td>
                        ${delivery.deliveryPlace}
                </td>
                <td>
                        ${delivery.deliveryStatus}
                </td>
            </tr>
        </c:forEach>
    </table>
    <span>
<c:forEach begin="1" end="${pages}" varStatus="loop">
    | <a
        href="${Uri.DELIVERIES}?${ParameterName.PAGE}=${loop.current}&${requestScope['javax.servlet.forward.query_string'].replaceFirst("page=[\\d]+&?", "")}"
        style="color: black; font-size: 1.5em;">${loop.current}</a>
</c:forEach>|
</span>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
