<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.main.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.main.table.name" var="table_name"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="part/header.jsp"/>
</header>
<main>
    <c:set var="numCols" value="3"/>
    <c:set var="numRows" value="3"/>
    <c:set var="rowCount" value="0"/>
    <table width='100%' cellspacing="10px" style="table-layout: fixed;">
        <caption><h3>${table_name}</h3></caption>
        <colgroup>
            <col width="10%">
            <col width="23%">
            <col width="10%">
            <col width="23%">
            <col width="10%">
            <col width="23%">
        </colgroup>
        <tr>
            <c:forEach var="book" items="${books}" varStatus="status" end="2">
            <c:if test="${rowCount lt numRows}">
            <td>
                <a href="${Uri.BOOK}?id=${book.idBook}">
                    <img src="${book.imageLink}" alt="${book.title}"></a>
            </td>
            <td>
                    ${book.title}<br><br>
                <c:forEach var="author" items="${book.authors}">
                    <a href="${Uri.AUTHOR}?id=${author.idAuthor}"
                       style="color:black; text-decoration:underline">${author.nameAuthor}, </a>
                </c:forEach>
                    ${book.year}
            </td>
            <c:if test="${status.count ne 0 && status.count % numCols == 0}">
            <c:set var="rowCount" value="${rowCount + 1}"/>
        </tr>
        <tr>
            </c:if>
            </c:if>
            </c:forEach>
        </tr>
    </table>
</main>
<jsp:include page="part/footer.jsp"/>
</body>
</html>
