<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.Role" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.authors.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.authors.button.submit" var="submit"/>
    <fmt:message bundle="${loc}" key="local.authors.button.add_author" var="add_author"/>
    <fmt:message bundle="${loc}" key="local.books.sort_how.ascending" var="ascending"/>
    <fmt:message bundle="${loc}" key="local.books.sort_how.descending" var="descending"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <form method="get" action="${Uri.AUTHORS}">
        <label>
            <select name="${ParameterName.SORT_HOW}">
                <option selected value="${ParameterName.ASC}">${ascending}</option>
                <option value="${ParameterName.DESC}">${descending}</option>
            </select>
        </label>
        <p><input type="submit" value="${submit}"></p>
    </form>
    <br>
    <c:if test="${sessionScope.get(ParameterName.ROLE) == Role.LIBRARIAN}">
        <form action="${Uri.ADD_AUTHOR_BLANK}">
            <button>${add_author}</button>
        </form>
    </c:if>
    <br>
    <c:forEach var="author" items="${authors}" end="${on_page-1}">
        <a href="${Uri.AUTHOR}?${ParameterName.ID}=${author.idAuthor}"
           style="color: black; font-size: 1.5em; text-decoration:underline">${author.nameAuthor}</a><br>
    </c:forEach>
    <span>
<c:forEach begin="1" end="${pages}" varStatus="loop">
    | <a
        href="${Uri.AUTHORS}?${ParameterName.PAGE}=${loop.current}&${requestScope['javax.servlet.forward.query_string'].replaceFirst("page=[\\d]+&?", "")}"
        style="color: black; font-size: 1.5em;">${loop.current}</a>
</c:forEach>|
</span>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
