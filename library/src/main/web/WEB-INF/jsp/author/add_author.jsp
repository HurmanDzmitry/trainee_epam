<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.add_author.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.add_author.message.not_add" var="not_add"/>
    <fmt:message bundle="${loc}" key="local.add_author.button.add_author" var="add_author"/>
    <fmt:message bundle="${loc}" key="local.add_author.button.submit" var="submit"/>
    <fmt:message bundle="${loc}" key="local.add_author.form.name" var="name"/>
    <fmt:message bundle="${loc}" key="local.add_author.form.message.invalid_name" var="invalid_name"/>
    <fmt:message bundle="${loc}" key="local.add_author.form.message.busy_name" var="busy_name"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${add_author}</h3>
    <br>
    <c:if test="${not empty requestScope.message}">
        ${not_add}<br>
        <c:choose>
            <c:when test="${requestScope.message eq 'busy_name'}">
                ${busy_name}
            </c:when>
            <c:otherwise>
                ${requestScope.message}
            </c:otherwise>
        </c:choose>
    </c:if>
    <form method="post" action="${Uri.ADD_AUTHOR}">
        <label>${name}
            <input type="text" name="${ParameterName.NAME}"
                   pattern="^([а-яА-Я-]){2,20}?([а-яА-Я-]{2,20})?$"
                   maxlength="41"
                   oninvalid="this.setCustomValidity('${invalid_name}')"
                   oninput="this.setCustomValidity('')" required><br>
        </label>
        <br>
        <button type="submit">${submit}</button>
    </form>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
