<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.reservations.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.name_table" var="name_table"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.idReservation" var="idReservation"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.id_subscriber" var="id_subscriber"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.name_subscriber" var="name_subscriber"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.idBook" var="idBook"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.book_title" var="book_title"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.dateOfBegin" var="dateOfBegin"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.dateOfReset" var="dateOfReset"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.status" var="status"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <table border="1" width='100%'>
        <caption><h3>${name_table}</h3></caption>
        <tr>
            <td>${idReservation}</td>
            <td>${id_subscriber}</td>
            <td>${name_subscriber}</td>
            <td>${idBook}</td>
            <td>${book_title}</td>
            <td>${dateOfBegin}</td>
            <td>${dateOfReset}</td>
            <td>${status}</td>
        </tr>
        <c:forEach var="reservation" items="${reservations}" end="${on_page-1}">
            <tr>
                <td>
                    <a href="${Uri.RESERVATION}?${ParameterName.ID}=${reservation.idReservation}">${reservation.idReservation}</a>
                </td>
                <td>
                    <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${reservation.subscriber.idUser}">${reservation.subscriber.idUser}</a>
                </td>
                <td>
                    <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${reservation.subscriber.idUser}">${reservation.subscriber.nameUser} ${reservation.subscriber.surnameUser}</a>
                </td>
                <td>
                    <a href="${Uri.BOOK}?${ParameterName.ID}=${reservation.book.idBook}">${reservation.book.idBook}</a>
                </td>
                <td>
                    <a href="${Uri.BOOK}?${ParameterName.ID}=${reservation.book.idBook}">${reservation.book.title}</a>
                </td>
                <td>
                    <datefmt:dateFormatTag value="${reservation.dateOfBegin}"/>
                </td>
                <td>
                    <datefmt:dateFormatTag value="${reservation.dateOfReset}"/>
                </td>
                <td>
                        ${reservation.reservationStatus}
                </td>
            </tr>
        </c:forEach>
    </table>
    <span>
<c:forEach begin="1" end="${pages}" varStatus="loop">
    | <a
        href="${Uri.RESERVATIONS}?${ParameterName.PAGE}=${loop.current}&${requestScope['javax.servlet.forward.query_string'].replaceFirst("page=[\\d]+&?", "")}"
        style="color: black; font-size: 1.5em;">${loop.current}</a>
</c:forEach>|
</span>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
