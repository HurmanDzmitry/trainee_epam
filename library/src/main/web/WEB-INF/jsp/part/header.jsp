<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.Role" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<fmt:setLocale value="${sessionScope.locale}"/>
<fmt:setBundle basename="local" var="loc"/>
<fmt:message bundle="${loc}" key="local.header.href.main_page" var="main_page"/>
<fmt:message bundle="${loc}" key="local.header.href.books" var="books"/>
<fmt:message bundle="${loc}" key="local.header.href.authors" var="authors"/>
<fmt:message bundle="${loc}" key="local.header.href.deliveries" var="deliveries"/>
<fmt:message bundle="${loc}" key="local.header.href.reservations" var="reservations"/>
<fmt:message bundle="${loc}" key="local.header.href.subscribers" var="subscribers"/>
<fmt:message bundle="${loc}" key="local.header.href.about_us" var="about_us"/>
<fmt:message bundle="${loc}" key="local.header.href.log_in" var="log_in"/>
<fmt:message bundle="${loc}" key="local.header.href.account" var="account"/>
<fmt:message bundle="${loc}" key="local.header.href.lang" var="lang"/>
<fmt:message bundle="${loc}" key="local.header.back_main" var="back_main_page"/>
<fmt:message bundle="${loc}" key="local.header.searching" var="searching"/>
<a href="${Uri.MAIN}" title="${back_main_page}" id="logo">${main_page}</a>
<span class="about">
    | <a href="${Uri.BOOKS}" title="${books}">${books}</a>
    | <a href="${Uri.AUTHORS}" title="${authors}">${authors}</a>
    <c:if test="${sessionScope.get(ParameterName.ROLE) == Role.LIBRARIAN}">
        | <a href="${Uri.DELIVERIES}" title="${deliveries}">${deliveries}</a>
        | <a href="${Uri.RESERVATIONS}" title="${reservations}">${reservations}</a>
        | <a href="${Uri.SUBSCRIBERS}" title="${subscribers}">${subscribers}</a>
    </c:if>
    | <a href="${Uri.ABOUT_US}" title="${about_us}">${about_us}</a>
    |
</span>
<div class="right">
    <div class="about">
        <form method="post" action="${Uri.SEARCHING}" style="display:inline">
            <input type="text" name="${ParameterName.SEARCH}" placeholder="${searching}" required/>
        </form>
        <c:choose>
            <c:when test="${sessionScope.get(ParameterName.ID)!= null}">
                <a href="${Uri.ACCOUNT}" title="${account}">${account}</a>
            </c:when>
            <c:otherwise>
                <a href="${Uri.LOG_IN_BLANK}" title="${log_in}">${log_in}</a>
            </c:otherwise>
        </c:choose>
        <label>
            <select name="lang" class="btn-link" onchange="top.location.href =
                this.options[this.selectedIndex].value;">
                <option selected value="#">${lang}</option>
                <option value="${Uri.LOCALE}?${ParameterName.LOCALE}=ru&${ParameterName.URI}=${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string']}">
                    ru
                </option>
                <option value="${Uri.LOCALE}?${ParameterName.LOCALE}=en&${ParameterName.URI}=${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string']}">
                    en
                </option>
            </select>
        </label>
    </div>
</div>
