<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.error.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.error.head" var="head"/>
    <fmt:message bundle="${loc}" key="local.error403.message.error" var="message_error"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${head}</h3>
    <br>
    ${message_error}
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
