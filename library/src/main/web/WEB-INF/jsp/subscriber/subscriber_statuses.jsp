<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.subscriber_statuses.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.subscriber_statuses.table.name" var="name"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.id" var="id"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.status" var="status"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.limitReservation" var="limitReservation"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.limitTaking" var="limitTaking"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.days_reservation" var="days_reservation"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.days_return" var="days_return"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <br>
    <table border="1" width='100%'>
        <caption><h3>${name}</h3></caption>
        <tr>
            <td>${id}</td>
            <td>${status}</td>
            <td>${limitReservation}</td>
            <td>${limitTaking}</td>
            <td>${days_reservation}</td>
            <td>${days_return}</td>
        </tr>
        <tr>
            <c:forEach var="subscriberStatus" items="${subscriber_statuses}">
            <td>
                    ${subscriberStatus.idSubscriberStatus}
            </td>
            <td>
                    ${subscriberStatus.nameStatus}
            </td>
            <td>
                    ${subscriberStatus.limitReservation}
            </td>
            <td>
                    ${subscriberStatus.limitTaking}
            </td>
            <td>
                    ${subscriberStatus.daysForReservation}
            </td>
            <td>
                    ${subscriberStatus.daysForReturn}
            </td>
        </tr>
        </c:forEach>
    </table>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
