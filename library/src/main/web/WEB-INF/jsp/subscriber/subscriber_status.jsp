<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.name" var="name"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.id" var="id"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.status" var="status"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.limitReservation" var="limitReservation"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.limitTaking" var="limitTaking"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.days_reservation" var="days_reservation"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.table.days_return" var="days_return"/>
    <fmt:message bundle="${loc}" key="local.subscriberStatus.see_all" var="see_all"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <br>
    <table border="1" width='100%'>
        <caption><h3>${name}</h3></caption>
        <tr>
            <td>${id}</td>
            <td>${status}</td>
            <td>${limitReservation}</td>
            <td>${limitTaking}</td>
            <td>${days_reservation}</td>
            <td>${days_return}</td>
        </tr>
        <tr>
            <td>
                ${subscriber_status.idSubscriberStatus}
            </td>
            <td>
                ${subscriber_status.nameStatus}
            </td>
            <td>
                ${subscriber_status.limitReservation}
            </td>
            <td>
                ${subscriber_status.limitTaking}
            </td>
            <td>
                ${subscriber_status.daysForReservation}
            </td>
            <td>
                ${subscriber_status.daysForReturn}
            </td>
        </tr>
    </table>
    <br>
    <form action="${Uri.SUBSCRIBER_STATUSES}">
        <button>${see_all}</button>
    </form>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
