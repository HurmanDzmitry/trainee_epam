<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.subscriber.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.subscriber.message.empty_delivery" var="empty_delivery"/>
    <fmt:message bundle="${loc}" key="local.subscriber.message.empty_reserve" var="empty_reserve"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.name_table1" var="name_table1"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.id" var="id"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.name_subscriber" var="name_subscriber"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.birthday" var="birthday"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.email" var="email"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.status" var="status"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.name_table2" var="name_table2"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.name_table3" var="name_table3"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.idDelivery" var="idDelivery"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.id_librarian" var="id_librarian"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.name_librarian" var="name_librarian"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.idBook" var="idBook"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.book_title" var="book_title"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.issue_date" var="issue_date"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.expected_return_date" var="expected_return_date"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.actual_return_date" var="actual_return_date"/>
    <fmt:message bundle="${loc}" key="local.deliveries.table.place" var="place"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.idReservation" var="idReservation"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.dateOfBegin" var="dateOfBegin"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.dateOfReset" var="dateOfReset"/>
    <fmt:message bundle="${loc}" key="local.reservations.table.status" var="status"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <br>
    <table border="1" width='100%'>
        <caption><h3>${name_table1}</h3></caption>
        <tr>
            <td>${id}</td>
            <td>${name_subscriber}</td>
            <td>${birthday}</td>
            <td>${email}</td>
            <td>${status}</td>
        </tr>
        <tr>
            <td>
                <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${subscriber.idUser}">${subscriber.idUser}</a>
            </td>
            <td>
                <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${subscriber.idUser}">${subscriber.nameUser} ${subscriber.surnameUser}</a>
            </td>
            <td>
                <datefmt:dateFormatTag value="${subscriber.dateOfBirth}"/>
            </td>
            <td>
                ${subscriber.email}
            </td>
            <td>
                <a href="${Uri.SUBSCRIBER_STATUS}?${ParameterName.NAME}=${subscriber.subscriberStatus.nameStatus}">${subscriber.subscriberStatus.nameStatus}</a>
            </td>
        </tr>
    </table>
    <br>
    <strong>${name_table2}:</strong>
    <br>
    <c:choose>
        <c:when test="${not empty reservations}">
            <table border="1" width='100%'>
                <tr>
                    <td>${idDelivery}</td>
                    <td>${id_librarian}</td>
                    <td>${name_librarian}</td>
                    <td>${idBook}</td>
                    <td>${book_title}</td>
                    <td>${issue_date}</td>
                    <td>${actual_return_date}</td>
                    <td>${expected_return_date}</td>
                    <td>${place}</td>
                    <td>${status}</td>
                </tr>
                <c:forEach var="delivery" items="${deliveries}" end="19">
                    <tr>
                        <td>
                            <a href="${Uri.DELIVERY}?${ParameterName.ID}=${delivery.idDelivery}">${delivery.idDelivery}</a>
                        </td>
                        <td>
                                ${delivery.librarian.idUser}
                        </td>
                        <td>
                                ${delivery.librarian.nameUser} ${delivery.librarian.surnameUser}
                        </td>
                        <td>
                            <a href="${Uri.BOOK}?${ParameterName.ID}=${delivery.book.idBook}">${delivery.book.idBook}</a>
                        </td>
                        <td>
                            <a href="${Uri.BOOK}?${ParameterName.ID}=${delivery.book.idBook}">${delivery.book.title}</a>
                        </td>
                        <td>
                            <datefmt:dateFormatTag value="${delivery.dateOfIssue}"/>
                        </td>
                        <td>
                            <datefmt:dateFormatTag value="${delivery.dateOfReturnActual}"/>
                        </td>
                        <td>
                            <datefmt:dateFormatTag value="${delivery.dateOfReturnExpected}"/>
                        </td>
                        <td>
                                ${delivery.deliveryPlace}
                        </td>
                        <td>
                                ${delivery.deliveryStatus}
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:when>
        <c:otherwise>
            ${empty_delivery}
            <br>
        </c:otherwise>
    </c:choose>
    <br>
    <strong>${name_table3}:</strong>
    <br>
    <c:choose>
        <c:when test="${not empty reservations}">
            <table border="1" width='100%'>
                <tr>
                    <td>${idReservation}</td>
                    <td>${idBook}</td>
                    <td>${book_title}</td>
                    <td>${dateOfBegin}</td>
                    <td>${dateOfReset}</td>
                    <td>${status}</td>
                </tr>
                <c:forEach var="reservation" items="${reservations}" end="9">
                    <tr>
                        <td>
                            <a href="${Uri.RESERVATION}?${ParameterName.ID}=${reservation.idReservation}">${reservation.idReservation}</a>
                        </td>
                        <td>
                            <a href="${Uri.BOOK}?${ParameterName.ID}=${reservation.book.idBook}">${reservation.book.idBook}</a>
                        </td>
                        <td>
                            <a href="${Uri.BOOK}?${ParameterName.ID}=${reservation.book.idBook}">${reservation.book.title}</a>
                        </td>
                        <td>
                            <datefmt:dateFormatTag value="${reservation.dateOfBegin}"/>
                        </td>
                        <td>
                            <datefmt:dateFormatTag value="${reservation.dateOfReset}"/>
                        </td>
                        <td>
                                ${reservation.reservationStatus}
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:when>
        <c:otherwise>
            ${empty_reserve}
            <br>
        </c:otherwise>
    </c:choose>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
