<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.subscribers.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.subscribers.table.name_table" var="name_table"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.id" var="id"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.name_subscriber" var="name_subscriber"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.birthday" var="birthday"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.email" var="email"/>
    <fmt:message bundle="${loc}" key="local.subscriber.table.status" var="status"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <table border="1" width='100%'>
        <caption><h3>${name_table}</h3></caption>
        <tr>
            <td>${id}</td>
            <td>${name_subscriber}</td>
            <td>${birthday}</td>
            <td>${email}</td>
            <td>${status}</td>
        </tr>
        <c:forEach var="subscriber" items="${subscribers}" end="${on_page-1}">
            <tr>
                <td>
                    <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${subscriber.idUser}">${subscriber.idUser}</a>
                </td>
                <td>
                    <a href="${Uri.SUBSCRIBER}?${ParameterName.ID}=${subscriber.idUser}">${subscriber.surnameUser} ${subscriber.nameUser}</a>
                </td>
                <td>
                    <datefmt:dateFormatTag value="${subscriber.dateOfBirth}"/>
                </td>
                <td>
                        ${subscriber.email}
                </td>
                <td>
                    <a href="${Uri.SUBSCRIBER_STATUS}?${ParameterName.NAME}=${subscriber.subscriberStatus.nameStatus}">${subscriber.subscriberStatus.nameStatus}</a>
                </td>
            </tr>
        </c:forEach>
    </table>
    <span>
<c:forEach begin="1" end="${pages}" varStatus="loop">
    | <a
        href="${Uri.SUBSCRIBERS}?${ParameterName.PAGE}=${loop.current}&${requestScope['javax.servlet.forward.query_string'].replaceFirst("page=[\\d]+&?", "")}"
        style="color: black; font-size: 1.5em;">${loop.current}</a>
</c:forEach>|
</span>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
