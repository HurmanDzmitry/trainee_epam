<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.log_in.button.submit" var="apply"/>
    <fmt:message bundle="${loc}" key="local.log_in.field.login" var="login"/>
    <fmt:message bundle="${loc}" key="local.log_in.field.password" var="password"/>
    <fmt:message bundle="${loc}" key="local.log_in.message.wrong_log_in" var="wrong_log_in"/>
    <fmt:message bundle="${loc}" key="local.log_in.message.yet_log_in" var="yet_log_in"/>
    <fmt:message bundle="${loc}" key="local.log_in.text.head" var="head"/>
    <fmt:message bundle="${loc}" key="local.log_in.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.log_in.button.sign_up" var="sign_up"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.see_pass" var="see_pass"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${head}</h3>
    <br>
    <c:choose>
        <c:when test="${sessionScope.id == null}">
            <c:if test="${requestScope.message==true}">
                ${wrong_log_in}<br>
            </c:if>
            <form method="post" action="${Uri.LOG_IN}">
                <label>${login}
                    <input type="text" name="${ParameterName.LOGIN}" required><br>
                </label>
                <label>${password}
                    <input type="password" name="${ParameterName.PASSWORD}" onmouseover="this.type='text'"
                           onmouseout="this.type='password'" placeholder="${see_pass}" required><br>
                </label>
                <button type="submit">${apply}</button>
            </form>
            <br>
            <form action="${Uri.SIGN_UP_BLANK}">
                <button>${sign_up}</button>
            </form>
        </c:when>
        <c:otherwise>
            ${yet_log_in}<br>
        </c:otherwise>
    </c:choose>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
