<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.sign_up.button.submit" var="apply"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.name" var="name"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.surname" var="surname"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.birthday" var="birthday"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.login" var="login"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.password" var="password"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.password_replay" var="password_replay"/>
    <fmt:message bundle="${loc}" key="local.sign_up.field.email" var="email"/>
    <fmt:message bundle="${loc}" key="local.sign_up.message.wrong_sign_up" var="wrong_sign_up"/>
    <fmt:message bundle="${loc}" key="local.sign_up.message.yet_log_in" var="yet_log_in"/>
    <fmt:message bundle="${loc}" key="local.sign_up.text.head" var="head"/>
    <fmt:message bundle="${loc}" key="local.sign_up.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.invalid_name" var="invalid_name"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.invalid_surname" var="invalid_surname"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.invalid_birthday" var="invalid_birthday"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.invalid_login" var="invalid_login"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.invalid_password" var="invalid_password"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.invalid_email" var="invalid_email"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.see_pass" var="see_pass"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.busy_login" var="busy_login"/>
    <fmt:message bundle="${loc}" key="local.sign_up.form.message.busy_email" var="busy_email"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <h3>${head}</h3>
    <br>
    <c:choose>
        <c:when test="${sessionScope.id == null}">
            <c:if test="${not empty requestScope.message}">
                ${wrong_sign_up}<br>
                <c:choose>
                    <c:when test="${requestScope.message eq 'busy_login'}">
                        ${busy_login}
                    </c:when>
                    <c:when test="${requestScope.message eq 'busy_email'}">
                        ${busy_email}
                    </c:when>
                    <c:otherwise>
                        ${requestScope.message}
                    </c:otherwise>
                </c:choose>
            </c:if>
            <form method="post" action="${Uri.SIGN_UP}">
                <label>${name}
                    <input type="text" name="${ParameterName.NAME}" pattern="^[A-ZА-Я]([a-zA-Z-]|[а-яА-Я-]){2,19}$"
                           maxlength="20"
                           oninvalid="this.setCustomValidity('${invalid_name}')"
                           oninput="this.setCustomValidity('')" required><br>
                </label>
                <label>${surname}
                    <input type="text" name="${ParameterName.SURNAME}" pattern="^[A-ZА-Я]([a-zA-Z-]|[а-яА-Я-]){2,19}$"
                           maxlength="20"
                           oninvalid="this.setCustomValidity('${invalid_surname}')"
                           oninput="this.setCustomValidity('')" required><br>
                </label>
                <label>${birthday}
                    <input type="date" name="${ParameterName.BIRTHDAY}"
                           max="${LocalDate.now().minusYears(5)}" min="${LocalDate.now().minusYears(100)}"
                           oninvalid="this.setCustomValidity('${invalid_birthday}')"
                           oninput="this.setCustomValidity('')" required><br>
                </label>
                <label>${login}
                    <input type="text" name="${ParameterName.LOGIN}" pattern="^[A-Za-zа-яА-Я][A-Za-zа-яА-Я\d_]{5,19}$"
                           maxlength="20"
                           oninvalid="this.setCustomValidity('${invalid_login}')"
                           oninput="this.setCustomValidity('')" required><br>
                </label>
                <label>${password}
                    <input type="password" name="${ParameterName.PASSWORD}" onmouseover="this.type='text'"
                           onmouseout="this.type='password'" placeholder="${see_pass}"
                           pattern="^(?=.*[\d])(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=\S+$).{8,20}$"
                           maxlength="20"
                           oninvalid="this.setCustomValidity('${invalid_password}')"
                           oninput="this.setCustomValidity('')" required><br>
                </label>
                <label>${email}
                    <input type="email" name="${ParameterName.EMAIL}"
                           pattern="^[A-Za-z\d._%+-]+@[A-Za-z\d.-]+\.[A-Za-z]{2,6}$"
                           oninvalid="this.setCustomValidity('${invalid_email}')"
                           oninput="this.setCustomValidity('')" required><br>
                </label>
                <br>
                <p>
                    <button type="submit">${apply}</button>
                </p>
            </form>
        </c:when>
        <c:otherwise>
            ${yet_log_in}<br>
        </c:otherwise>
    </c:choose>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>


