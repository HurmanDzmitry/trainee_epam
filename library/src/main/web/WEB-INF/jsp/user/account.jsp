<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.epam.javatr.controller.command.util.ParameterName" %>
<%@ page import="by.epam.javatr.controller.command.util.Uri" %>
<%@ page import="by.epam.javatr.entity.enumeration.DeliveryStatus" %>
<%@ page import="by.epam.javatr.entity.enumeration.Role" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="datefmt" uri="/WEB-INF/tld/dateFormatter.tld" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <c:set var="user" value="${user}"/>
    <fmt:setLocale value="${sessionScope.locale}"/>
    <fmt:setBundle basename="local" var="loc"/>
    <fmt:message bundle="${loc}" key="local.account.button.log_out" var="log_out"/>
    <fmt:message bundle="${loc}" key="local.account.button.delivery_history" var="delivery_history"/>
    <fmt:message bundle="${loc}" key="local.account.button.reservation_history" var="reservation_history"/>
    <fmt:message bundle="${loc}" key="local.account.field.name" var="name"/>
    <fmt:message bundle="${loc}" key="local.account.field.surname" var="suranme"/>
    <fmt:message bundle="${loc}" key="local.account.field.login" var="login"/>
    <fmt:message bundle="${loc}" key="local.account.field.birthday" var="birthday"/>
    <fmt:message bundle="${loc}" key="local.account.message.not_log_in" var="not_log_in"/>
    <fmt:message bundle="${loc}" key="local.account.text.head" var="head"/>
    <fmt:message bundle="${loc}" key="local.account.title" var="title"/>
    <fmt:message bundle="${loc}" key="local.account.message.new_reservation" var="new_reservation"/>
    <fmt:message bundle="${loc}" key="local.account.message.new_reservation_part_1" var="new_reservation_part_1"/>
    <fmt:message bundle="${loc}" key="local.account.message.new_reservation_part_2" var="new_reservation_part_2"/>
    <fmt:message bundle="${loc}" key="local.account.message.reserve_fail" var="reserve_fail"/>
    <fmt:message bundle="${loc}" key="local.account.message.blocked" var="blocked"/>
    <fmt:message bundle="${loc}" key="local.account.message.wrong_quantity" var="wrong_quantity"/>
    <fmt:message bundle="${loc}" key="local.account.message.not_issued" var="not_issued"/>
    <fmt:message bundle="${loc}" key="local.account.message.limit" var="limit"/>
    <fmt:message bundle="${loc}" key="local.account.message.empty_reserve" var="empty_reserve"/>
    <fmt:message bundle="${loc}" key="local.account.table.table_name_reserve" var="table_name_reserve"/>
    <fmt:message bundle="${loc}" key="local.account.table.finish_reserve" var="finish_reserve"/>
    <fmt:message bundle="${loc}" key="local.account.message.empty_delivery" var="empty_delivery"/>
    <fmt:message bundle="${loc}" key="local.account.message.expired" var="expired"/>
    <fmt:message bundle="${loc}" key="local.account.table.table_name_delivery" var="table_name_delivery"/>
    <fmt:message bundle="${loc}" key="local.account.table.finish_delivery" var="finish_delivery"/>
    <fmt:message bundle="${loc}" key="local.account.table.start_delivery" var="start_delivery"/>
    <title>${title}</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<main>
    <c:choose>
        <c:when test="${not empty requestScope.user}">
            <c:if test="${not empty requestScope.reservation}">
                ${new_reservation}
                ${new_reservation_part_1} ${requestScope.reservation.book.title}.
                ${new_reservation_part_2}   <datefmt:dateFormatTag value="${requestScope.reservation.dateOfReset}"/>.
            </c:if>
            <c:if test="${not empty requestScope.message}">
                ${reserve_fail}
                <c:choose>
                    <c:when test="${requestScope.message eq 'blocked'}">
                        ${blocked}
                    </c:when>
                    <c:when test="${requestScope.message eq 'limit'}">
                        ${limit}
                    </c:when>
                </c:choose>
                <br>
                <br>
            </c:if>
            <h3>${head}</h3>
            <br>
            ${name} ${user.nameUser} <br>
            ${suranme} ${user.surnameUser} <br>
            ${login} ${user.login} <br>
            ${birthday} <datefmt:dateFormatTag value="${user.dateOfBirth}"/><br>
            <br>
            <form action="${Uri.LOG_OUT}" method="post">
                <input type="submit" value="${log_out}"/>
            </form>
            <br>
            <c:if test="${sessionScope.get(ParameterName.ROLE) == Role.SUBSCRIBER}">
                <strong>${table_name_delivery}</strong>
                <br>
                <c:choose>
                    <c:when test="${not empty deliveries}">
                        <c:set var="numCols" value="5"/>
                        <c:set var="numRows" value="2"/>
                        <c:set var="rowCount" value="0"/>
                        <table width='100%' cellspacing="10px" style="table-layout: fixed;">
                            <colgroup>
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                            </colgroup>
                            <tr>
                                <c:forEach var="delivery" items="${deliveries}" varStatus="status" end="9">
                                <c:if test="${rowCount lt numRows}">
                                <td>
                                    <a href="${Uri.BOOK}?id=${delivery.book.idBook}">
                                        <img src="${delivery.book.imageLink}" alt="${delivery.book.title}"></a>
                                </td>
                                <td>
                                        ${delivery.book.title}<br>
                                    <c:forEach var="author" items="${delivery.book.authors}">
                                        <a href="${Uri.AUTHOR}?id=${author.idAuthor}"
                                           style="color:black; text-decoration:underline">${author.nameAuthor}, </a>
                                    </c:forEach>
                                        ${delivery.book.year}<br><br>
                                        ${start_delivery} <datefmt:dateFormatTag
                                        value="${delivery.dateOfIssue}"/>
                                    <br>
                                        ${finish_delivery} <datefmt:dateFormatTag
                                        value="${delivery.dateOfReturnExpected}"/>
                                    <br>
                                    <c:if test="${delivery.deliveryStatus == DeliveryStatus.EXPIRED}">
                                        <strong style="color: red;">${expired}</strong>
                                    </c:if>
                                    <br>
                                    </c:if>
                                </td>
                                <c:if test="${status.count ne 0 && status.count % numCols == 0}">
                                <c:set var="rowCount" value="${rowCount + 1}"/>
                            </tr>
                            <tr>
                                </c:if>
                                </c:forEach>
                            </tr>
                        </table>
                        <br>
                    </c:when>
                    <c:otherwise>
                        ${empty_delivery}
                        <br>
                    </c:otherwise>
                </c:choose>
                <br>
                <form action="${Uri.DELIVERY_HISTORY}">
                    <button>${delivery_history}</button>
                </form>
                <br>
                <strong>${table_name_reserve}</strong>
                <br>
                <c:choose>
                    <c:when test="${not empty reservations}">
                        <c:set var="numCols" value="5"/>
                        <c:set var="numRows" value="1"/>
                        <c:set var="rowCount" value="0"/>
                        <table width='100%' cellspacing="10px" style="table-layout: fixed;">
                            <colgroup>
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                                <col width="7%">
                                <col width="13%">
                            </colgroup>
                            <tr>
                                <c:forEach var="reservation" items="${reservations}" varStatus="status" end="5">
                                <c:if test="${rowCount lt numRows}">
                                <td>
                                    <a href="${Uri.BOOK}?id=${reservation.book.idBook}">
                                        <img src="${reservation.book.imageLink}" alt="${reservation.book.title}"></a>
                                </td>
                                <td>
                                        ${reservation.book.title}<br>
                                    <c:forEach var="author" items="${reservation.book.authors}">
                                        <a href="${Uri.AUTHOR}?id=${author.idAuthor}"
                                           style="color:black; text-decoration:underline">${author.nameAuthor}, </a>
                                    </c:forEach>
                                        ${reservation.book.year}<br><br>
                                        ${finish_reserve} <datefmt:dateFormatTag
                                        value="${reservation.dateOfReset}"/>
                                    <br>
                                    </c:if>
                                </td>
                                <c:if test="${status.count ne 0 && status.count % numCols == 0}">
                                <c:set var="rowCount" value="${rowCount + 1}"/>
                            </tr>
                            <tr>
                                </c:if>
                                </c:forEach>
                            </tr>
                        </table>
                    </c:when>
                    <c:otherwise>
                        ${empty_reserve}
                        <br>
                    </c:otherwise>
                </c:choose>
                <br>
                <form action="${Uri.RESERVATION_HISTORY}">
                    <button>${reservation_history}</button>
                </form>
                <br>
            </c:if>
        </c:when>
        <c:otherwise>
            ${not_log_in}<br>
        </c:otherwise>
    </c:choose>
</main>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
