ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map("map", {
        center: [53.931069, 27.645982],
        zoom: 14
    });
    myMap.geoObjects
        .add(new ymaps.Placemark(myMap.getCenter(), {
            balloonContent: '<strong>library</strong>'
        }, {
            preset: 'islands#dotIcon',
            iconColor: '#735184'
        }));
}