var countOfFields = 1;
var curFieldNameId = 1;
var maxFieldLimit = 10;

function deleteField(a) {
    if (countOfFields > 1) {
        var contDiv = a.parentNode;
        contDiv.parentNode.removeChild(contDiv);
        countOfFields--;
    }
    return false;
}

function addField() {
    if (countOfFields >= maxFieldLimit) {
        return false;
    }
    countOfFields++;
    curFieldNameId++;
    var div = document.createElement("div");
    div.innerHTML = "<nobr><input list=\"authors\" name=\"id" + curFieldNameId + "\" type=\"text\" required/> <datalist id=\"authors\"> </datalist> <a style=\"color:red;\" onclick=\"return deleteField(this)\" href=\"#\">[-]</a> <a style=\"color:green;\" onclick=\"return addField()\" href=\"#\">[+]</a></nobr>";
    document.getElementById("parentId").appendChild(div);
    return false;
}

function setQuantityAuthors() {
    return document.getElementById('js_var').setAttribute('value', countOfFields);
}