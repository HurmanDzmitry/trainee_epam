package by.epam.javatr.controller.command.impl.user;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogOutCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().setAttribute(ParameterName.ID, null);
        req.getSession().setAttribute(ParameterName.ROLE, null);
        return new Router(Uri.MAIN, Transition.REDIRECT);
    }
}
