package by.epam.javatr.controller.command.impl.reservation;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ReservationCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        BookReservation reservation = (BookReservation) req.getSession().getAttribute(ParameterName.RESERVATION);
        if (reservation != null) {
            req.getSession().setAttribute(ParameterName.RESERVATION, null);
            req.setAttribute(ParameterName.RESERVATION, reservation);
            return new Router(JspPath.RESERVATION, Transition.FORWARD);
        }
        try {
            int id = Integer.parseInt(req.getParameter(ParameterName.ID));
            reservation = ServiceFactory.getInstance().getReservationService().findById(id);
            req.setAttribute(ParameterName.RESERVATION, reservation);
        } catch (NumberFormatException | NullPointerException e) {
            req.setAttribute(ParameterName.RESERVATION, null);
        }
        return new Router(JspPath.RESERVATION, Transition.FORWARD);
    }
}
