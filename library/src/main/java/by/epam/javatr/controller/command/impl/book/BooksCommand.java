package by.epam.javatr.controller.command.impl.book;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Book;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.util.filter.factory.FilteringFactory;
import by.epam.javatr.service.util.pagination.Pagination;
import by.epam.javatr.service.util.sorting.factory.SortingFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class BooksCommand implements Command {

    private static final int BOOKS_ON_PAGE = 6;

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String numberPage = req.getParameter(ParameterName.PAGE);
        String byWhatSort = req.getParameter(ParameterName.SORT_BY);
        String howSort = req.getParameter(ParameterName.SORT_HOW);
        String status = req.getParameter(ParameterName.STATUS);

        List<Book> books = FilteringFactory.getInstance().getBookFiltering().filter(status);
        int pages = (int) Math.ceil(books.size() / (double) BOOKS_ON_PAGE);
        books = SortingFactory.getInstance().getBookSorting().sort(books, byWhatSort, howSort);
        books = Pagination.getInstance().paginate(books, numberPage, BOOKS_ON_PAGE);

        req.setAttribute(ParameterName.PAGES, pages);
        req.setAttribute(ParameterName.ON_PAGE, BOOKS_ON_PAGE);
        req.setAttribute(ParameterName.BOOKS, books);
        return new Router(JspPath.BOOKS, Transition.FORWARD);
    }
}