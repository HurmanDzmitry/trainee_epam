package by.epam.javatr.controller.command.impl.reservation;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.entity.enumeration.ReservationStatus;
import by.epam.javatr.service.entity_service.ReservationService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CloseReservationCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int id = Integer.parseInt(req.getParameter(ParameterName.ID));
        ReservationService reservationService = ServiceFactory.getInstance().getReservationService();
        BookReservation reservation = reservationService.findById(id);
        LocalDate now = LocalDate.now();
        if (!reservation.getDateOfReset().isBefore(now)) {
            reservation.setReservationStatus(ReservationStatus.PERFORMED);
        } else {
            reservation.setReservationStatus(ReservationStatus.NOT_DONE);
        }
        InvalidKey invalidKey = ServiceFactory.getInstance().getReservationService().update(reservation);
        if (invalidKey != null) {
            throw new ServiceException("Invalid reservation.");
        }
        req.setAttribute(ParameterName.RESERVATION, reservation);

        Book book = reservation.getBook();
        book.setQuantity(book.getQuantity() + 1);
        ServiceFactory.getInstance().getBookService().update(book);

        if (req.getParameter(ParameterName.DELIVERY) != null) {
            req.getSession().setAttribute(ParameterName.SUBSCRIBER, reservation.getSubscriber());
            req.getSession().setAttribute(ParameterName.BOOK, reservation.getBook());
            return new Router(Uri.ADD_DELIVERY_BLANK, Transition.REDIRECT);
        }
        return new Router(JspPath.RESERVATION, Transition.FORWARD);
    }
}
