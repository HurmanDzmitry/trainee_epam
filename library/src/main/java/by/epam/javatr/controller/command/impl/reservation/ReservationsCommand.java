package by.epam.javatr.controller.command.impl.reservation;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.pagination.Pagination;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class ReservationsCommand implements Command {

    private static final int RESERVATIONS_ON_PAGE = 20;

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String numberPage = req.getParameter(ParameterName.PAGE);

        List<BookReservation> reservations = ServiceFactory.getInstance().getReservationService().findAll();
        int pages = (int) Math.ceil(reservations.size() / (double) RESERVATIONS_ON_PAGE);
        reservations = Pagination.getInstance().paginate(reservations, numberPage, RESERVATIONS_ON_PAGE);

        req.setAttribute(ParameterName.PAGES, pages);
        req.setAttribute(ParameterName.ON_PAGE, RESERVATIONS_ON_PAGE);
        req.setAttribute(ParameterName.RESERVATIONS, reservations);
        return new Router(JspPath.RESERVATIONS, Transition.FORWARD);
    }
}
