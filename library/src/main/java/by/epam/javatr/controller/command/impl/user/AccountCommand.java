package by.epam.javatr.controller.command.impl.user;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.entity.User;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AccountCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int id = Integer.parseInt(req.getSession().getAttribute(ParameterName.ID).toString());
        User user = ServiceFactory.getInstance().getUserService().findById(id);
        req.setAttribute(ParameterName.USER, user);

        List<BookReservation> bookReservations =
                ServiceFactory.getInstance().getReservationService().findAllActiveForSubscriber(id);
        req.setAttribute(ParameterName.RESERVATIONS, bookReservations);

        List<BookDelivery> bookDeliveries =
                ServiceFactory.getInstance().getDeliveryService().getAllActiveForSubscriber(id);
        req.setAttribute(ParameterName.DELIVERIES, bookDeliveries);

        BookReservation reservation = (BookReservation) req.getSession().getAttribute(ParameterName.RESERVATION);
        if (reservation != null) {
            req.getSession().removeAttribute(ParameterName.RESERVATION);
            req.setAttribute(ParameterName.RESERVATION, reservation);
        }

        String message = (String) req.getSession().getAttribute(ParameterName.MESSAGE);
        if (message != null) {
            req.getSession().removeAttribute(ParameterName.MESSAGE);
            req.setAttribute(ParameterName.MESSAGE, message);
        }

        return new Router(JspPath.ACCOUNT, Transition.FORWARD);
    }
}
