package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DeliveryCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        BookDelivery delivery = (BookDelivery) req.getSession().getAttribute(ParameterName.DELIVERY);
        if (delivery != null) {
            req.getSession().setAttribute(ParameterName.DELIVERY, null);
            req.setAttribute(ParameterName.DELIVERY, delivery);
            return new Router(JspPath.DELIVERY, Transition.FORWARD);
        }
        try {
            int id = Integer.parseInt(req.getParameter(ParameterName.ID));
            delivery = ServiceFactory.getInstance().getDeliveryService().findById(id);
            req.setAttribute(ParameterName.DELIVERY, delivery);
        } catch (NumberFormatException | NullPointerException e) {
            req.setAttribute(ParameterName.DELIVERY, null);
        }
        return new Router(JspPath.DELIVERY, Transition.FORWARD);
    }
}
