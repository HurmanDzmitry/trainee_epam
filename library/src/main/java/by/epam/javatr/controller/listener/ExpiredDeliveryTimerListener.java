package by.epam.javatr.controller.listener;

import by.epam.javatr.controller.background.ExpiredDelivery;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@WebListener
public class ExpiredDeliveryTimerListener implements ServletContextListener {

    private ScheduledExecutorService scheduler;

    @Override
    public void contextInitialized(ServletContextEvent event) {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime next = now.withHour(3).withMinute(0).withSecond(0);
        if (now.compareTo(next) > 0) {
            next = next.plusDays(1);
        }
        Duration duration = Duration.between(now, next);
        long delay = duration.getSeconds();
        scheduler = Executors.newSingleThreadScheduledExecutor();
        scheduler.scheduleAtFixedRate(new ExpiredDelivery(), delay,
                TimeUnit.DAYS.toSeconds(1), TimeUnit.SECONDS);
    }

    @Override
    public void contextDestroyed(ServletContextEvent event) {
        scheduler.shutdownNow();
    }
}
