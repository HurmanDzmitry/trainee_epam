package by.epam.javatr.controller.command;

import by.epam.javatr.service.exception.ServiceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface Command {

    Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException;
}
