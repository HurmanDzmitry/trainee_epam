package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.pagination.Pagination;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class DeliveriesCommand implements Command {

    private static final int DELIVERIES_ON_PAGE = 20;

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String numberPage = req.getParameter(ParameterName.PAGE);

        List<BookDelivery> deliveries = ServiceFactory.getInstance().getDeliveryService().findAll();
        int pages = (int) Math.ceil(deliveries.size() / (double) DELIVERIES_ON_PAGE);
        deliveries = Pagination.getInstance().paginate(deliveries, numberPage, DELIVERIES_ON_PAGE);

        req.setAttribute(ParameterName.PAGES, pages);
        req.setAttribute(ParameterName.ON_PAGE, DELIVERIES_ON_PAGE);
        req.setAttribute(ParameterName.DELIVERIES, deliveries);
        return new Router(JspPath.DELIVERIES, Transition.FORWARD);
    }
}
