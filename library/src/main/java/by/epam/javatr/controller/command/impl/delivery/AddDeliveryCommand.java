package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class AddDeliveryCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        BookDelivery delivery = (BookDelivery) req.getSession().getAttribute(ParameterName.DELIVERY);
        LocalDate dateOfReturnExpected = LocalDate.parse(req.getParameter(ParameterName.EXPECTED_DATE));
        delivery.setDateOfReturnExpected(dateOfReturnExpected);
        InvalidKey invalidKey = ServiceFactory.getInstance().getDeliveryService().save(delivery);
        if (invalidKey != null) {
            throw new ServiceException("Invalid delivery.");
        }
        delivery.getBook().setQuantity(delivery.getBook().getQuantity() - 1);
        ServiceFactory.getInstance().getBookService().update(delivery.getBook());
        return new Router(Uri.DELIVERY, Transition.REDIRECT);
    }
}
