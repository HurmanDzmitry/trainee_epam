package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.Librarian;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.entity.enumeration.BookStatus;
import by.epam.javatr.entity.enumeration.DeliveryPlace;
import by.epam.javatr.entity.enumeration.DeliveryStatus;
import by.epam.javatr.entity.enumeration.NameSubscriberStatus;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class AddDeliveryConfirmBlankCommand implements Command {

    private static final int MIN_QUANTITY_BOOKS_IN_LIBRARY = 1;
    private static final String EXCEEDING_LIMIT = "limit";

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int idSubscriber = Integer.parseInt(req.getParameter(ParameterName.ID_SUBSCRIBER));
        Subscriber subscriber = ServiceFactory.getInstance().getSubscriberService().findById(idSubscriber);
        if (subscriber.getSubscriberStatus().getNameStatus()
                .equals(NameSubscriberStatus.BLOCKED.name().toLowerCase())) {
            req.setAttribute(ParameterName.MESSAGE, NameSubscriberStatus.BLOCKED.name().toLowerCase());
            return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
        }
        int quantityBooksIssuedForSubscriber =
                ServiceFactory.getInstance().getDeliveryService().getQuantityBooksIssuedForSubscriber(idSubscriber);
        if (quantityBooksIssuedForSubscriber >= subscriber.getSubscriberStatus().getLimitTaking()) {
            req.setAttribute(ParameterName.MESSAGE, EXCEEDING_LIMIT);
            return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
        }
        int idLibrarian = Integer.parseInt(req.getSession().getAttribute(ParameterName.ID).toString());
        Librarian librarian = ServiceFactory.getInstance().getLibrarianService().findById(idLibrarian);
        int idBook = Integer.parseInt(req.getParameter(ParameterName.ID_BOOK));
        Book book = ServiceFactory.getInstance().getBookService().findById(idBook);
        if (book.getBookStatus() == BookStatus.NOT_ISSUED) {
            req.setAttribute(ParameterName.MESSAGE, BookStatus.NOT_ISSUED.name().toLowerCase());
            return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
        }
        LocalDate dateOfIssue = LocalDate.now();
        LocalDate dateOfReturnExpected;
        LocalDate dateOfReturnActual = null;
        DeliveryStatus status = DeliveryStatus.IN_ACTION;
        DeliveryPlace place = DeliveryPlace.valueOf(req.getParameter(ParameterName.PLACE).toUpperCase());
        if (place == DeliveryPlace.HERE) {
            if (book.getQuantity() < MIN_QUANTITY_BOOKS_IN_LIBRARY) {
                req.setAttribute(ParameterName.MESSAGE, InvalidKey.WRONG_QUANTITY.name().toLowerCase());
                req.setAttribute(ParameterName.QUANTITY, book.getQuantity());
                return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
            }
            dateOfReturnExpected = dateOfIssue;
        } else {
            if (book.getBookStatus() == BookStatus.ONLY_HERE) {
                req.setAttribute(ParameterName.MESSAGE, BookStatus.ONLY_HERE.name().toLowerCase());
                return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
            }
            if (book.getQuantity() < MIN_QUANTITY_BOOKS_IN_LIBRARY + 1) {
                req.setAttribute(ParameterName.MESSAGE, InvalidKey.WRONG_QUANTITY.name().toLowerCase());
                req.setAttribute(ParameterName.QUANTITY, book.getQuantity());
                return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
            }
            dateOfReturnExpected = dateOfIssue.plusDays(subscriber.getSubscriberStatus().getDaysForReturn());
        }
        BookDelivery delivery = new BookDelivery(subscriber, librarian, book, dateOfIssue,
                dateOfReturnActual, dateOfReturnExpected, status, place);
        req.getSession().setAttribute(ParameterName.DELIVERY, delivery);
        return new Router(JspPath.ADD_DELIVERY_CONFIRM, Transition.FORWARD);
    }
}
