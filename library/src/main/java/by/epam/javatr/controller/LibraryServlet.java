package by.epam.javatr.controller;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.CommandProvider;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@MultipartConfig(fileSizeThreshold = 1024 * 1024,
        maxFileSize = 1024 * 1024 * 5,
        maxRequestSize = 1024 * 1024 * 5 * 5)
@WebServlet(name = "LibraryServlet",
        urlPatterns = {"/main", "/error", "/locale", "/search", "/about_us",
                "/sign_up", "/sign_up_blank", "/log_in", "/log_in_blank", "/log_out", "/account",
                "/book", "/books", "/add_book_blank", "/add_book",
                "/subscriber", "/subscribers", "/subscriber_status", "/subscriber_statuses",
                "/author", "/authors", "/add_author_blank", "/add_author",
                "/delivery", "/deliveries", "/add_delivery_blank", "/add_delivery_confirm_blank",
                "/add_delivery", "/closing_delivery", "/delivery_history",
                "/reservation", "/reservations", "/add_reservation", "/close_reservation", "/reservation_history"})
public class LibraryServlet extends HttpServlet {

    public static final long serialVersionUID = 8053154988541237104L;

    private static final Logger logger = LogManager.getLogger(LibraryServlet.class);

    private final Map<String, Command> commands = CommandProvider.getInstance().getCommands();

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        Command command = commands.get(req.getRequestURI());
        if (command == null) {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
        } else {
            try {
                Router router = command.execute(req, resp);
                if (router == null) {
                    logger.info("Empty router for " + req.getRequestURI());
                } else if (router.getTransition() == Transition.FORWARD) {
                    req.getRequestDispatcher(router.getAddress()).forward(req, resp);
                } else {
                    resp.sendRedirect(router.getAddress());
                }
            } catch (Exception e) {
                logger.error("Servlet command doesn't execute.", e);
                resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        }
    }
}
