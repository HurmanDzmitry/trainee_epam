package by.epam.javatr.controller.command.impl.book;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Author;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddBookBlankCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        List<Author> authors = ServiceFactory.getInstance().getAuthorService().findAll();
        req.setAttribute(ParameterName.AUTHORS, authors);
        return new Router(JspPath.ADD_BOOK, Transition.FORWARD);
    }
}
