package by.epam.javatr.controller.command.impl.author;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Author;
import by.epam.javatr.entity.Book;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AuthorCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        Author author = (Author) req.getSession().getAttribute(ParameterName.AUTHOR);
        if (author != null) {
            req.getSession().setAttribute(ParameterName.AUTHOR, null);
            List<Book> books = ServiceFactory.getInstance().getBookService().findByAuthorId(author.getIdAuthor());
            req.setAttribute(ParameterName.AUTHOR, author);
            req.setAttribute(ParameterName.BOOKS, books);
            return new Router(JspPath.AUTHOR, Transition.FORWARD);
        }
        try {
            int id = Integer.parseInt(req.getParameter(ParameterName.ID));
            author = ServiceFactory.getInstance().getAuthorService().findById(id);
            List<Book> books = ServiceFactory.getInstance().getBookService().findByAuthorId(id);
            req.setAttribute(ParameterName.AUTHOR, author);
            req.setAttribute(ParameterName.BOOKS, books);
        } catch (NumberFormatException | NullPointerException e) {
            req.setAttribute(ParameterName.AUTHOR, null);
        }
        return new Router(JspPath.AUTHOR, Transition.FORWARD);
    }
}
