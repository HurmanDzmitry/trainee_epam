package by.epam.javatr.controller.command;

import by.epam.javatr.controller.command.impl.AboutUsCommand;
import by.epam.javatr.controller.command.impl.LocaleCommand;
import by.epam.javatr.controller.command.impl.MainPageCommand;
import by.epam.javatr.controller.command.impl.SearchingCommand;
import by.epam.javatr.controller.command.impl.author.AddAuthorBlankCommand;
import by.epam.javatr.controller.command.impl.author.AddAuthorCommand;
import by.epam.javatr.controller.command.impl.author.AuthorCommand;
import by.epam.javatr.controller.command.impl.author.AuthorsCommand;
import by.epam.javatr.controller.command.impl.book.AddBookBlankCommand;
import by.epam.javatr.controller.command.impl.book.AddBookCommand;
import by.epam.javatr.controller.command.impl.book.BookCommand;
import by.epam.javatr.controller.command.impl.book.BooksCommand;
import by.epam.javatr.controller.command.impl.delivery.*;
import by.epam.javatr.controller.command.impl.reservation.*;
import by.epam.javatr.controller.command.impl.subscriber.SubscriberCommand;
import by.epam.javatr.controller.command.impl.subscriber.SubscriberStatusCommand;
import by.epam.javatr.controller.command.impl.subscriber.SubscriberStatusesCommand;
import by.epam.javatr.controller.command.impl.subscriber.SubscribersCommand;
import by.epam.javatr.controller.command.impl.user.*;
import by.epam.javatr.controller.command.util.Uri;

import java.util.HashMap;
import java.util.Map;

public class CommandProvider {

    private static final CommandProvider INSTANCE = new CommandProvider();

    private final Map<String, Command> commands = new HashMap<>();
    {
        commands.put(Uri.ACCOUNT, new AccountCommand());
        commands.put(Uri.AUTHOR, new AuthorCommand());
        commands.put(Uri.AUTHORS, new AuthorsCommand());
        commands.put(Uri.BOOK, new BookCommand());
        commands.put(Uri.BOOKS, new BooksCommand());
        commands.put(Uri.ADD_BOOK_BLANK, new AddBookBlankCommand());
        commands.put(Uri.ADD_BOOK, new AddBookCommand());
        commands.put(Uri.DELIVERY, new DeliveryCommand());
        commands.put(Uri.DELIVERIES, new DeliveriesCommand());
        commands.put(Uri.ADD_DELIVERY, new AddDeliveryCommand());
        commands.put(Uri.ADD_DELIVERY_BLANK, new AddDeliveryFirstBlankCommand());
        commands.put(Uri.ADD_DELIVERY_CONFIRM_BLANK, new AddDeliveryConfirmBlankCommand());
        commands.put(Uri.CLOSE_DELIVERY, new CloseDeliveryCommand());
        commands.put(Uri.DELIVERY_HISTORY, new DeliveryHistory());
        commands.put(Uri.LOCALE, new LocaleCommand());
        commands.put(Uri.LOG_IN_BLANK, new LogInBlankCommand());
        commands.put(Uri.LOG_IN, new LogInCommand());
        commands.put(Uri.LOG_OUT, new LogOutCommand());
        commands.put(Uri.MAIN, new MainPageCommand());
        commands.put(Uri.SUBSCRIBER, new SubscriberCommand());
        commands.put(Uri.SUBSCRIBERS, new SubscribersCommand());
        commands.put(Uri.SUBSCRIBER_STATUS, new SubscriberStatusCommand());
        commands.put(Uri.SUBSCRIBER_STATUSES, new SubscriberStatusesCommand());
        commands.put(Uri.SIGN_UP_BLANK, new SignUpBlankCommand());
        commands.put(Uri.SIGN_UP, new SignUpCommand());
        commands.put(Uri.SEARCHING, new SearchingCommand());
        commands.put(Uri.ADD_AUTHOR_BLANK, new AddAuthorBlankCommand());
        commands.put(Uri.ADD_AUTHOR, new AddAuthorCommand());
        commands.put(Uri.ABOUT_US, new AboutUsCommand());
        commands.put(Uri.RESERVATION, new ReservationCommand());
        commands.put(Uri.RESERVATIONS, new ReservationsCommand());
        commands.put(Uri.ADD_RESERVATION, new AddReservationCommand());
        commands.put(Uri.CLOSE_RESERVATION, new CloseReservationCommand());
        commands.put(Uri.RESERVATION_HISTORY, new ReservationHistory());
    }

    private CommandProvider() {
    }

    public static synchronized CommandProvider getInstance() {
        return INSTANCE;
    }

    public Map<String, Command> getCommands() {
        return commands;
    }
}

