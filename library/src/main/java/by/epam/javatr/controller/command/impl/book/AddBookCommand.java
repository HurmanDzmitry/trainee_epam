package by.epam.javatr.controller.command.impl.book;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.Author;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.enumeration.BookStatus;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class AddBookCommand implements Command {

    private static final String IMAGE_PATH_FULL = "web\\resources\\img\\book\\";
    private static final String IMAGE_PATH_INNER = "resources/img/book/";
    private static final String REGEX_FOR_FIX_PATH = "\\\\target\\\\artifacts\\\\.+";
    private static final String EMPTY_STR = "";

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String title = req.getParameter(ParameterName.TITLE);
        int year = Integer.parseInt(req.getParameter(ParameterName.YEAR));
        int quantityBooks = Integer.parseInt(req.getParameter(ParameterName.QUANTITY));
        int quantityAuthors = Integer.parseInt(req.getParameter(ParameterName.QUANTITY_AUTHORS));
        BookStatus status = BookStatus.valueOf(req.getParameter(ParameterName.STATUS).toUpperCase());

        Set<Author> authorSet = new HashSet<>();
        for (int i = 1; i <= quantityAuthors; i++) {
            int id;
            try {
                id = Integer.parseInt(req.getParameter(ParameterName.ID + i));
            } catch (NumberFormatException e) {
                req.setAttribute(ParameterName.MESSAGE, InvalidKey.WRONG_CHOOSING_AUTHOR.name().toLowerCase());
                return new Router(JspPath.ADD_BOOK, Transition.FORWARD);
            }
            authorSet.add(ServiceFactory.getInstance().getAuthorService().findById(id));
        }

        String image_link;
        try {
//            String uploadPath = req.getServletContext().getRealPath(EMPTY_STR)
//                    .replaceAll(REGEX_FOR_FIX_PATH, EMPTY_STR) + File.separator + IMAGE_PATH_FULL;
            String uploadPath = "D:\\Java\\Code\\MY\\trainee_epam\\library\\src\\main\\web\\resources\\img\\book\\";
            File uploadDir = new File(uploadPath);
            if (!uploadDir.exists()) {
                uploadDir.mkdir();
            }
            String fileName = null;
            for (Part part : req.getParts()) {
                String submittedFileName = part.getSubmittedFileName();
                if (submittedFileName != null && !submittedFileName.isEmpty()) {
                    fileName = submittedFileName;
                }
                if (submittedFileName != null && submittedFileName.isEmpty()) {
                    submittedFileName = null;
                }
                part.write(uploadPath + File.separator + submittedFileName);
            }
            image_link = IMAGE_PATH_INNER + fileName;
        } catch (ServletException | IOException e) {
            throw new ServiceException("Upload image fail.", e);
        }

        Book book = new Book(title, year, quantityBooks, status, image_link, new ArrayList<>(authorSet));
        InvalidKey sik = ServiceFactory.getInstance().getBookService().save(book);
        if (sik != null) {
            req.setAttribute(ParameterName.MESSAGE, sik.name().toLowerCase());
            return new Router(JspPath.ADD_BOOK, Transition.FORWARD);
        }
        req.getSession().setAttribute(ParameterName.BOOK, book);
        return new Router(Uri.BOOK, Transition.REDIRECT);
    }
}
