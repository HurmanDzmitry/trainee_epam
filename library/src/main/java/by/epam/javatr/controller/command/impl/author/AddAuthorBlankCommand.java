package by.epam.javatr.controller.command.impl.author;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddAuthorBlankCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) {
        return new Router(JspPath.ADD_AUTHOR, Transition.FORWARD);
    }
}
