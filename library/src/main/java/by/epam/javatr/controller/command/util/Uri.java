package by.epam.javatr.controller.command.util;

public final class Uri {

    private static final String CONTEXT = "/library";

    public static final String MAIN = CONTEXT + "/main";
    public static final String LOCALE = CONTEXT + "/locale";
    public static final String SEARCHING = CONTEXT + "/search";
    public static final String ABOUT_US = CONTEXT + "/about_us";

    public static final String SIGN_UP = CONTEXT + "/sign_up";
    public static final String SIGN_UP_BLANK = CONTEXT + "/sign_up_blank";
    public static final String LOG_IN = CONTEXT + "/log_in";
    public static final String LOG_IN_BLANK = CONTEXT + "/log_in_blank";
    public static final String LOG_OUT = CONTEXT + "/log_out";
    public static final String ACCOUNT = CONTEXT + "/account";

    public static final String BOOK = CONTEXT + "/book";
    public static final String BOOKS = CONTEXT + "/books";
    public static final String ADD_BOOK_BLANK = CONTEXT + "/add_book_blank";
    public static final String ADD_BOOK = CONTEXT + "/add_book";

    public static final String SUBSCRIBER = CONTEXT + "/subscriber";
    public static final String SUBSCRIBERS = CONTEXT + "/subscribers";

    public static final String SUBSCRIBER_STATUS = CONTEXT + "/subscriber_status";
    public static final String SUBSCRIBER_STATUSES = CONTEXT + "/subscriber_statuses";

    public static final String AUTHOR = CONTEXT + "/author";
    public static final String AUTHORS = CONTEXT + "/authors";
    public static final String ADD_AUTHOR_BLANK = CONTEXT + "/add_author_blank";
    public static final String ADD_AUTHOR = CONTEXT + "/add_author";

    public static final String DELIVERY = CONTEXT + "/delivery";
    public static final String DELIVERIES = CONTEXT + "/deliveries";
    public static final String ADD_DELIVERY_BLANK = CONTEXT + "/add_delivery_blank";
    public static final String ADD_DELIVERY_CONFIRM_BLANK = CONTEXT + "/add_delivery_confirm_blank";
    public static final String ADD_DELIVERY = CONTEXT + "/add_delivery";
    public static final String CLOSE_DELIVERY = CONTEXT + "/closing_delivery";
    public static final String DELIVERY_HISTORY = CONTEXT + "/delivery_history";

    public static final String RESERVATION = CONTEXT + "/reservation";
    public static final String RESERVATIONS = CONTEXT + "/reservations";
    public static final String ADD_RESERVATION = CONTEXT + "/add_reservation";
    public static final String CLOSE_RESERVATION = CONTEXT + "/close_reservation";
    public static final String RESERVATION_HISTORY = CONTEXT + "/reservation_history";

    private Uri() {
    }
}
