package by.epam.javatr.controller.command.util;

public final class JspPath {

    private static final String ROOT = "WEB-INF/jsp";

    public static final String ADD_AUTHOR = ROOT + "/author/add_author.jsp";
    public static final String AUTHOR = ROOT + "/author/author.jsp";
    public static final String AUTHORS = ROOT + "/author/authors.jsp";

    public static final String ADD_BOOK = ROOT + "/book/add_book.jsp";
    public static final String BOOK = ROOT + "/book/book.jsp";
    public static final String BOOKS = ROOT + "/book/books.jsp";

    public static final String ADD_DELIVERY = ROOT + "/delivery/add_delivery_first_step.jsp";
    public static final String ADD_DELIVERY_CONFIRM = ROOT + "/delivery/add_delivery_confirm.jsp";
    public static final String DELIVERY = ROOT + "/delivery/delivery.jsp";
    public static final String DELIVERIES = ROOT + "/delivery/deliveries.jsp";

    public static final String SUBSCRIBER = ROOT + "/subscriber/subscriber.jsp";
    public static final String SUBSCRIBERS = ROOT + "/subscriber/subscribers.jsp";
    public static final String SUBSCRIBER_STATUS = ROOT + "/subscriber/subscriber_status.jsp";
    public static final String SUBSCRIBER_STATUSES = ROOT + "/subscriber/subscriber_statuses.jsp";

    public static final String ACCOUNT = ROOT + "/user/account.jsp";
    public static final String LOG_IN = ROOT + "/user/log_in.jsp";
    public static final String SIGN_UP = ROOT + "/user/sign_up.jsp";

    public static final String ABOUT_US = ROOT + "/about_us.jsp";
    public static final String MAIN = ROOT + "/main.jsp";
    public static final String SEARCH = ROOT + "/search.jsp";

    public static final String RESERVATION = ROOT + "/reservation/reservation.jsp";
    public static final String RESERVATIONS = ROOT + "/reservation/reservations.jsp";

    private JspPath() {
    }
}