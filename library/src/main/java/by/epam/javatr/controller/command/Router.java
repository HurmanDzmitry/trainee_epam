package by.epam.javatr.controller.command;

import java.util.Objects;

public class Router {

    private String address;
    private Transition transition;

    public Router(String address, Transition transition) {
        this.address = address;
        this.transition = transition;
    }

    public String getAddress() {
        return address;
    }

    public Transition getTransition() {
        return transition;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Router router = (Router) o;
        return Objects.equals(address, router.address) &&
                transition == router.transition;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, transition);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "address='" + address + '\'' +
                ", transition=" + transition +
                '}';
    }
}