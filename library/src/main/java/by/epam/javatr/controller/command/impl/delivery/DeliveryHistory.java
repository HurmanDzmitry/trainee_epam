package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.excel.WriteExcel;
import by.epam.javatr.service.util.excel.WriteExcelFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

public class DeliveryHistory implements Command {

    private static final int SIZE_PART = 1048;
    private static final char SLASH = '\\';

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int id = (int) req.getSession().getAttribute(ParameterName.ID);
        Subscriber subscriber = ServiceFactory.getInstance().getSubscriberService().findById(id);
        List<BookDelivery> deliveries = ServiceFactory.getInstance().getDeliveryService().findAllForSubscriber(id);
        WriteExcel<BookDelivery> deliveryWriteExcel = WriteExcelFactory.getInstance().getDeliveryWriteExcel();

        String path = deliveryWriteExcel.write(deliveries, subscriber);
        String fileName = path.substring(path.lastIndexOf(SLASH) + 1);
        resp.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        resp.setHeader("Content-disposition", "attachment; filename="
                + URLEncoder.encode(fileName, StandardCharsets.UTF_8));

        try (InputStream in = new FileInputStream(path);
             OutputStream out = resp.getOutputStream()) {
            byte[] buffer = new byte[SIZE_PART];
            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
        } catch (IOException e) {
            throw new ServiceException("Downloading file fail.", e);
        }
        return null;
    }
}
