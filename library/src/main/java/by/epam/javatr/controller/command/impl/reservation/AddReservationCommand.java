package by.epam.javatr.controller.command.impl.reservation;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.entity.enumeration.BookStatus;
import by.epam.javatr.entity.enumeration.NameSubscriberStatus;
import by.epam.javatr.entity.enumeration.ReservationStatus;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class AddReservationCommand implements Command {

    private static final int MIN_QUANTITY_BOOKS_IN_LIBRARY = 1;
    private static final String EXCEEDING_LIMIT = "limit";

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int subscriberId = (int) req.getSession().getAttribute(ParameterName.ID);
        Subscriber subscriber = ServiceFactory.getInstance().getSubscriberService().findById(subscriberId);
        if (subscriber.getSubscriberStatus().getNameStatus()
                .equals(NameSubscriberStatus.BLOCKED.name().toLowerCase())) {
            req.getSession().setAttribute(ParameterName.MESSAGE, NameSubscriberStatus.BLOCKED.name().toLowerCase());
            return new Router(Uri.ACCOUNT, Transition.REDIRECT);
        }
        int quantityBooksReservedForSubscriber =
                ServiceFactory.getInstance().getReservationService().getQuantityBooksReservedForSubscriber(subscriberId);
        if (quantityBooksReservedForSubscriber >= subscriber.getSubscriberStatus().getLimitReservation()) {
            req.getSession().setAttribute(ParameterName.MESSAGE, EXCEEDING_LIMIT);
            return new Router(Uri.ACCOUNT, Transition.REDIRECT);
        }

        int bookId = Integer.parseInt(req.getParameter(ParameterName.ID));
        Book book = ServiceFactory.getInstance().getBookService().findById(bookId);
        if (book.getBookStatus() == BookStatus.NOT_ISSUED) {
            req.setAttribute(ParameterName.BOOK, book);
            req.setAttribute(ParameterName.MESSAGE, BookStatus.NOT_ISSUED.name().toLowerCase());
            return new Router(JspPath.BOOK, Transition.FORWARD);
        }
        if (book.getQuantity() < MIN_QUANTITY_BOOKS_IN_LIBRARY + 1) {
            req.setAttribute(ParameterName.BOOK, book);
            req.setAttribute(ParameterName.MESSAGE, InvalidKey.WRONG_QUANTITY.name().toLowerCase());
            return new Router(JspPath.BOOK, Transition.FORWARD);
        }

        LocalDate dateOfBegin = LocalDate.now();

        int daysForReservation = subscriber.getSubscriberStatus().getDaysForReservation();
        LocalDate dateOfReset = dateOfBegin.plusDays(daysForReservation);

        BookReservation reservation = new BookReservation(subscriber, book,
                dateOfBegin, dateOfReset, ReservationStatus.IN_ACTION);
        InvalidKey invalidKey = ServiceFactory.getInstance().getReservationService().save(reservation);
        if (invalidKey != null) {
            throw new ServiceException("Invalid reservation.");
        }

        book.setQuantity(book.getQuantity() - 1);
        ServiceFactory.getInstance().getBookService().update(reservation.getBook());
        req.getSession().setAttribute(ParameterName.RESERVATION, reservation);
        return new Router(Uri.ACCOUNT, Transition.REDIRECT);
    }
}
