package by.epam.javatr.controller.command;

public enum Transition {

    FORWARD, REDIRECT
}