package by.epam.javatr.controller.background;

import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;

public class NotMadeReservation implements Runnable {

    private static final Logger logger = LogManager.getLogger(NotMadeReservation.class);

    @Override
    public void run() {
        List<BookReservation> reservations = Collections.emptyList();
        try {
            reservations = ServiceFactory.getInstance().getReservationService().findAllNotMade();
            ServiceFactory.getInstance().getReservationService().updateAllWhenNotMade();
            for (BookReservation reservation : reservations) {
                Book book = reservation.getBook();
                book.setQuantity(book.getQuantity() + 1);
                ServiceFactory.getInstance().getBookService().update(book);
            }
        } catch (ServiceException e) {
            logger.error("Background process of changing reservation and sending message fail.", reservations, e);
        }
    }
}
