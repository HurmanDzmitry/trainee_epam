package by.epam.javatr.controller.filter;

import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.enumeration.Role;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebFilter(urlPatterns = {"/add_book_blank", "/add_book",
        "/subscriber", "/subscribers",
        "/subscriber_status", "/subscriber_statuses",
        "/add_author_blank", "/add_author",
        "/delivery", "/deliveries", "/add_delivery_blank", "/add_delivery",
        "/add_delivery", "/closing_delivery",
        "/reservation", "/reservations", "/close_reservation"})
public class AccessFilter implements Filter {

    private static final Integer ERROR_NUMBER = 403;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
            throws IOException, ServletException {
        HttpSession session = ((HttpServletRequest) servletRequest).getSession();
        Role role = (Role) session.getAttribute(ParameterName.ROLE);

        if (role != Role.LIBRARIAN) {
            ((HttpServletResponse) servletResponse).sendError(ERROR_NUMBER);
            return;
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
