package by.epam.javatr.controller.command.impl.subscriber;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.SubscriberStatus;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SubscriberStatusesCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        List<SubscriberStatus> subscriberStatuses = ServiceFactory.getInstance().getSubscriberStatusService().findAll();
        req.setAttribute(ParameterName.SUBSCRIBER_STATUSES, subscriberStatuses);
        return new Router(JspPath.SUBSCRIBER_STATUSES, Transition.FORWARD);
    }
}
