package by.epam.javatr.controller.command.impl.author;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.Author;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AddAuthorCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String name = req.getParameter(ParameterName.NAME);
        Author author = new Author(name);
        InvalidKey sik = ServiceFactory.getInstance().getAuthorService().save(author);
        if (sik != null) {
            req.setAttribute(ParameterName.MESSAGE, sik.name().toLowerCase());
            return new Router(JspPath.ADD_AUTHOR, Transition.FORWARD);
        }
        req.getSession().setAttribute(ParameterName.AUTHOR, author);
        return new Router(Uri.AUTHOR, Transition.REDIRECT);
    }
}
