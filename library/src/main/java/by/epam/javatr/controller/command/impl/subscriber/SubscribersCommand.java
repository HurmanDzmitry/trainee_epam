package by.epam.javatr.controller.command.impl.subscriber;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.pagination.Pagination;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SubscribersCommand implements Command {

    private static final int ON_PAGE = 20;

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String numberPage = req.getParameter(ParameterName.PAGE);

        List<Subscriber> subscribers = ServiceFactory.getInstance().getSubscriberService().findAll();
        int pages = (int) Math.ceil(subscribers.size() / (double) ON_PAGE);
        subscribers = Pagination.getInstance().paginate(subscribers, numberPage, ON_PAGE);

        req.setAttribute(ParameterName.PAGES, pages);
        req.setAttribute(ParameterName.ON_PAGE, ON_PAGE);
        req.setAttribute(ParameterName.SUBSCRIBERS, subscribers);
        return new Router(JspPath.SUBSCRIBERS, Transition.FORWARD);
    }
}