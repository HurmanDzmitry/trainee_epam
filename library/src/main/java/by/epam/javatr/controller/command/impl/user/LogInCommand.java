package by.epam.javatr.controller.command.impl.user;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.User;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.hashing.HashingPassword;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

public class LogInCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String login = req.getParameter(ParameterName.LOGIN);
        User user = ServiceFactory.getInstance().getUserService().findByLogin(login);
        if (user != null) {
            char[] password = req.getParameter(ParameterName.PASSWORD).toCharArray();
            boolean isEqual = HashingPassword.getInstance().checkPassword(password, user.getPassword());
            Arrays.fill(password, ' ');
            if (user != null && isEqual) {
                req.getSession().setAttribute(ParameterName.ID, user.getIdUser());
                req.getSession().setAttribute(ParameterName.ROLE, user.getRole());
                return new Router(Uri.MAIN, Transition.REDIRECT);
            }
        }
        req.setAttribute(ParameterName.MESSAGE, true);
        return new Router(JspPath.LOG_IN, Transition.FORWARD);
    }
}