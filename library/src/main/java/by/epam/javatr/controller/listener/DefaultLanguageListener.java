package by.epam.javatr.controller.listener;

import by.epam.javatr.controller.command.util.ParameterName;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class DefaultLanguageListener implements HttpSessionListener {

    private static final String DEFAULT_LANGUAGE = "ru";

    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        httpSessionEvent.getSession().setAttribute(ParameterName.LOCALE, DEFAULT_LANGUAGE);
    }
}
