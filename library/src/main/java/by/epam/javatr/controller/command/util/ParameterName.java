package by.epam.javatr.controller.command.util;

public final class ParameterName {

    public static final String YEAR = "year";
    public static final String TITLE = "title";
    public static final String QUANTITY = "quantity";
    public static final String QUANTITY_AUTHORS = "quantity_authors";
    public static final String DESC = "desc";
    public static final String ASC = "asc";
    public static final String AUTHORS = "authors";
    public static final String AUTHOR = "author";
    public static final String ON_PAGE = "on_page";
    public static final String SEARCH = "search";
    public static final String DELIVERIES = "deliveries";
    public static final String DELIVERY = "delivery";
    public static final String PLACE = "place";
    public static final String SUBSCRIBERS = "subscribers";
    public static final String ID_SUBSCRIBER = "id_subscriber";
    public static final String ID_BOOK = "id_book";
    public static final String EXPECTED_DATE = "date_of_return_expected";
    public static final String SUBSCRIBER = "subscriber";
    public static final String ALL = "all";
    public static final String ID = "id";
    public static final String USER = "user";
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String BIRTHDAY = "birthday";
    public static final String ROLE = "role";
    public static final String STATUS = "status";
    public static final String LOCALE = "locale";
    public static final String URI = "uri";
    public static final String MESSAGE = "message";
    public static final String BOOKS = "books";
    public static final String BOOK = "book";
    public static final String PAGE = "page";
    public static final String PAGES = "pages";
    public static final String SORT_HOW = "sort";
    public static final String SORT_BY = "sort_by";
    public static final String SUBSCRIBER_STATUS = "subscriber_status";
    public static final String SUBSCRIBER_STATUSES = "subscriber_statuses";
    public static final String RESERVATIONS = "reservations";
    public static final String RESERVATION = "reservation";
    public static final String RESET_DATE = "reset_date";

    private ParameterName() {
    }
}
