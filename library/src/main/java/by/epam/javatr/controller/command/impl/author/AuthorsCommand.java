package by.epam.javatr.controller.command.impl.author;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Author;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.pagination.Pagination;
import by.epam.javatr.service.util.sorting.factory.SortingFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AuthorsCommand implements Command {

    private static final int AUTHORS_ON_PAGE = 6;

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String numberPage = req.getParameter(ParameterName.PAGE);
        String howSort = req.getParameter(ParameterName.SORT_HOW);

        List<Author> authors = ServiceFactory.getInstance().getAuthorService().findAll();
        authors = SortingFactory.getInstance().getAuthorSorting().sort(authors, howSort);
        int pages = (int) Math.ceil(authors.size() / (double) AUTHORS_ON_PAGE);
        authors = Pagination.getInstance().paginate(authors, numberPage, AUTHORS_ON_PAGE);

        req.setAttribute(ParameterName.PAGES, pages);
        req.setAttribute(ParameterName.ON_PAGE, AUTHORS_ON_PAGE);
        req.setAttribute(ParameterName.AUTHORS, authors);
        return new Router(JspPath.AUTHORS, Transition.FORWARD);
    }
}