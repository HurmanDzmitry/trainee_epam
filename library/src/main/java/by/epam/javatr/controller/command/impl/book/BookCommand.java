package by.epam.javatr.controller.command.impl.book;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.entity.enumeration.Role;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class BookCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        Book book = (Book) req.getSession().getAttribute(ParameterName.BOOK);
        if (book != null) {
            req.getSession().setAttribute(ParameterName.BOOK, null);
            req.setAttribute(ParameterName.BOOK, book);
            return new Router(JspPath.BOOK, Transition.FORWARD);
        }
        int id = Integer.parseInt(req.getParameter(ParameterName.ID));
        book = ServiceFactory.getInstance().getBookService().findById(id);
        req.setAttribute(ParameterName.BOOK, book);

        if (req.getSession().getAttribute(ParameterName.ID) != null
                && req.getSession().getAttribute(ParameterName.ROLE) == Role.SUBSCRIBER) {
            int subscriberId = (int) req.getSession().getAttribute(ParameterName.ID);
            Subscriber subscriber = ServiceFactory.getInstance().getSubscriberService().findById(subscriberId);
            int days_for_reservation = subscriber.getSubscriberStatus().getDaysForReservation();
            LocalDate date_of_reset = LocalDate.now().plusDays(days_for_reservation);
            req.setAttribute(ParameterName.RESET_DATE, date_of_reset);
        }
        return new Router(JspPath.BOOK, Transition.FORWARD);
    }
}
