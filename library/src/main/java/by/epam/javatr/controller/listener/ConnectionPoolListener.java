package by.epam.javatr.controller.listener;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ConnectionPoolListener implements ServletContextListener {

    private static final Logger logger = LogManager.getLogger(ConnectionPoolListener.class);

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    public ConnectionPoolListener() {
    }

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        try {
            CONNECTION_POOL.initPoolData();
        } catch (ConnectionPoolException e) {
            logger.error("Connection pool didn't initialize.", e);
            throw new ExceptionInInitializerError(e);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        CONNECTION_POOL.dispose();
    }
}
