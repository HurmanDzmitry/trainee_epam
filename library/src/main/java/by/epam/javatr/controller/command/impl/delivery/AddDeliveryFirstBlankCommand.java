package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class AddDeliveryFirstBlankCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        List<Subscriber> subscribers = ServiceFactory.getInstance().getSubscriberService().findAll();
        req.setAttribute(ParameterName.SUBSCRIBERS, subscribers);
        List<Book> books = ServiceFactory.getInstance().getBookService().findAll();
        req.setAttribute(ParameterName.BOOKS, books);

        Subscriber subscriber = (Subscriber) req.getSession().getAttribute(ParameterName.SUBSCRIBER);
        Book book = (Book) req.getSession().getAttribute(ParameterName.BOOK);
        if (subscriber != null && book != null) {
            req.setAttribute(ParameterName.SUBSCRIBER, subscriber);
            req.getSession().removeAttribute(ParameterName.SUBSCRIBER);
            req.setAttribute(ParameterName.BOOK, book);
            req.getSession().removeAttribute(ParameterName.BOOK);
        }
        return new Router(JspPath.ADD_DELIVERY, Transition.FORWARD);
    }
}
