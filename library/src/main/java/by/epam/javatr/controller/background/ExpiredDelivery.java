package by.epam.javatr.controller.background;

import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.mail.MailUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collections;
import java.util.List;

public class ExpiredDelivery implements Runnable {

    private static final Logger logger = LogManager.getLogger(ExpiredDelivery.class);

    @Override
    public void run() {
        List<BookDelivery> deliveries = Collections.emptyList();
        try {
            deliveries = ServiceFactory.getInstance().getDeliveryService().findAllExpired();
            ServiceFactory.getInstance().getDeliveryService().updateAllWhenDelay();
            for (BookDelivery delivery : deliveries) {
                MailUtil.getInstance().sendMessageAboutExpired(delivery);
            }
        } catch (ServiceException e) {
            logger.error("Background process of changing delivery and sending message fail.", deliveries, e);
        }
    }
}
