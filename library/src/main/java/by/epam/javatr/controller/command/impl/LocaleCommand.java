package by.epam.javatr.controller.command.impl;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.ParameterName;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LocaleCommand implements Command {

    private static final String REGEX_FOR_FIX_REDIRECT_URI = ".*locale=[a-z]+&uri=";

    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) {
        req.getSession().setAttribute(ParameterName.LOCALE, req.getParameter(ParameterName.LOCALE));
        String redirectUri = req.getQueryString().replaceFirst(REGEX_FOR_FIX_REDIRECT_URI, "");
        return new Router(redirectUri, Transition.REDIRECT);
    }
}
