package by.epam.javatr.controller.command.impl.user;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.controller.command.util.Uri;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.entity.SubscriberStatus;
import by.epam.javatr.entity.enumeration.NameSubscriberStatus;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.hashing.HashingPassword;
import by.epam.javatr.service.validation.factory.ValidatorFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.Arrays;

public class SignUpCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        String name = req.getParameter(ParameterName.NAME);
        String surname = req.getParameter(ParameterName.SURNAME);
        String login = req.getParameter(ParameterName.LOGIN);
        char[] password = req.getParameter(ParameterName.PASSWORD).toCharArray();
        InvalidKey sik = ValidatorFactory.getInstance().getPasswordValidator().validate(password);
        if (sik != null) {
            Arrays.fill(password, ' ');
            req.setAttribute(ParameterName.MESSAGE, sik.name().toLowerCase());
            return new Router(JspPath.SIGN_UP, Transition.FORWARD);
        }
        String hashedPass = HashingPassword.getInstance().hash(password);
        Arrays.fill(password, ' ');
        String birthDayInString = req.getParameter(ParameterName.BIRTHDAY);
        LocalDate birthDay = birthDayInString == null ? (LocalDate.now()) : (LocalDate.parse(birthDayInString));
        String email = req.getParameter(ParameterName.EMAIL);
        SubscriberStatus subscriberStatus =
                ServiceFactory.getInstance().getSubscriberStatusService()
                        .findByName(NameSubscriberStatus.USUAL.name().toLowerCase());
        Subscriber subscriber =
                new Subscriber(name, surname, birthDay, login, hashedPass, email, subscriberStatus);
        sik = ServiceFactory.getInstance().getSubscriberService().save(subscriber);
        if (sik != null) {
            req.setAttribute(ParameterName.MESSAGE, sik.name().toLowerCase());
            return new Router(JspPath.SIGN_UP, Transition.FORWARD);
        }
        req.getSession().setAttribute(ParameterName.ID, subscriber.getIdUser());
        req.getSession().setAttribute(ParameterName.ROLE, subscriber.getRole());
        return new Router(Uri.MAIN, Transition.REDIRECT);
    }
}
