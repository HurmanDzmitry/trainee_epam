package by.epam.javatr.controller.command.impl.subscriber;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

public class SubscriberCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int id = Integer.parseInt(req.getParameter(ParameterName.ID));
        Subscriber subscriber = ServiceFactory.getInstance().getSubscriberService().findById(id);
        List<BookDelivery> deliveries = ServiceFactory.getInstance().getDeliveryService().findAllForSubscriber(id);
        List<BookReservation> reservations = ServiceFactory.getInstance().getReservationService().findAllForSubscriber(id);
        req.setAttribute(ParameterName.SUBSCRIBER, subscriber);
        req.setAttribute(ParameterName.DELIVERIES, deliveries);
        req.setAttribute(ParameterName.RESERVATIONS, reservations);
        return new Router(JspPath.SUBSCRIBER, Transition.FORWARD);
    }
}
