package by.epam.javatr.controller.command.impl.delivery;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;
import by.epam.javatr.controller.command.util.ParameterName;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.enumeration.DeliveryStatus;
import by.epam.javatr.service.entity_service.DeliveryService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;

public class CloseDeliveryCommand implements Command {
    @Override
    public Router execute(HttpServletRequest req, HttpServletResponse resp) throws ServiceException {
        int id = Integer.parseInt(req.getParameter(ParameterName.ID));
        DeliveryService deliveryService = ServiceFactory.getInstance().getDeliveryService();
        BookDelivery delivery = deliveryService.findById(id);
        LocalDate now = LocalDate.now();
        delivery.setDateOfReturnActual(now);
        if (!delivery.getDateOfReturnExpected().isBefore(now)) {
            delivery.setDeliveryStatus(DeliveryStatus.ON_TIME);
        } else {
            delivery.setDeliveryStatus(DeliveryStatus.EXPIRED);
        }
        InvalidKey invalidKey = deliveryService.update(delivery);
        if (invalidKey != null) {
            throw new ServiceException("Invalid delivery");
        }
        Book book = delivery.getBook();
        book.setQuantity(book.getQuantity() + 1);
        ServiceFactory.getInstance().getBookService().update(book);
        req.setAttribute(ParameterName.DELIVERY, delivery);
        return new Router(JspPath.DELIVERY, Transition.FORWARD);
    }
}
