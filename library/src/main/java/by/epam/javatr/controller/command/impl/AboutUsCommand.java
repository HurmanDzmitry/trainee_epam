package by.epam.javatr.controller.command.impl;

import by.epam.javatr.controller.command.Command;
import by.epam.javatr.controller.command.Router;
import by.epam.javatr.controller.command.Transition;
import by.epam.javatr.controller.command.util.JspPath;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AboutUsCommand implements Command {
    public Router execute(HttpServletRequest req, HttpServletResponse resp) {
        return new Router(JspPath.ABOUT_US, Transition.FORWARD);
    }
}
