package by.epam.javatr.dao.db_pool.factory;

import by.epam.javatr.dao.db_pool.ConnectionPool;

public class ConnectionPoolFactory {

    private static final ConnectionPoolFactory INSTANCE = new ConnectionPoolFactory();
    private static final ConnectionPool CONNECTION_POOL = new ConnectionPool();

    private ConnectionPoolFactory() {
    }

    public static synchronized ConnectionPoolFactory getInstance() {
        return INSTANCE;
    }

    public ConnectionPool getConnectionPool() {
        return CONNECTION_POOL;
    }
}
