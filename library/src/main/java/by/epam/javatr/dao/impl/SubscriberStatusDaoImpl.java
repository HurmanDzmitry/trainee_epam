package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.SubscriberStatusDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.impl.util.EntityTemplate;
import by.epam.javatr.entity.SubscriberStatus;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SubscriberStatusDaoImpl implements SubscriberStatusDao {

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    private static final String INSERT = "INSERT INTO subscriber_statuses (name_status, limit_reservation, " +
            "limit_taking, days_for_reservation, days_for_return) " +
            "VALUES (?, ?, ?, ?, ?);";
    private static final String FIND_ALL = "SELECT * FROM subscriber_statuses;";
    private static final String UPDATE = "UPDATE subscriber_statuses SET name_status = ?, limit_reservation = ?, " +
            "limit_taking = ?, days_for_reservation = ?, days_for_return = ? WHERE id_subscriber_status = ?;";
    private static final String DELETE_BY_ID = "DELETE FROM subscriber_statuses WHERE id_subscriber_status = ?;";
    private static final String FIND_BY_NAME = "SELECT * FROM subscriber_statuses WHERE name_status = ?;";

    public SubscriberStatusDaoImpl() {
    }

    @Override
    public SubscriberStatus save(SubscriberStatus entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getNameStatus());
            ps.setInt(2, entity.getLimitReservation());
            ps.setInt(3, entity.getLimitTaking());
            ps.setInt(4, entity.getDaysForReservation());
            ps.setInt(5, entity.getDaysForReturn());
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entity.setIdSubscriberStatus(rs.getInt(1));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return entity;
    }

    @Override
    public List<SubscriberStatus> findAll() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<SubscriberStatus> subscriberStatuses = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL);

            rs = ps.executeQuery();
            while (rs.next()) {
                SubscriberStatus subscriberStatus = EntityTemplate.getSubscriberStatusByTemplate(rs);
                subscriberStatuses.add(subscriberStatus);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return subscriberStatuses;
    }

    @Override
    public SubscriberStatus update(SubscriberStatus entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(UPDATE);
            ps.setString(1, entity.getNameStatus());
            ps.setInt(2, entity.getLimitReservation());
            ps.setInt(3, entity.getLimitTaking());
            ps.setInt(4, entity.getDaysForReservation());
            ps.setInt(5, entity.getDaysForReturn());
            ps.setInt(6, entity.getIdSubscriberStatus());
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
        return entity;
    }

    @Override
    public void deleteById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(DELETE_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public SubscriberStatus findByName(String name) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        SubscriberStatus subscriberStatus = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_NAME);
            ps.setString(1, name);

            rs = ps.executeQuery();
            if (rs.next()) {
                subscriberStatus = EntityTemplate.getSubscriberStatusByTemplate(rs);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return subscriberStatus;
    }
}
