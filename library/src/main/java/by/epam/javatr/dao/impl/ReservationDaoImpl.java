package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.ReservationDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.impl.util.EntityTemplate;
import by.epam.javatr.entity.Author;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookReservation;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ReservationDaoImpl implements ReservationDao {

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    private static final String INSERT = "INSERT INTO book_reservations (id_subscriber, id_book, date_of_begin, " +
            "date_of_reset, reservation_status) VALUES ( ?, ?, ?, ?, ?);";
    private static final String FIND_ALL = "SELECT * FROM book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_reservations.id_book = books.id_book " +
            "ORDER BY date_of_begin DESC;";
    private static final String UPDATE = "UPDATE book_reservations SET id_subscriber = ?, id_book = ?, " +
            "date_of_begin = ?, date_of_reset = ?, reservation_status = ? WHERE id_reservation = ?;";
    private static final String DELETE_BY_ID = "DELETE FROM book_reservations WHERE id_reservation = ?;";
    private static final String FIND_BY_ID = "SELECT * FROM book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_reservations.id_book = books.id_book " +
            " WHERE id_reservation = ?;";
    private static final String GET_QUANTITY_BOOKS = "SELECT COUNT(*) " +
            "FROM book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "WHERE subscribers.id_subscriber = ? " +
            "AND (book_reservations.reservation_status = 'not_done' " +
            "OR book_reservations.reservation_status = 'in_action');";
    private static final String CHANGE_STATUS_OF_NOT_MADE = "UPDATE book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "SET reservation_status = 'not_done' " +
            "WHERE date_of_reset < ? AND reservation_status = 'in_action';";
    private static final String FIND_ALL_NOT_MADE = "SELECT * FROM book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_reservations.id_book = books.id_book " +
            "WHERE date_of_reset < ? AND reservation_status = 'in_action';";
    private static final String FIND_ALL_ACTIVE = "SELECT * FROM book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_reservations.id_book = books.id_book " +
            "WHERE reservation_status = 'in_action' AND subscribers.id_subscriber = ? " +
            "ORDER BY date_of_begin DESC;";
    private static final String FIND_ALL_OWN = "SELECT * FROM book_reservations " +
            "JOIN subscribers ON book_reservations.id_subscriber = subscribers.id_subscriber " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_reservations.id_book = books.id_book " +
            "WHERE subscribers.id_subscriber = ? " +
            "ORDER BY date_of_begin DESC;";

    public ReservationDaoImpl() {
    }

    @Override
    public BookReservation save(BookReservation entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setInt(1, entity.getSubscriber().getIdUser());
            ps.setInt(2, entity.getBook().getIdBook());
            ps.setDate(3, Date.valueOf(entity.getDateOfBegin()));
            ps.setDate(4, Date.valueOf(entity.getDateOfReset()));
            ps.setString(5, entity.getReservationStatus().name().toLowerCase());
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entity.setIdReservation(rs.getInt(1));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return entity;
    }

    @Override
    public List<BookReservation> findAll() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookReservation> bookReservations = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL);

            rs = ps.executeQuery();
            while (rs.next()) {
                BookReservation bookReservation = EntityTemplate.getBookReservationByTemplate(rs);
                Book book = bookReservation.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                bookReservations.add(bookReservation);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookReservations;
    }

    @Override
    public BookReservation update(BookReservation entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(UPDATE);
            ps.setInt(1, entity.getSubscriber().getIdUser());
            ps.setInt(2, entity.getBook().getIdBook());
            ps.setDate(3, Date.valueOf(entity.getDateOfBegin()));
            ps.setDate(4, Date.valueOf(entity.getDateOfReset()));
            ps.setString(5, entity.getReservationStatus().name().toLowerCase());
            ps.setInt(6, entity.getIdReservation());
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
        return entity;
    }

    @Override
    public void deleteById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(DELETE_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public BookReservation findById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        BookReservation bookReservation = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {
                bookReservation = EntityTemplate.getBookReservationByTemplate(rs);
                Book book = bookReservation.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookReservation;
    }

    @Override
    public int getQuantityBooksReservedForSubscriber(int idSubscriber) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int quantity = 0;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(GET_QUANTITY_BOOKS);
            ps.setInt(1, idSubscriber);

            rs = ps.executeQuery();
            if (rs.next()) {
                quantity = rs.getInt(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return quantity;
    }

    @Override
    public List<BookReservation> findAllNotMade() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookReservation> bookReservations = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_NOT_MADE);
            ps.setString(1, Date.valueOf(LocalDate.now()).toString());

            rs = ps.executeQuery();
            while (rs.next()) {
                BookReservation bookReservation = EntityTemplate.getBookReservationByTemplate(rs);
                Book book = bookReservation.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                bookReservations.add(bookReservation);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookReservations;
    }

    @Override
    public void updateAllWhenNotMade() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(CHANGE_STATUS_OF_NOT_MADE);
            ps.setString(1, Date.valueOf(LocalDate.now()).toString());
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
    }

    @Override
    public List<BookReservation> findAllActiveForSubscriber(int idSubscriber) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookReservation> bookReservations = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_ACTIVE);
            ps.setInt(1, idSubscriber);

            rs = ps.executeQuery();
            while (rs.next()) {
                BookReservation bookReservation = EntityTemplate.getBookReservationByTemplate(rs);
                Book book = bookReservation.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                bookReservations.add(bookReservation);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookReservations;
    }

    @Override
    public List<BookReservation> findAllForSubscriber(int idSubscriber) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookReservation> bookReservations = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_OWN);
            ps.setInt(1, idSubscriber);

            rs = ps.executeQuery();
            while (rs.next()) {
                BookReservation bookReservation = EntityTemplate.getBookReservationByTemplate(rs);
                Book book = bookReservation.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                bookReservations.add(bookReservation);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookReservations;
    }

}