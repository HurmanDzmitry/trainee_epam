package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.Book;

import java.util.List;

public interface BookDao {

    Book save(Book entity) throws DaoException;

    List<Book> findAll() throws DaoException;

    Book update(Book entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    List<Book> searchByTitle(String query) throws DaoException;

    List<Book> searchByAuthor(String query) throws DaoException;

    Book findById(int id) throws DaoException;

    List<Book> findByStatus(String status) throws DaoException;

    List<Book> findByAuthorId(int id) throws DaoException;
}
