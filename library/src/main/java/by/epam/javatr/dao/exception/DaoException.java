package by.epam.javatr.dao.exception;

public class DaoException extends Exception {

    public static final long serialVersionUID = 7991671360264154550L;

    public DaoException(String message) {
        super(message);
    }

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

    public DaoException(Throwable cause) {
        super(cause);
    }
}
