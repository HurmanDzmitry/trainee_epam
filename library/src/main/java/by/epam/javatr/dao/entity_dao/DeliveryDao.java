package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.Author;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookDelivery;

import java.util.List;

public interface DeliveryDao {

    BookDelivery save(BookDelivery entity) throws DaoException;

    List<BookDelivery> findAll() throws DaoException;

    BookDelivery findById(int id) throws DaoException;

    BookDelivery update(BookDelivery entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    List<Book> findBooksUsedSubscriber(int id) throws DaoException;

    List<Book> findMostPopularBooks() throws DaoException;

    List<Book> findMostPopularBooksByAuthor(int id) throws DaoException;

    List<Author> findMostPopularAuthors() throws DaoException;

    void updateAllWhenDelay() throws DaoException;

    int getQuantityBooksIssuedForSubscriber(int idSubscriber) throws DaoException;

    List<BookDelivery> findAllExpired() throws DaoException;

    List<BookDelivery> getAllActiveForSubscriber(int idSubscriber) throws DaoException;

    List<BookDelivery> findAllForSubscriber(int idSubscriber) throws DaoException;
}
