package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.User;

public interface UserDao {

    User findByLogin(String login) throws DaoException;

    User findById(int id) throws DaoException;

    User findByEmail(String email) throws DaoException;
}
