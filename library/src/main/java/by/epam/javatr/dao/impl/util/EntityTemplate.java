package by.epam.javatr.dao.impl.util;

import by.epam.javatr.entity.*;
import by.epam.javatr.entity.enumeration.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Collections;

import static by.epam.javatr.dao.impl.util.DBColumnName.*;

public final class EntityTemplate {

    private EntityTemplate() {
    }

    public static Author getAuthorByTemplate(ResultSet rs) throws SQLException {
        int idAuthor = rs.getInt(ID_AUTHOR);
        String nameAuthor = rs.getString(NAME_AUTHOR);

        return new Author(idAuthor, nameAuthor);
    }

    public static Book getBookByTemplate(ResultSet rs) throws SQLException {
        int idBook = rs.getInt(ID_BOOK);
        String title = rs.getString(TITLE_BOOK);
        int year = rs.getInt(YEAR_BOOK);
        int quantity = rs.getInt(QUANTITY_BOOK);
        BookStatus bookStatus = BookStatus.valueOf(rs.getString(STATUS_BOOK).toUpperCase());
        String imageLink = rs.getString(IMAGE_LINK_BOOK);

        return new Book(idBook, title, year, quantity, bookStatus, imageLink, Collections.emptyList());
    }

    public static BookDelivery getBookDeliveryByTemplate(ResultSet rs) throws SQLException {
        int idDelivery = rs.getInt(ID_DELIVERY);
        LocalDate dateOfIssue = rs.getDate(DATE_OF_ISSUE_DELIVERY).toLocalDate();
        LocalDate dateOfReturnActual;
        dateOfReturnActual = rs.getDate(DATE_OF_RETURN_ACTUAL_DELIVERY) == null
                ? null : rs.getDate(DATE_OF_RETURN_ACTUAL_DELIVERY).toLocalDate();
        LocalDate dateOfReturnExpected = rs.getDate(DATE_OF_RETURN_EXPECTED_DELIVERY).toLocalDate();
        DeliveryStatus deliveryStatus = DeliveryStatus.valueOf(rs.getString(STATUS_DELIVERY).toUpperCase());
        DeliveryPlace kindPlace = DeliveryPlace.valueOf(rs.getString(KIND_PLACE_DELIVERY).toUpperCase());

        return new BookDelivery(idDelivery, getSubscriberByTemplate(rs), getBookByTemplate(rs),
                dateOfIssue, dateOfReturnActual, dateOfReturnExpected, deliveryStatus, kindPlace);
    }

    public static Librarian getLibrarianByTemplate(ResultSet rs) throws SQLException {
        LocalDate dateOfBeginningWork = rs.getDate(DATE_BEGINNING_WORK_LIBRARIAN).toLocalDate();
        double salary = rs.getDouble(SALARY_LIBRARIAN);

        return new Librarian(getUserByTemplate(rs), dateOfBeginningWork, salary);
    }

    public static BookReservation getBookReservationByTemplate(ResultSet rs) throws SQLException {
        int idReservation = rs.getInt(ID_RESERVATION);
        LocalDate dateOfBegin = rs.getDate(DATE_OF_BEGIN_RESERVATION).toLocalDate();
        LocalDate dateOfReset = rs.getDate(DATE_OF_RESET_RESERVATION).toLocalDate();
        ReservationStatus deliveryStatus = ReservationStatus.valueOf(rs.getString(STATUS_RESERVATION).toUpperCase());

        return new BookReservation(idReservation, getSubscriberByTemplate(rs), getBookByTemplate(rs),
                dateOfBegin, dateOfReset, deliveryStatus);
    }

    public static Subscriber getSubscriberByTemplate(ResultSet rs) throws SQLException {
        return new Subscriber(getUserByTemplate(rs), getSubscriberStatusByTemplate(rs));
    }

    public static SubscriberStatus getSubscriberStatusByTemplate(ResultSet rs) throws SQLException {
        int idSubscriberStatus = rs.getInt(ID_SUBSCRIBER_STATUS);
        String nameStatus = rs.getString(NAME_SUBSCRIBER_STATUS);
        int limitReservation = rs.getInt(LIMIT_RESERVATION_SUBSCRIBER_STATUS);
        int limitTaking = rs.getInt(LIMIT_TAKING_SUBSCRIBER_STATUS);
        int daysForReservation = rs.getInt(DAYS_RESERVATION_SUBSCRIBER_STATUS);
        int daysForReturn = rs.getInt(DAYS_RETURN_SUBSCRIBER_STATUS);

        return new SubscriberStatus(idSubscriberStatus, nameStatus, limitReservation,
                limitTaking, daysForReservation, daysForReturn);
    }

    public static User getUserByTemplate(ResultSet rs) throws SQLException {
        int idUser = rs.getInt(ID_USER);
        String nameUser = rs.getString(NAME_USER);
        String surnameUser = rs.getString(SURNAME_USER);
        LocalDate dateOfBirth = rs.getDate(BIRTHDAY_USER).toLocalDate();
        String login = rs.getString(LOGIN_USER);
        String password = rs.getString(PASSWORD_USER);
        String email = rs.getString(EMAIL_USER);
        Role role = Role.valueOf(rs.getString(NAME_ROLE).toUpperCase());

        return new User(idUser, nameUser, surnameUser, dateOfBirth, login,
                password, email, role);
    }
}