package by.epam.javatr.dao.factory;

import by.epam.javatr.dao.entity_dao.*;
import by.epam.javatr.dao.impl.*;

public class DaoFactory {

    private static final DaoFactory INSTANCE = new DaoFactory();

    private final AuthorDao authorDao = new AuthorDaoImpl();
    private final BookDao bookDao = new BookDaoImpl();
    private final DeliveryDao deliveryDao = new DeliveryDaoImpl();
    private final LibrarianDao librarianDao = new LibrarianDaoImpl();
    private final ReservationDao reservationDao = new ReservationDaoImpl();
    private final SubscriberDao subscriberDao = new SubscriberDaoImpl();
    private final SubscriberStatusDao subscriberStatusDao = new SubscriberStatusDaoImpl();
    private final UserDao userDao = new UserDaoImpl();

    private DaoFactory() {
    }

    public static synchronized DaoFactory getInstance() {
        return INSTANCE;
    }

    public AuthorDao getAuthorDao() {
        return authorDao;
    }

    public BookDao getBookDao() {
        return bookDao;
    }

    public DeliveryDao getDeliveryDao() {
        return deliveryDao;
    }

    public LibrarianDao getLibrarianDao() {
        return librarianDao;
    }

    public ReservationDao getReservationDao() {
        return reservationDao;
    }

    public SubscriberDao getSubscriberDao() {
        return subscriberDao;
    }

    public SubscriberStatusDao getSubscriberStatusDao() {
        return subscriberStatusDao;
    }

    public UserDao getUserDao() {
        return userDao;
    }
}
