package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.DeliveryDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.impl.util.EntityTemplate;
import by.epam.javatr.entity.Author;
import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.Librarian;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class DeliveryDaoImpl implements DeliveryDao {

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    private static final String ID_BOOK_COLUMN = "id_book";
    private static final String ID_LIBRARIAN_COLUMN = "id_librarian";

    private static final String FIND_MOST_POPULAR_AUTHORS = "SELECT *, COUNT(*) FROM book_deliveries " +
            "JOIN books ON books.id_book = book_deliveries.id_book " +
            "JOIN books_has_authors ON books_has_authors.id_book = books.id_book " +
            "JOIN authors ON books_has_authors.id_author = authors.id_author " +
            "GROUP BY authors.id_author ORDER BY COUNT(*) DESC LIMIT 10";
    private static final String INSERT = "INSERT INTO book_deliveries (id_subscriber, id_librarian, id_book, date_of_issue, " +
            "date_of_return_actual, date_of_return_expected, delivery_status, kind_place) " +
            "VALUES ( ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String FIND_ALL = "SELECT * FROM book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "JOIN librarians ON book_deliveries.id_librarian = librarians.id_librarian " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_deliveries.id_book = books.id_book " +
            "ORDER BY date_of_issue DESC;";
    private static final String FIND_BY_ID = "SELECT * FROM book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "JOIN librarians ON book_deliveries.id_librarian = librarians.id_librarian " +
            "JOIN books ON book_deliveries.id_book = books.id_book " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "WHERE book_deliveries.id_delivery = ?;";
    private static final String FIND_LIBRARIAN_BY_ID = "SELECT * FROM librarians " +
            "JOIN users ON users.id_user = librarians.id_librarian " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "WHERE librarians.id_librarian = ?;";
    private static final String UPDATE = "UPDATE book_deliveries SET id_subscriber = ?, id_librarian = ?, id_book = ?, " +
            "date_of_issue = ?, date_of_return_actual = ?, date_of_return_expected = ?, delivery_status = ?, " +
            "kind_place = ? WHERE id_delivery = ?;";
    private static final String DELETE_BY_ID = "DELETE FROM book_deliveries WHERE id_delivery = ?;";
    private static final String FIND_BOOKS_USED_SUBSCRIBER = "SELECT * FROM book_deliveries " +
            "JOIN books ON books.id_book = book_deliveries.id_book " +
            "JOIN books_has_authors ON books_has_authors.id_book = books.id_book " +
            "JOIN authors ON books_has_authors.id_author = authors.id_author " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "WHERE subscribers.id_subscriber = ? GROUP BY books.id_book " +
            "ORDER BY book_deliveries.date_of_issue DESC LIMIT 6;";
    private static final String FIND_MOST_POPULAR_BOOKS = "SELECT *, COUNT(*) FROM book_deliveries " +
            "JOIN books ON book_deliveries.id_book = books.id_book " +
            "GROUP BY book_deliveries.id_book ORDER BY COUNT(*) DESC LIMIT 10;";
    private static final String FIND_MOST_POPULAR_BOOKS_BY_AUTHOR = "SELECT *, COUNT(*) FROM book_deliveries " +
            "JOIN books ON books.id_book = book_deliveries.id_book " +
            "JOIN books_has_authors ON books_has_authors.id_book = books.id_book " +
            "JOIN authors ON books_has_authors.id_author = authors.id_author " +
            "WHERE authors.id_author = ? GROUP BY books.id_book ORDER BY COUNT(*) DESC LIMIT 10;";
    private static final String CHANGE_STATUS_OF_EXPECTED = "UPDATE book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "SET delivery_status = 'expired', subscribers.id_subscriber_status = " +
            "(SELECT id_subscriber_status FROM subscriber_statuses WHERE name_status = 'blocked') " +
            "WHERE date_of_return_expected < ? AND delivery_status = 'in_action';";
    private static final String GET_QUANTITY_BOOKS = "SELECT COUNT(*) " +
            "FROM book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "WHERE subscribers.id_subscriber = ? " +
            "AND (book_deliveries.delivery_status = 'expired' OR book_deliveries.delivery_status = 'in_action');";
    private static final String FIND_ALL_EXPIRED = "SELECT * FROM book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "JOIN librarians ON book_deliveries.id_librarian = librarians.id_librarian " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_deliveries.id_book = books.id_book " +
            "WHERE date_of_return_expected < ? AND delivery_status = 'in_action';";
    private static final String FIND_ALL_ACTIVE = "SELECT * FROM book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "JOIN librarians ON book_deliveries.id_librarian = librarians.id_librarian " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_deliveries.id_book = books.id_book " +
            "WHERE subscribers.id_subscriber = ? AND date_of_return_actual IS NULL " +
            "ORDER BY date_of_issue DESC;";
    private static final String FIND_ALL_OWN = "SELECT * FROM book_deliveries " +
            "JOIN subscribers ON book_deliveries.id_subscriber = subscribers.id_subscriber " +
            "JOIN librarians ON book_deliveries.id_librarian = librarians.id_librarian " +
            "JOIN users ON subscribers.id_subscriber = users.id_user " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "JOIN subscriber_statuses ON subscriber_statuses.id_subscriber_status = subscribers.id_subscriber_status " +
            "JOIN books ON book_deliveries.id_book = books.id_book " +
            "WHERE subscribers.id_subscriber = ? " +
            "ORDER BY date_of_issue DESC;";

    public DeliveryDaoImpl() {
    }

    private Librarian getLibrarian(Connection cn, int id_librarian) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        Librarian librarian = null;
        try {
            ps = cn.prepareStatement(FIND_LIBRARIAN_BY_ID);
            ps.setInt(1, id_librarian);

            rs = ps.executeQuery();
            if (rs.next()) {
                librarian = EntityTemplate.getLibrarianByTemplate(rs);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return librarian;
    }

    @Override
    public BookDelivery save(BookDelivery entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setInt(1, entity.getSubscriber().getIdUser());
            ps.setInt(2, entity.getLibrarian().getIdUser());
            ps.setInt(3, entity.getBook().getIdBook());
            ps.setDate(4, Date.valueOf(entity.getDateOfIssue()));
            if (entity.getDateOfReturnActual() == null) {
                ps.setDate(5, null);
            } else {
                ps.setDate(5, Date.valueOf(entity.getDateOfReturnActual()));
            }
            ps.setDate(6, Date.valueOf(entity.getDateOfReturnExpected()));
            ps.setString(7, entity.getDeliveryStatus().name().toLowerCase());
            ps.setString(8, entity.getDeliveryPlace().name().toLowerCase());
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entity.setIdDelivery(rs.getInt(1));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return entity;
    }

    @Override
    public List<BookDelivery> findAll() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookDelivery> bookDeliveries = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL);

            rs = ps.executeQuery();
            while (rs.next()) {
                BookDelivery bookDelivery = EntityTemplate.getBookDeliveryByTemplate(rs);
                Book book = bookDelivery.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                Librarian librarian = getLibrarian(cn, rs.getInt(ID_LIBRARIAN_COLUMN));
                bookDelivery.setLibrarian(librarian);
                bookDeliveries.add(bookDelivery);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookDeliveries;
    }

    @Override
    public BookDelivery findById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        BookDelivery bookDelivery = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {
                bookDelivery = EntityTemplate.getBookDeliveryByTemplate(rs);
                Book book = bookDelivery.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                Librarian librarian = getLibrarian(cn, rs.getInt(ID_LIBRARIAN_COLUMN));
                bookDelivery.setLibrarian(librarian);
                book.setAuthors(authors);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookDelivery;
    }

    @Override
    public BookDelivery update(BookDelivery entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(UPDATE);
            ps.setInt(1, entity.getSubscriber().getIdUser());
            ps.setInt(2, entity.getLibrarian().getIdUser());
            ps.setInt(3, entity.getBook().getIdBook());
            ps.setDate(4, Date.valueOf(entity.getDateOfIssue()));
            if (entity.getDateOfReturnActual() == null) {
                ps.setDate(5, null);
            } else {
                ps.setDate(5, Date.valueOf(entity.getDateOfReturnActual()));
            }
            ps.setDate(6, Date.valueOf(entity.getDateOfReturnExpected()));
            ps.setString(7, entity.getDeliveryStatus().name().toLowerCase());
            ps.setString(8, entity.getDeliveryPlace().name().toLowerCase());
            ps.setInt(9, entity.getIdDelivery());
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
        return entity;
    }

    @Override
    public void deleteById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(DELETE_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public List<Book> findBooksUsedSubscriber(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BOOKS_USED_SUBSCRIBER);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public List<Book> findMostPopularBooks() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_MOST_POPULAR_BOOKS);

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public List<Book> findMostPopularBooksByAuthor(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_MOST_POPULAR_BOOKS_BY_AUTHOR);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public List<Author> findMostPopularAuthors() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Author> authors = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_MOST_POPULAR_AUTHORS);

            rs = ps.executeQuery();
            while (rs.next()) {
                Author author = EntityTemplate.getAuthorByTemplate(rs);
                authors.add(author);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return authors;
    }

    @Override
    public void updateAllWhenDelay() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(CHANGE_STATUS_OF_EXPECTED);
            ps.setString(1, Date.valueOf(LocalDate.now()).toString());
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
    }

    @Override
    public int getQuantityBooksIssuedForSubscriber(int idSubscriber) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        int quantity = 0;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(GET_QUANTITY_BOOKS);
            ps.setInt(1, idSubscriber);

            rs = ps.executeQuery();
            if (rs.next()) {
                quantity = rs.getInt(1);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return quantity;
    }

    @Override
    public List<BookDelivery> findAllExpired() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookDelivery> bookDeliveries = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_EXPIRED);
            ps.setString(1, Date.valueOf(LocalDate.now()).toString());

            rs = ps.executeQuery();
            while (rs.next()) {
                BookDelivery bookDelivery = EntityTemplate.getBookDeliveryByTemplate(rs);
                Book book = bookDelivery.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                Librarian librarian = getLibrarian(cn, rs.getInt(ID_LIBRARIAN_COLUMN));
                bookDelivery.setLibrarian(librarian);
                bookDeliveries.add(bookDelivery);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookDeliveries;
    }

    @Override
    public List<BookDelivery> getAllActiveForSubscriber(int idSubscriber) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookDelivery> bookDeliveries = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_ACTIVE);
            ps.setInt(1, idSubscriber);

            rs = ps.executeQuery();
            while (rs.next()) {
                BookDelivery bookDelivery = EntityTemplate.getBookDeliveryByTemplate(rs);
                Book book = bookDelivery.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                Librarian librarian = getLibrarian(cn, rs.getInt(ID_LIBRARIAN_COLUMN));
                bookDelivery.setLibrarian(librarian);
                bookDeliveries.add(bookDelivery);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookDeliveries;
    }

    @Override
    public List<BookDelivery> findAllForSubscriber(int idSubscriber) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<BookDelivery> bookDeliveries = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_OWN);
            ps.setInt(1, idSubscriber);

            rs = ps.executeQuery();
            while (rs.next()) {
                BookDelivery bookDelivery = EntityTemplate.getBookDeliveryByTemplate(rs);
                Book book = bookDelivery.getBook();
                List<Author> authors = BookDaoImpl.getAuthorsForBook(cn, book.getIdBook());
                book.setAuthors(authors);
                Librarian librarian = getLibrarian(cn, rs.getInt(ID_LIBRARIAN_COLUMN));
                bookDelivery.setLibrarian(librarian);
                bookDeliveries.add(bookDelivery);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return bookDeliveries;
    }
}