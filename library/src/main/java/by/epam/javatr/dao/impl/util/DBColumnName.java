package by.epam.javatr.dao.impl.util;

public final class DBColumnName {

    public static final String ID_AUTHOR = "id_author";
    public static final String NAME_AUTHOR = "name_author";

    public static final String ID_BOOK = "id_book";
    public static final String TITLE_BOOK = "title";
    public static final String YEAR_BOOK = "year";
    public static final String QUANTITY_BOOK = "quantity";
    public static final String STATUS_BOOK = "book_status";
    public static final String IMAGE_LINK_BOOK = "image_link";

    public static final String ID_DELIVERY = "id_delivery";
    public static final String DATE_OF_ISSUE_DELIVERY = "date_of_issue";
    public static final String DATE_OF_RETURN_ACTUAL_DELIVERY = "date_of_return_actual";
    public static final String DATE_OF_RETURN_EXPECTED_DELIVERY = "date_of_return_expected";
    public static final String STATUS_DELIVERY = "delivery_status";
    public static final String KIND_PLACE_DELIVERY = "kind_place";

    public static final String ID_LIBRARIAN = "id_librarian";
    public static final String DATE_BEGINNING_WORK_LIBRARIAN = "date_of_beginning_work";
    public static final String SALARY_LIBRARIAN = "salary";

    public static final String ID_RESERVATION = "id_reservation";
    public static final String DATE_OF_BEGIN_RESERVATION = "date_of_begin";
    public static final String DATE_OF_RESET_RESERVATION = "date_of_reset";
    public static final String STATUS_RESERVATION = "reservation_status";

    public static final String ID_ROLE = "id_role";
    public static final String NAME_ROLE = "name_role";

    public static final String ID_SUBSCRIBER = "id_subscriber";

    public static final String ID_SUBSCRIBER_STATUS = "id_subscriber_status";
    public static final String NAME_SUBSCRIBER_STATUS = "name_status";
    public static final String LIMIT_RESERVATION_SUBSCRIBER_STATUS = "limit_reservation";
    public static final String LIMIT_TAKING_SUBSCRIBER_STATUS = "limit_taking";
    public static final String DAYS_RESERVATION_SUBSCRIBER_STATUS = "days_for_reservation";
    public static final String DAYS_RETURN_SUBSCRIBER_STATUS = "days_for_return";

    public static final String ID_USER = "id_user";
    public static final String NAME_USER = "name_user";
    public static final String SURNAME_USER = "surname_user";
    public static final String BIRTHDAY_USER = "date_of_birth";
    public static final String LOGIN_USER = "login";
    public static final String PASSWORD_USER = "password";
    public static final String EMAIL_USER = "email";

    private DBColumnName() {
    }
}
