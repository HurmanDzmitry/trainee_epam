package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.SubscriberStatus;

import java.util.List;

public interface SubscriberStatusDao {

    SubscriberStatus save(SubscriberStatus entity) throws DaoException;

    List<SubscriberStatus> findAll() throws DaoException;

    SubscriberStatus update(SubscriberStatus entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    SubscriberStatus findByName(String name) throws DaoException;
}
