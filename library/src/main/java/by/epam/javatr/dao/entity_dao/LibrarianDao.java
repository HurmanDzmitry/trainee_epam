package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.Librarian;

import java.util.List;

public interface LibrarianDao {

    Librarian save(Librarian entity) throws DaoException;

    List<Librarian> findAll() throws DaoException;

    Librarian update(Librarian entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    Librarian findById(int id) throws DaoException;
}
