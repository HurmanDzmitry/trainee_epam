package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.Author;

import java.util.List;

public interface AuthorDao {

    Author save(Author entity) throws DaoException;

    List<Author> findAll() throws DaoException;

    Author update(Author entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    void deleteByName(String name) throws DaoException;

    List<Author> searchByPartName(String query) throws DaoException;

    Author findById(int id) throws DaoException;

    Author findByName(String name) throws DaoException;
}
