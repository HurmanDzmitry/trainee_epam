package by.epam.javatr.dao.db_pool.exception;

public class ConnectionPoolException extends Exception {

    public static final long serialVersionUID = -3206817096347132716L;

    public ConnectionPoolException(String message) {
        super(message);
    }

    public ConnectionPoolException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConnectionPoolException(Throwable cause) {
        super(cause);
    }
}
