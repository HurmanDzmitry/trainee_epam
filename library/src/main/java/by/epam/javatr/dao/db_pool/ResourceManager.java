package by.epam.javatr.dao.db_pool;

import java.util.ResourceBundle;

public class ResourceManager {

    private static final ResourceManager RESOURCE_MANAGER = new ResourceManager();
    private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle("db");

    private ResourceManager() {
    }

    public static synchronized ResourceManager getInstance() {
        return RESOURCE_MANAGER;
    }

    public String getValue(String key) {
        return RESOURCE_BUNDLE.getString(key);
    }
}
