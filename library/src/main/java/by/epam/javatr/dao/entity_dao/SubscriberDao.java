package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.Subscriber;

import java.util.List;

public interface SubscriberDao {

    Subscriber save(Subscriber entity) throws DaoException;

    List<Subscriber> findAll() throws DaoException;

    Subscriber update(Subscriber entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    Subscriber findById(int id) throws DaoException;
}
