package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.LibrarianDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.impl.util.EntityTemplate;
import by.epam.javatr.entity.Librarian;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class LibrarianDaoImpl implements LibrarianDao {

    private static final Logger logger = LogManager.getLogger(LibrarianDaoImpl.class);

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    private static final String INSERT_LIBRARIAN = "INSERT INTO librarians (id_librarian, date_of_beginning_work, salary)" +
            "VALUES ((SELECT id_user FROM users WHERE login = ?), ?, ?);";
    private static final String INSERT_USER = "INSERT INTO users (name_user, surname_user, date_of_birth, login, password, " +
            "email, id_role) VALUES (?, ?, ?, ?, ?, ?, (SELECT id_role FROM roles WHERE name_role = ?));";
    private static final String FIND_ALL_QUERY = "SELECT * FROM librarians " +
            "JOIN users ON users.id_user = librarians.id_librarian " +
            "JOIN roles ON roles.id_role = users.id_role;";
    private static final String UPDATE_LIBRARIAN = "UPDATE librarians SET date_of_beginning_work = ?, salary = ? " +
            "WHERE id_librarian = ?;";
    private static final String UPDATE_USER = "UPDATE users SET name_user = ?, surname_user = ?, date_of_birth = ?, " +
            "login = ?, password = ?, email = ? WHERE id_user = ?;";
    private static final String DELETE_BY_ID_LIBRARIAN = "DELETE FROM librarians WHERE id_librarian = ?;";
    private static final String DELETE_BY_ID_USER = "DELETE FROM users WHERE id_user = ?;";
    private static final String FIND_BY_ID = "SELECT * FROM librarians " +
            "JOIN users ON users.id_user = librarians.id_librarian " +
            "JOIN roles ON roles.id_role = users.id_role " +
            "WHERE id_librarian = ?;";

    public LibrarianDaoImpl() {
    }

    @Override
    public Librarian save(Librarian entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            cn.setAutoCommit(false);

            ps = cn.prepareStatement(INSERT_USER, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getNameUser());
            ps.setString(2, entity.getSurnameUser());
            ps.setDate(3, Date.valueOf(entity.getDateOfBirth()));
            ps.setString(4, entity.getLogin());
            ps.setString(5, entity.getPassword());
            ps.setString(6, entity.getEmail());
            ps.setString(7, entity.getRole().name().toLowerCase());
            ps.executeUpdate();

            ps = cn.prepareStatement(INSERT_LIBRARIAN);
            ps.setString(1, entity.getLogin());
            ps.setDate(2, Date.valueOf(entity.getDateOfBeginningWork()));
            ps.setDouble(3, entity.getSalary());
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entity.setIdUser(rs.getInt(1));
            }

            cn.commit();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            try {
                cn.rollback();
                throw new DaoException("Database action failed", e);
            } catch (SQLException ex) {
                logger.error("Database action failed, rollback failed", e, ex);
            }
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return entity;
    }

    @Override
    public List<Librarian> findAll() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Librarian> librarians = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL_QUERY);

            rs = ps.executeQuery();
            while (rs.next()) {
                Librarian librarian = EntityTemplate.getLibrarianByTemplate(rs);
                librarians.add(librarian);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return librarians;
    }

    @Override
    public Librarian update(Librarian entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            cn.setAutoCommit(false);

            ps = cn.prepareStatement(UPDATE_USER);
            ps.setString(1, entity.getNameUser());
            ps.setString(2, entity.getSurnameUser());
            ps.setDate(3, Date.valueOf(entity.getDateOfBirth()));
            ps.setString(4, entity.getLogin());
            ps.setString(5, entity.getPassword());
            ps.setString(6, entity.getEmail());
            ps.setInt(7, entity.getIdUser());
            ps.executeUpdate();

            ps = cn.prepareStatement(UPDATE_LIBRARIAN);
            ps.setString(1, entity.getLogin());
            ps.setDate(2, Date.valueOf(entity.getDateOfBeginningWork()));
            ps.setDouble(3, entity.getSalary());
            ps.executeUpdate();

            cn.commit();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            try {
                cn.rollback();
                throw new DaoException("Database action failed", e);
            } catch (SQLException ex) {
                logger.error("Database action failed, rollback failed", e, ex);
            }
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
        return entity;
    }

    @Override
    public void deleteById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            cn.setAutoCommit(false);

            ps = cn.prepareStatement(DELETE_BY_ID_LIBRARIAN);
            ps.setInt(1, id);
            ps.executeUpdate();

            ps = cn.prepareStatement(DELETE_BY_ID_USER);
            ps.setInt(1, id);
            ps.executeUpdate();

            cn.commit();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            try {
                cn.rollback();
                throw new DaoException("Database action failed", e);
            } catch (SQLException ex) {
                logger.error("Database action failed, rollback failed", e, ex);
            }
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public Librarian findById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Librarian librarian = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {
                librarian = EntityTemplate.getLibrarianByTemplate(rs);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return librarian;
    }
}