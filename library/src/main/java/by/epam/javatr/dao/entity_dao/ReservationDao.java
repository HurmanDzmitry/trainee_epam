package by.epam.javatr.dao.entity_dao;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.entity.BookReservation;

import java.util.List;

public interface ReservationDao {

    BookReservation save(BookReservation entity) throws DaoException;

    List<BookReservation> findAll() throws DaoException;

    BookReservation update(BookReservation entity) throws DaoException;

    void deleteById(int id) throws DaoException;

    BookReservation findById(int id) throws DaoException;

    int getQuantityBooksReservedForSubscriber(int id) throws DaoException;

    List<BookReservation> findAllNotMade() throws DaoException;

    void updateAllWhenNotMade() throws DaoException;

    List<BookReservation> findAllActiveForSubscriber(int idSubscriber) throws DaoException;

    List<BookReservation> findAllForSubscriber(int idSubscriber) throws DaoException;
}
