package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.AuthorDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.impl.util.EntityTemplate;
import by.epam.javatr.entity.Author;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class AuthorDaoImpl implements AuthorDao {

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    private static final String INSERT = "INSERT INTO authors (name_author) VALUES (?);";
    private static final String FIND_ALL = "SELECT * FROM authors;";
    private static final String UPDATE = "UPDATE authors SET name_author = ? WHERE id_author = ?;";
    private static final String DELETE_BY_ID = "DELETE FROM authors WHERE id_author = ?;";
    private static final String DELETE_BY_NAME = "DELETE FROM authors WHERE name_author = ?;";
    private static final String SEARCH_BY_PART_NAME = "SELECT * FROM authors WHERE name_author LIKE ?;";
    private static final String FIND_BY_ID = "SELECT * FROM authors WHERE id_author = ?;";
    private static final String FIND_BY_NAME = "SELECT * FROM authors WHERE name_author = ?;";

    public AuthorDaoImpl() {
    }

    @Override
    public Author save(Author entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(INSERT, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getNameAuthor());
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            if (rs.next()) {
                entity.setIdAuthor(rs.getInt(1));
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return entity;
    }

    @Override
    public List<Author> findAll() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Author> authors = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL);

            rs = ps.executeQuery();
            while (rs.next()) {
                Author author = EntityTemplate.getAuthorByTemplate(rs);
                authors.add(author);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return authors;
    }

    @Override
    public Author update(Author entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(UPDATE);
            ps.setString(1, entity.getNameAuthor());
            ps.setInt(2, entity.getIdAuthor());
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
        return entity;
    }

    @Override
    public void deleteById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(DELETE_BY_ID);
            ps.setInt(1, id);
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public void deleteByName(String name) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(DELETE_BY_NAME);
            ps.setString(1, name);
            ps.executeUpdate();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public List<Author> searchByPartName(String query) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Author> authors = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(SEARCH_BY_PART_NAME);
            ps.setString(1, '%' + query + '%');

            rs = ps.executeQuery();
            while (rs.next()) {
                Author author = EntityTemplate.getAuthorByTemplate(rs);
                authors.add(author);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return authors;
    }

    @Override
    public Author findById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {
                author = EntityTemplate.getAuthorByTemplate(rs);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return author;
    }

    @Override
    public Author findByName(String name) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Author author = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_NAME);
            ps.setString(1, name);

            rs = ps.executeQuery();
            if (rs.next()) {
                author = EntityTemplate.getAuthorByTemplate(rs);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed.", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return author;
    }
}
