package by.epam.javatr.dao.impl;

import by.epam.javatr.dao.db_pool.ConnectionPool;
import by.epam.javatr.dao.db_pool.exception.ConnectionPoolException;
import by.epam.javatr.dao.db_pool.factory.ConnectionPoolFactory;
import by.epam.javatr.dao.entity_dao.BookDao;
import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.impl.util.EntityTemplate;
import by.epam.javatr.entity.Author;
import by.epam.javatr.entity.Book;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDaoImpl implements BookDao {

    private static final Logger logger = LogManager.getLogger(BookDaoImpl.class);

    private static final ConnectionPool CONNECTION_POOL = ConnectionPoolFactory.getInstance().getConnectionPool();

    private static final String ID_BOOK_COLUMN = "id_book";
    private static final String INSERT_BOOK = "INSERT INTO books (title, year, quantity, book_status, image_link)" +
            "VALUES (?, ?, ?, ?, ?);";
    private static final String INSERT_AUTHORS_FOR_BOOK = "INSERT INTO books_has_authors (id_author, id_book)" +
            "VALUES ((SELECT id_author FROM authors WHERE name_author = ?), ?);";
    private static final String FIND_ALL = "SELECT * FROM books;";
    private static final String FIND_AUTHORS_FOR_BOOK = "SELECT  authors.id_author, authors.name_author " +
            "FROM books_has_authors " +
            "JOIN books ON books_has_authors.id_book = books.id_book " +
            "JOIN authors ON books_has_authors.id_author = authors.id_author " +
            "WHERE books_has_authors.id_book = ?;";
    private static final String UPDATE_BOOK = "UPDATE books SET title = ?, year = ?, quantity = ?, " +
            "book_status = ?, image_link = ? WHERE id_book = ?;";
    private static final String UPDATE_AUTHORS_FOR_BOOK = "UPDATE books_has_authors SET id_author = ? " +
            "WHERE id_book = ?;";
    private static final String DELETE_BY_ID_BOOK = "DELETE FROM books WHERE id_book = ?;";
    private static final String DELETE_BY_ID_AUTHORS_FOR_BOOK = "DELETE FROM books_has_authors WHERE id_book = ?;";
    private static final String SEARCH_BY_PART_TITLE = "SELECT * FROM books WHERE title LIKE ?;";
    private static final String SEARCH_BY_AUTHOR = "SELECT * FROM books_has_authors " +
            "JOIN books ON books_has_authors.id_book = books.id_book " +
            "JOIN authors ON books_has_authors.id_author = authors.id_author " +
            "WHERE name_author LIKE ?;";
    private static final String FIND_BY_ID = "SELECT * FROM books WHERE id_book = ?;";
    private static final String FIND_BY_STATUS = "SELECT * FROM books WHERE book_status = ?;";
    private static final String FIND_BY_AUTHOR_ID = "SELECT * FROM books_has_authors " +
            "JOIN books ON books_has_authors.id_book = books.id_book " +
            "JOIN authors ON books_has_authors.id_author = authors.id_author " +
            "WHERE authors.id_author = ?;";

    public BookDaoImpl() {
    }

    public static List<Author> getAuthorsForBook(Connection cn, int id_book) throws SQLException {
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Author> authors = new ArrayList<>();
        try {
            ps = cn.prepareStatement(FIND_AUTHORS_FOR_BOOK);
            ps.setInt(1, id_book);

            rs = ps.executeQuery();
            while (rs.next()) {
                Author author = EntityTemplate.getAuthorByTemplate(rs);
                authors.add(author);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
        }
        return authors;
    }

    @Override
    public Book save(Book entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            cn.setAutoCommit(false);

            ps = cn.prepareStatement(INSERT_BOOK, PreparedStatement.RETURN_GENERATED_KEYS);
            ps.setString(1, entity.getTitle());
            ps.setInt(2, entity.getYear());
            ps.setInt(3, entity.getQuantity());
            ps.setString(4, entity.getBookStatus().name().toLowerCase());
            ps.setString(5, entity.getImageLink());
            ps.executeUpdate();

            rs = ps.getGeneratedKeys();
            Integer id = null;
            if (rs.next()) {
                id = rs.getInt(1);
                entity.setIdBook(id);
            }

            ps = cn.prepareStatement(INSERT_AUTHORS_FOR_BOOK);
            if (id != null) {
                ps.setInt(2, id);
            }
            for (Author a : entity.getAuthors()) {
                ps.setString(1, a.getNameAuthor());
                ps.executeUpdate();
            }

            cn.commit();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            try {
                cn.rollback();
                throw new DaoException("Database action failed", e);
            } catch (SQLException ex) {
                logger.error("Database action failed, rollback failed", e, ex);
            }
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return entity;
    }

    @Override
    public List<Book> findAll() throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_ALL);

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public Book update(Book entity) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            cn.setAutoCommit(false);

            ps = cn.prepareStatement(UPDATE_BOOK);
            ps.setString(1, entity.getTitle());
            ps.setInt(2, entity.getYear());
            ps.setInt(3, entity.getQuantity());
            ps.setString(4, entity.getBookStatus().name().toLowerCase());
            ps.setString(5, entity.getImageLink());
            ps.setInt(6, entity.getIdBook());
            ps.executeUpdate();

            ps = cn.prepareStatement(UPDATE_AUTHORS_FOR_BOOK);
            ps.setInt(2, entity.getIdBook());
            for (Author a : entity.getAuthors()) {
                ps.setInt(1, a.getIdAuthor());
                ps.executeUpdate();
            }
            cn.commit();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            try {
                cn.rollback();
                throw new DaoException("Database action failed", e);
            } catch (SQLException ex) {
                logger.error("Database action failed, rollback failed", e, ex);
            }
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
        return entity;
    }

    @Override
    public void deleteById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        try {
            cn = CONNECTION_POOL.takeConnection();
            cn.setAutoCommit(false);

            ps = cn.prepareStatement(DELETE_BY_ID_AUTHORS_FOR_BOOK);
            ps.setInt(1, id);
            ps.executeUpdate();

            ps = cn.prepareStatement(DELETE_BY_ID_BOOK);
            ps.setInt(1, id);
            ps.executeUpdate();

            cn.commit();
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            try {
                cn.rollback();
                throw new DaoException("Database action failed", e);
            } catch (SQLException ex) {
                logger.error("Database action failed, rollback failed", e, ex);
            }
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps);
        }
    }

    @Override
    public List<Book> searchByTitle(String query) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(SEARCH_BY_PART_TITLE);
            ps.setString(1, '%' + query + '%');

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public List<Book> searchByAuthor(String query) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(SEARCH_BY_AUTHOR);
            ps.setString(1, '%' + query + '%');

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public Book findById(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        Book book = null;
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_ID);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            if (rs.next()) {
                List<Author> authors = getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return book;
    }

    @Override
    public List<Book> findByStatus(String status) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_STATUS);
            ps.setString(1, status);

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }

    @Override
    public List<Book> findByAuthorId(int id) throws DaoException {
        Connection cn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Book> books = new ArrayList<>();
        try {
            cn = CONNECTION_POOL.takeConnection();

            ps = cn.prepareStatement(FIND_BY_AUTHOR_ID);
            ps.setInt(1, id);

            rs = ps.executeQuery();
            while (rs.next()) {
                List<Author> authors = getAuthorsForBook(cn, rs.getInt(ID_BOOK_COLUMN));
                Book book = EntityTemplate.getBookByTemplate(rs);
                book.setAuthors(authors);
                books.add(book);
            }
        } catch (ConnectionPoolException e) {
            throw new DaoException("Сonnection not received.", e);
        } catch (SQLException e) {
            throw new DaoException("Database action failed", e);
        } finally {
            CONNECTION_POOL.closeConnection(cn, ps, rs);
        }
        return books;
    }
}
