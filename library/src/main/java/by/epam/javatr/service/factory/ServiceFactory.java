package by.epam.javatr.service.factory;

import by.epam.javatr.service.entity_service.*;
import by.epam.javatr.service.impl.*;

public class ServiceFactory {

    private static final ServiceFactory INSTANCE = new ServiceFactory();

    private final AuthorService authorService = new AuthorServiceImpl();
    private final BookService bookService = new BookServiceImpl();
    private final DeliveryService deliveryService = new DeliveryServiceImpl();
    private final LibrarianService librarianService = new LibrarianServiceImpl();
    private final ReservationService reservationService = new ReservationServiceImpl();
    private final SubscriberService subscriberService = new SubscriberServiceImpl();
    private final SubscriberStatusService subscriberStatusService = new SubscriberStatusServiceImpl();
    private final UserService userService = new UserServiceImpl();

    private ServiceFactory() {
    }

    public static synchronized ServiceFactory getInstance() {
        return INSTANCE;
    }

    public AuthorService getAuthorService() {
        return authorService;
    }

    public BookService getBookService() {
        return bookService;
    }

    public DeliveryService getDeliveryService() {
        return deliveryService;
    }

    public LibrarianService getLibrarianService() {
        return librarianService;
    }

    public ReservationService getReservationService() {
        return reservationService;
    }

    public SubscriberService getSubscriberService() {
        return subscriberService;
    }

    public SubscriberStatusService getSubscriberStatusService() {
        return subscriberStatusService;
    }

    public UserService getUserService() {
        return userService;
    }
}
