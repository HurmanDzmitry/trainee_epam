package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.User;
import by.epam.javatr.service.entity_service.UserService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.factory.ValidatorFactory;

public class UserServiceImpl implements UserService {
    @Override
    public User findByLogin(String login) throws ServiceException {
        if (login == null) {
            throw new ServiceException("Parameter is null.");
        }
        try {
            return DaoFactory.getInstance().getUserDao().findByLogin(login);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public User findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getUserDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public User findByEmail(String email) throws ServiceException {
        try {
            return DaoFactory.getInstance().getUserDao().findByEmail(email);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}
