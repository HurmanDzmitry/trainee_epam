package by.epam.javatr.service.validation.impl;

import by.epam.javatr.entity.Book;
import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.time.LocalDate;

public class BookValidatorImpl implements Validator<Book> {

    private final int maxYear = LocalDate.now().getYear();

    private static final String TITLE_PATTERN = "^([A-Za-z\\w .,:;!?]|[а-яА-Я\\w .,:;!?]){1,100}$";
    private static final int MIN_YEAR = 1900;
    private static final int MIN_QUANTITY = 1;
    private static final int MAX_QUANTITY = 100;

    @Override
    public InvalidKey validate(Book book) {
        if (!book.getTitle().matches(TITLE_PATTERN)) {
            return InvalidKey.WRONG_TITLE;
        }
        if (book.getYear() < MIN_YEAR || book.getYear() > maxYear) {
            return InvalidKey.WRONG_YEAR;
        }
        if (book.getQuantity() < MIN_QUANTITY || book.getQuantity() > MAX_QUANTITY) {
            return InvalidKey.WRONG_QUANTITY;
        }
        return null;
    }
}
