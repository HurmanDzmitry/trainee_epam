package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.Book;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public interface BookService {

    InvalidKey save(Book entity) throws ServiceException;

    List<Book> findAll() throws ServiceException;

    InvalidKey update(Book entity) throws ServiceException;

    void deleteById(int id) throws ServiceException;

    List<Book> searchByTitle(String query) throws ServiceException;

    List<Book> findMostPopularBook() throws ServiceException;

    Book findById(int id) throws ServiceException;

    List<Book> findByStatus(String status) throws ServiceException;

    List<Book> findByAuthorId(int id) throws ServiceException;

    List<Book> findBooksUsedSubscriber(int id) throws ServiceException;
}
