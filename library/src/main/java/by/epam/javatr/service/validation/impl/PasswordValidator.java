package by.epam.javatr.service.validation.impl;

import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

public class PasswordValidator implements Validator<char[]> {

    private static final String PASSWORD_PATTERN = "^(?=.*[\\d])(?=.*[a-zа-я])(?=.*[A-ZА-Я])(?=\\S+$).{8,20}$";

    @Override
    public InvalidKey validate(char[] password) {
        if (!new String(password).matches(PASSWORD_PATTERN)) {
            return InvalidKey.WRONG_PASSWORD;
        }
        return null;
    }
}
