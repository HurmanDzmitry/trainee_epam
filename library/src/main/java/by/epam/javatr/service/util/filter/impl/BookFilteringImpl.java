package by.epam.javatr.service.util.filter.impl;

import by.epam.javatr.entity.Book;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.util.filter.Filtering;

import java.util.List;

public class BookFilteringImpl implements Filtering<Book> {

    private static final String ALL = "all";

    @Override
    public List<Book> filter(String... specific) throws ServiceException {
        String status = specific[0];
        if (status == null || status.equals(ALL)) {
            return ServiceFactory.getInstance().getBookService().findAll();
        } else {
            return ServiceFactory.getInstance().getBookService().findByStatus(status);
        }
    }
}