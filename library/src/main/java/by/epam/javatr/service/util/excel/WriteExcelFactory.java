package by.epam.javatr.service.util.excel;

import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.util.excel.impl.DeliveryWriteExcel;
import by.epam.javatr.service.util.excel.impl.ReservationWriteExcel;

public class WriteExcelFactory {

    private static final WriteExcelFactory INSTANCE = new WriteExcelFactory();

    private final WriteExcel<BookDelivery> deliveryWriteExcel = new DeliveryWriteExcel();
    private final WriteExcel<BookReservation> reservationWriteExcel = new ReservationWriteExcel();

    private WriteExcelFactory() {
    }

    public static synchronized WriteExcelFactory getInstance() {
        return INSTANCE;
    }

    public WriteExcel<BookDelivery> getDeliveryWriteExcel() {
        return deliveryWriteExcel;
    }

    public WriteExcel<BookReservation> getReservationWriteExcel() {
        return reservationWriteExcel;
    }
}
