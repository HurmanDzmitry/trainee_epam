package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.Book;
import by.epam.javatr.service.entity_service.BookService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.factory.ValidatorFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public class BookServiceImpl implements BookService {
    @Override
    public InvalidKey save(Book book) throws ServiceException {
        InvalidKey sik = ValidatorFactory.getInstance().getBookValidator().validate(book);
        if (sik != null) {
            return sik;
        }
        try {
            DaoFactory.getInstance().getBookDao().save(book);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public InvalidKey update(Book book) throws ServiceException {
        InvalidKey sik = ValidatorFactory.getInstance().getBookValidator().validate(book);
        if (sik != null) {
            return sik;
        }
        try {
            DaoFactory.getInstance().getBookDao().update(book);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public void deleteById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            DaoFactory.getInstance().getBookDao().deleteById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }

    }

    @Override
    public List<Book> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getBookDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public Book findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getBookDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Book> searchByTitle(String query) throws ServiceException {
        try {
            return DaoFactory.getInstance().getBookDao().searchByTitle(query);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Book> findByStatus(String status) throws ServiceException {
        try {
            return DaoFactory.getInstance().getBookDao().findByStatus(status);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Book> findMostPopularBook() throws ServiceException {
        try {
            return DaoFactory.getInstance().getDeliveryDao().findMostPopularBooks();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Book> findByAuthorId(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getBookDao().findByAuthorId(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Book> findBooksUsedSubscriber(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getDeliveryDao().findBooksUsedSubscriber(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}
