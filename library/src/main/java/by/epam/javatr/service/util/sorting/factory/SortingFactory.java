package by.epam.javatr.service.util.sorting.factory;

import by.epam.javatr.service.util.sorting.Sorting;
import by.epam.javatr.service.util.sorting.impl.AuthorSortingImpl;
import by.epam.javatr.service.util.sorting.impl.BookSortingImpl;

public class SortingFactory {

    private static final SortingFactory INSTANCE = new SortingFactory();

    private final Sorting bookSorting = new BookSortingImpl();
    private final Sorting authorSorting = new AuthorSortingImpl();

    private SortingFactory() {
    }

    public static synchronized SortingFactory getInstance() {
        return INSTANCE;
    }

    public Sorting getBookSorting() {
        return bookSorting;
    }

    public Sorting getAuthorSorting() {
        return authorSorting;
    }

}
