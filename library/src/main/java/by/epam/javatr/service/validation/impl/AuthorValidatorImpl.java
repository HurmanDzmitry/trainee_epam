package by.epam.javatr.service.validation.impl;

import by.epam.javatr.entity.Author;
import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

public class AuthorValidatorImpl implements Validator<Author> {

    private static final String NAME_PATTERN = "^([а-яА-Я-]){2,20}?([а-яА-Я-]{2,20})?$";

    @Override
    public InvalidKey validate(Author entity) {
        if (!entity.getNameAuthor().matches(NAME_PATTERN)) {
            return InvalidKey.WRONG_NAME;
        }
        return null;
    }
}
