package by.epam.javatr.service.util.pagination;

import java.util.List;

public class Pagination<T> {

    private static final Pagination INSTANCE = new Pagination();

    private Pagination() {
    }

    public static synchronized Pagination getInstance() {
        return INSTANCE;
    }

    public List<T> paginate(List<T> t, String numberPage, int onOnePage) {
        int page;
        if (numberPage != null) {
            page = Integer.parseInt(numberPage);
        } else {
            page = 1;
        }
        int begin = (page - 1) * onOnePage;
        int end = page * onOnePage;
        if (end > t.size()) {
            end = t.size();
        }
        t = t.subList(begin, end);
        return t;
    }
}
