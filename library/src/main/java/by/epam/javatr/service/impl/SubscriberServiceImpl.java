package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.entity_service.SubscriberService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.factory.ServiceFactory;
import by.epam.javatr.service.validation.factory.ValidatorFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public class SubscriberServiceImpl implements SubscriberService {
    @Override
    public InvalidKey save(Subscriber subscriber) throws ServiceException {
        if (subscriber == null) {
            throw new ServiceException("Parameter is null.");
        }
        if (ServiceFactory.getInstance().getUserService().findByLogin(subscriber.getLogin()) != null) {
            return InvalidKey.BUSY_LOGIN;
        }
        if (ServiceFactory.getInstance().getUserService().findByEmail(subscriber.getEmail()) != null) {
            return InvalidKey.BUSY_EMAIL;
        }
        InvalidKey sik = ValidatorFactory.getInstance().getSubscriberValidator().validate(subscriber);
        if (sik != null) {
            return sik;
        }
        try {
            DaoFactory.getInstance().getSubscriberDao().save(subscriber);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Subscriber> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getSubscriberDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public Subscriber findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getSubscriberDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}
