package by.epam.javatr.service.exception;

public class ServiceException extends Exception {

    public static final long serialVersionUID = 1088831225030164100L;

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
