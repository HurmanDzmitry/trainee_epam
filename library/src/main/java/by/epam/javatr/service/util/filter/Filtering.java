package by.epam.javatr.service.util.filter;

import by.epam.javatr.service.exception.ServiceException;

import java.util.List;

public interface Filtering<T> {

    List<T> filter(String... specific) throws ServiceException;
}
