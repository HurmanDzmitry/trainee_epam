package by.epam.javatr.service.util.mail;

import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.exception.ServiceException;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

public class MailUtil {

    private static final MailUtil INSTANCE = new MailUtil();

    private static final String GREETING = "Здравствуйте, ";
    private static final String EMPTY = " ";
    private static final String SUBJECT_EXPIRED = "Истек срок сдачи книги";
    private static final String TEXT_EXPIRED_PART_1 = ".\nВы не сдали в срок (";
    private static final String TEXT_EXPIRED_PART_2 = ") книгу \"";
    private static final String TEXT_EXPIRED_PART_3 = "\". Просим вернуть в ближайшее время.";

    private static final Properties PROPERTIES;

    static {
        try {
            PROPERTIES = getProperties();
        } catch (ServiceException e) {
            throw new ExceptionInInitializerError(e);
        }
    }

    private final String name = PROPERTIES.getProperty("mail.smtp.login");
    private final String email = PROPERTIES.getProperty("mail.smtp.email");
    private final String password = PROPERTIES.getProperty("mail.smtp.password");

    private MailUtil() {
    }

    public static synchronized MailUtil getInstance() {
        return INSTANCE;
    }

    public void sendMessageAboutExpired(BookDelivery delivery) throws ServiceException {
        String text = GREETING +
                delivery.getSubscriber().getNameUser() +
                EMPTY +
                delivery.getSubscriber().getSurnameUser() +
                TEXT_EXPIRED_PART_1 +
                delivery.getDateOfReturnExpected() +
                TEXT_EXPIRED_PART_2 +
                delivery.getBook().getTitle() +
                TEXT_EXPIRED_PART_3;

        sendMessage(delivery.getSubscriber().getEmail(), SUBJECT_EXPIRED, text);
    }

    private void sendMessage(String recipientEmail, String subject, String text) throws ServiceException {
        Session session = Session.getInstance(PROPERTIES, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(email, password);
            }
        });
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(email, name));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail));
            message.setSubject(subject);
            message.setText(text);

            Transport.send(message);
        } catch (MessagingException | UnsupportedEncodingException e) {
            throw new ServiceException("Error sending message.", e);
        }
    }

    private static Properties getProperties() throws ServiceException {
        Properties props = new Properties();
        try {
            props.load(MailUtil.class.getClassLoader().getResourceAsStream("mail.properties"));
        } catch (IOException e) {
            throw new ServiceException("Error reading property file.", e);
        }
        return props;
    }
}
