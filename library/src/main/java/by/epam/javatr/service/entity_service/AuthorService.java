package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.Author;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public interface AuthorService {

    InvalidKey save(Author entity) throws ServiceException;

    List<Author> findAll() throws ServiceException;

    List<Author> searchByPartName(String query) throws ServiceException;

    Author findById(int id) throws ServiceException;

    Author findByName(String name) throws ServiceException;
}
