package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.SubscriberStatus;
import by.epam.javatr.service.entity_service.SubscriberStatusService;
import by.epam.javatr.service.exception.ServiceException;

import java.util.List;

public class SubscriberStatusServiceImpl implements SubscriberStatusService {
    @Override
    public SubscriberStatus findByName(String name) throws ServiceException {
        if (name == null) {
            throw new ServiceException("Parameter is null.");
        }
        try {
            return DaoFactory.getInstance().getSubscriberStatusDao().findByName(name);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<SubscriberStatus> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getSubscriberStatusDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}
