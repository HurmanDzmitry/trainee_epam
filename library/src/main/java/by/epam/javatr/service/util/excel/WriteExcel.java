package by.epam.javatr.service.util.excel;

import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;

import java.util.List;

public interface WriteExcel<T> {

    String write(List<T> data, Subscriber subscriber) throws ServiceException;
}
