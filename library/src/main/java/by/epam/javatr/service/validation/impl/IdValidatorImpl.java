package by.epam.javatr.service.validation.impl;

import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

public class IdValidatorImpl implements Validator<Integer> {

    @Override
    public InvalidKey validate(Integer id) {
        if (id <= 0) {
            return InvalidKey.WRONG_ID;
        }
        return null;
    }
}
