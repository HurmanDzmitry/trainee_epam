package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public interface SubscriberService {

    InvalidKey save(Subscriber entity) throws ServiceException;

    List<Subscriber> findAll() throws ServiceException;

    Subscriber findById(int id) throws ServiceException;
}
