package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.User;
import by.epam.javatr.service.exception.ServiceException;

public interface UserService {

    User findByLogin(String login) throws ServiceException;

    User findById(int id) throws ServiceException;

    User findByEmail(String email) throws ServiceException;
}
