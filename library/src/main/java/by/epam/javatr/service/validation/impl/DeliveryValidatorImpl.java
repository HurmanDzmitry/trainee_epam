package by.epam.javatr.service.validation.impl;

import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.time.LocalDate;

public class DeliveryValidatorImpl implements Validator<BookDelivery> {

    @Override
    public InvalidKey validate(BookDelivery bookDelivery) {
        if (bookDelivery.getDateOfIssue().isAfter(LocalDate.now())
                || bookDelivery.getDateOfIssue().isAfter(bookDelivery.getDateOfReturnExpected())) {
            return InvalidKey.WRONG_DATE;
        }
        return null;
    }
}
