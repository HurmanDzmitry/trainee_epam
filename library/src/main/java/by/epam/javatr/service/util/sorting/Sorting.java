package by.epam.javatr.service.util.sorting;

import java.util.List;

public interface Sorting<T> {

    List<T> sort(List<T> t, String... specific);
}
