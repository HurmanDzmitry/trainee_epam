package by.epam.javatr.service.util.sorting.impl;

import by.epam.javatr.entity.Author;
import by.epam.javatr.service.util.sorting.Sorting;

import java.util.Collections;
import java.util.List;

public class AuthorSortingImpl implements Sorting<Author> {

    private static final String DESC = "desc";

    @Override
    public List<Author> sort(List<Author> authors, String... specific) {
        String howSort = specific[0];
        if (howSort != null && howSort.equals(DESC)) {
            Collections.reverse(authors);
        }
        return authors;
    }
}
