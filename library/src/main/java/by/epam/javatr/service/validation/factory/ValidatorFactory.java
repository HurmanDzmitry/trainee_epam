package by.epam.javatr.service.validation.factory;

import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.impl.*;

public class ValidatorFactory {

    private static ValidatorFactory INSTANCE;

    private final Validator authorValidator = new AuthorValidatorImpl();
    private final Validator bookValidator = new BookValidatorImpl();
    private final Validator deliveryValidator = new DeliveryValidatorImpl();
    private final Validator idValidator = new IdValidatorImpl();
    private final Validator passwordValidator = new PasswordValidator();
    private final Validator reservationValidator = new ReservationValidatorImpl();
    private final Validator subscriberValidator = new SubscriberValidatorImpl();

    private ValidatorFactory() {
    }

    public static synchronized ValidatorFactory getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new ValidatorFactory();
        }
        return INSTANCE;
    }

    public Validator getAuthorValidator() {
        return authorValidator;
    }

    public Validator getDeliveryValidator() {
        return deliveryValidator;
    }

    public Validator getIdValidator() {
        return idValidator;
    }

    public Validator getBookValidator() {
        return bookValidator;
    }

    public Validator getPasswordValidator() {
        return passwordValidator;
    }

    public Validator getReservationValidator() {
        return reservationValidator;
    }

    public Validator getSubscriberValidator() {
        return subscriberValidator;
    }

}
