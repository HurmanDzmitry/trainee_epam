package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public interface ReservationService {

    InvalidKey save(BookReservation entity) throws ServiceException;

    List<BookReservation> findAll() throws ServiceException;

    BookReservation findById(int id) throws ServiceException;

    int getQuantityBooksReservedForSubscriber(int idSubscriber) throws ServiceException;

    InvalidKey update(BookReservation reservation) throws ServiceException;

    List<BookReservation> findAllNotMade() throws ServiceException;

    void updateAllWhenNotMade() throws ServiceException;

    List<BookReservation> findAllActiveForSubscriber(int idSubscriber) throws ServiceException;

    List<BookReservation> findAllForSubscriber(int idSubscriber) throws ServiceException;
}
