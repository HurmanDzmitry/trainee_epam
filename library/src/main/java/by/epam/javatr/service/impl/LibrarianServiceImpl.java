package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.Librarian;
import by.epam.javatr.service.entity_service.LibrarianService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.factory.ValidatorFactory;

import java.util.List;

public class LibrarianServiceImpl implements LibrarianService {
    @Override
    public List<Librarian> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getLibrarianDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public Librarian findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getLibrarianDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}
