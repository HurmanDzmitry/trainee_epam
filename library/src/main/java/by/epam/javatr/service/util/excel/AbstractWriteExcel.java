package by.epam.javatr.service.util.excel;

import by.epam.javatr.entity.Subscriber;
import by.epam.javatr.service.exception.ServiceException;
import org.apache.logging.log4j.web.WebLoggerContextUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

abstract public class AbstractWriteExcel<T> implements WriteExcel<T> {

    protected static final String FORMAT = "dd.MM.yyyy";
    protected static final String EXCEL_PATH_LOCAL = "WEB-INF\\excel\\";
    protected static final String EMPTY_STR = "";
    protected static final String UNDERSCORE = "_";
    protected static final String EXCEL_FORMAT = ".xlsx";
    protected static final int FONT_HEIGHT = 14;

    protected String[] title;
    protected String name;

    public String write(List<T> data, Subscriber subscriber) throws ServiceException {
        XSSFWorkbook workbook = new XSSFWorkbook();

        XSSFSheet sheet = createSheet(workbook);

        int rowCount = 0;
        Row row = sheet.createRow(rowCount);
        writeHeader(row, workbook);

        row = sheet.createRow(++rowCount);
        writeTitle(row, workbook);

        for (T t : data) {
            row = sheet.createRow(++rowCount);
            writeData(t, row, workbook);
        }

        setSheetStyle(sheet);

        String path = generateFilePath(subscriber);
        try (FileOutputStream outputStream = new FileOutputStream(path)) {
            workbook.write(outputStream);
        } catch (IOException e) {
            throw new ServiceException("Excel file wasn't written.", e);
        }
        return path;
    }

    protected String[] getTitle() {
        return title;
    }

    protected String getName() {
        return name;
    }

    protected XSSFSheet createSheet(XSSFWorkbook workbook) {
        return workbook.createSheet(getName());
    }

    protected void writeHeader(Row row, XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(setHeightAndBoldFont(workbook));
        Cell cell;
        cell = row.createCell(1);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(getName().toUpperCase());
        cell = row.createCell(getTitle().length);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(dateFormat(LocalDate.now()));
    }

    protected void writeTitle(Row row, XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        cellStyle.setFont(setHeightAndBoldFont(workbook));
        setAroundBorderStyle(cellStyle);
        Cell cell;
        for (int i = 0; i < getTitle().length; i++) {
            cell = row.createCell(i + 1);
            cell.setCellValue(getTitle()[i]);
            cell.setCellStyle(cellStyle);
        }
    }

    protected abstract void writeData(T delivery, Row row, XSSFWorkbook workbook);

    protected void setSheetStyle(XSSFSheet sheet) {
        for (int i = 1; i < getTitle().length + 1; i++) {
            sheet.autoSizeColumn(i, true);
        }
    }

    protected String generateFilePath(Subscriber subscriber) {
        String folderPath = WebLoggerContextUtils.getServletContext().getRealPath(EMPTY_STR) + EXCEL_PATH_LOCAL;
        File dir = new File(folderPath);
        if (!dir.exists()) {
            dir.mkdir();
        }
        String fileName = subscriber.getSurnameUser() + UNDERSCORE
                + subscriber.getNameUser() + UNDERSCORE
                + dateFormat(LocalDate.now()) + UNDERSCORE
                + getName() + EXCEL_FORMAT;
        return folderPath + fileName;
    }

    protected Font setHeightAndBoldFont(XSSFWorkbook workbook) {
        Font font = workbook.createFont();
        font.setBold(true);
        font.setFontHeightInPoints((short) FONT_HEIGHT);
        return font;
    }

    protected void setAroundBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderBottom(BorderStyle.MEDIUM);
        cellStyle.setBorderTop(BorderStyle.MEDIUM);
        cellStyle.setBorderRight(BorderStyle.MEDIUM);
        cellStyle.setBorderLeft(BorderStyle.MEDIUM);
    }

    protected String dateFormat(LocalDate date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(FORMAT);
        return date.format(formatter);
    }
}