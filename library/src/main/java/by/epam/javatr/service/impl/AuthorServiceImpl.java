package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.Author;
import by.epam.javatr.service.entity_service.AuthorService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.factory.ValidatorFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public class AuthorServiceImpl implements AuthorService {
    @Override
    public InvalidKey save(Author author) throws ServiceException {
        if (author == null) {
            throw new ServiceException("Parameter is null.");
        }
        if (findByName(author.getNameAuthor()) != null) {
            return InvalidKey.BUSY_NAME;
        }
        InvalidKey sik = ValidatorFactory.getInstance().getAuthorValidator().validate(author);
        if (sik != null) {
            return sik;
        }
        try {
            DaoFactory.getInstance().getAuthorDao().save(author);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<Author> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getAuthorDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public Author findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getAuthorDao().findById(id);
        } catch (
                DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }

    }

    @Override
    public List<Author> searchByPartName(String query) throws ServiceException {
        try {
            return DaoFactory.getInstance().getAuthorDao().searchByPartName(query);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public Author findByName(String name) throws ServiceException {
        try {
            return DaoFactory.getInstance().getAuthorDao().findByName(name);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}
