package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.entity_service.DeliveryService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.factory.ValidatorFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public class DeliveryServiceImpl implements DeliveryService {
    @Override
    public InvalidKey save(BookDelivery delivery) throws ServiceException {
        InvalidKey validate = ValidatorFactory.getInstance().getDeliveryValidator().validate(delivery);
        if (validate != null) {
            return validate;
        }
        try {
            DaoFactory.getInstance().getDeliveryDao().save(delivery);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public InvalidKey update(BookDelivery delivery) throws ServiceException {
        InvalidKey validate = ValidatorFactory.getInstance().getDeliveryValidator().validate(delivery);
        if (validate != null) {
            return validate;
        }
        try {
            DaoFactory.getInstance().getDeliveryDao().update(delivery);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookDelivery> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getDeliveryDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public BookDelivery findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getDeliveryDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public void updateAllWhenDelay() throws ServiceException {
        try {
            DaoFactory.getInstance().getDeliveryDao().updateAllWhenDelay();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public int getQuantityBooksIssuedForSubscriber(int idSubscriber) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(idSubscriber) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getDeliveryDao().getQuantityBooksIssuedForSubscriber(idSubscriber);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookDelivery> findAllExpired() throws ServiceException {
        try {
            return DaoFactory.getInstance().getDeliveryDao().findAllExpired();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookDelivery> getAllActiveForSubscriber(int idSubscriber) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(idSubscriber) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getDeliveryDao().getAllActiveForSubscriber(idSubscriber);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookDelivery> findAllForSubscriber(int idSubscriber) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(idSubscriber) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getDeliveryDao().findAllForSubscriber(idSubscriber);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}