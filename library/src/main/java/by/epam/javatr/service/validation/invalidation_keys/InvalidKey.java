package by.epam.javatr.service.validation.invalidation_keys;

public enum InvalidKey {
    BUSY_EMAIL,
    BUSY_LOGIN,
    BUSY_NAME,
    WRONG_SURNAME,
    WRONG_BIRTHDAY,
    WRONG_LOGIN,
    WRONG_PASSWORD,
    WRONG_EMAIL,
    WRONG_NAME,
    WRONG_TITLE,
    WRONG_YEAR,
    WRONG_QUANTITY,
    WRONG_CHOOSING_AUTHOR,
    WRONG_ID,
    WRONG_DATE,
}
