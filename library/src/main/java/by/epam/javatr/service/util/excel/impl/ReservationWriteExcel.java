package by.epam.javatr.service.util.excel.impl;

import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.entity.enumeration.ReservationStatus;
import by.epam.javatr.service.util.excel.AbstractWriteExcel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ReservationWriteExcel extends AbstractWriteExcel<BookReservation> {

    private static final String[] TITLE = {"Книга", "Дата начала", "Дата окончания", "Примечание"};
    private static final String NAME = "резервирования";
    private static final String NOT_DONE = "не использовано";
    private static final String ACTIVE = "активно";

    @Override
    public void writeData(BookReservation reservation, Row row, XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        setAroundBorderStyle(cellStyle);
        int counter = 0;
        Cell cell;
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(reservation.getBook().getTitle());
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(dateFormat(reservation.getDateOfBegin()));
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(dateFormat(reservation.getDateOfReset()));
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(getNote(reservation.getReservationStatus()));
    }

    private String getNote(ReservationStatus reservationStatus) {
        switch (reservationStatus) {
            case IN_ACTION:
                return ACTIVE;
            case NOT_DONE:
                return NOT_DONE;
            default:
                return EMPTY_STR;
        }
    }

    public String[] getTitle() {
        return TITLE;
    }

    public String getName() {
        return NAME;
    }
}
