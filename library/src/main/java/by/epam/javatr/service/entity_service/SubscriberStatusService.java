package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.SubscriberStatus;
import by.epam.javatr.service.exception.ServiceException;

import java.util.List;

public interface SubscriberStatusService {

    SubscriberStatus findByName(String name) throws ServiceException;

    List<SubscriberStatus> findAll() throws ServiceException;
}
