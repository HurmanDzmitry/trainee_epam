package by.epam.javatr.service.validation.impl;

import by.epam.javatr.entity.User;
import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class SubscriberValidatorImpl implements Validator<User> {

    private static final String LOGIN_PATTERN = "^[A-Za-zа-яА-Я][A-Za-zа-яА-Я\\d_]{5,19}$";
    private static final String EMAIL_PATTERN = "^[A-Za-z\\d._%+-]+@[A-Za-z\\d.-]+\\.[A-Za-z]{2,6}$";
    private static final String NAME_PATTERN = "^[A-ZА-Я]([a-zA-Z-]|[а-яА-Я-]){2,19}$";
    private static final int MIN_AGE = 5;
    private static final int MAX_AGE = 100;

    @Override
    public InvalidKey validate(User user) {
        if (!user.getNameUser().matches(NAME_PATTERN)) {
            return InvalidKey.WRONG_NAME;
        }
        if (!user.getSurnameUser().matches(NAME_PATTERN)) {
            return InvalidKey.WRONG_SURNAME;
        }
        try {
            LocalDate birthDay = user.getDateOfBirth();
            if (birthDay.isAfter(LocalDate.now().minusYears(MIN_AGE))
                    || birthDay.isBefore(LocalDate.now().minusYears(MAX_AGE)))
                throw new IllegalArgumentException();
        } catch (DateTimeParseException | IllegalArgumentException e) {
            return InvalidKey.WRONG_BIRTHDAY;
        }
        if (!user.getLogin().matches(LOGIN_PATTERN)) {
            return InvalidKey.WRONG_LOGIN;
        }
        if (!user.getEmail().matches(EMAIL_PATTERN)) {
            return InvalidKey.WRONG_EMAIL;
        }
        return null;
    }
}
