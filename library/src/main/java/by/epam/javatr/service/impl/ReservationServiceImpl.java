package by.epam.javatr.service.impl;

import by.epam.javatr.dao.exception.DaoException;
import by.epam.javatr.dao.factory.DaoFactory;
import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.entity_service.ReservationService;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.factory.ValidatorFactory;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public class ReservationServiceImpl implements ReservationService {
    @Override
    public InvalidKey save(BookReservation bookReservation) throws ServiceException {
        InvalidKey validate = ValidatorFactory.getInstance().getReservationValidator().validate(bookReservation);
        if (validate != null) {
            return validate;
        }
        try {
            DaoFactory.getInstance().getReservationDao().save(bookReservation);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public InvalidKey update(BookReservation reservation) throws ServiceException {
        InvalidKey validate = ValidatorFactory.getInstance().getReservationValidator().validate(reservation);
        if (validate != null) {
            return validate;
        }
        try {
            DaoFactory.getInstance().getReservationDao().update(reservation);
            return null;
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookReservation> findAll() throws ServiceException {
        try {
            return DaoFactory.getInstance().getReservationDao().findAll();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public BookReservation findById(int id) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(id) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getReservationDao().findById(id);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public int getQuantityBooksReservedForSubscriber(int idSubscriber) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(idSubscriber) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getReservationDao().getQuantityBooksReservedForSubscriber(idSubscriber);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookReservation> findAllNotMade() throws ServiceException {
        try {
            return DaoFactory.getInstance().getReservationDao().findAllNotMade();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public void updateAllWhenNotMade() throws ServiceException {
        try {
            DaoFactory.getInstance().getReservationDao().updateAllWhenNotMade();
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookReservation> findAllActiveForSubscriber(int idSubscriber) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(idSubscriber) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getReservationDao().findAllActiveForSubscriber(idSubscriber);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }

    @Override
    public List<BookReservation> findAllForSubscriber(int idSubscriber) throws ServiceException {
        if (ValidatorFactory.getInstance().getIdValidator().validate(idSubscriber) != null) {
            throw new ServiceException("Wrong id.");
        }
        try {
            return DaoFactory.getInstance().getReservationDao().findAllForSubscriber(idSubscriber);
        } catch (DaoException e) {
            throw new ServiceException("Dao layer error.", e);
        }
    }
}