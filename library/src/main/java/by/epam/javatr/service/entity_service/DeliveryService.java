package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.service.exception.ServiceException;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.util.List;

public interface DeliveryService {

    InvalidKey save(BookDelivery entity) throws ServiceException;

    List<BookDelivery> findAll() throws ServiceException;

    BookDelivery findById(int id) throws ServiceException;

    void updateAllWhenDelay() throws ServiceException;

    InvalidKey update(BookDelivery delivery) throws ServiceException;

    int getQuantityBooksIssuedForSubscriber(int idSubscriber) throws ServiceException;

    List<BookDelivery> findAllExpired() throws ServiceException;

    List<BookDelivery> getAllActiveForSubscriber(int idSubscriber) throws ServiceException;

    List<BookDelivery> findAllForSubscriber(int idSubscriber) throws ServiceException;
}
