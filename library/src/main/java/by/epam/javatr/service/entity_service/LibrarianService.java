package by.epam.javatr.service.entity_service;

import by.epam.javatr.entity.Librarian;
import by.epam.javatr.service.exception.ServiceException;

import java.util.List;

public interface LibrarianService {

    List<Librarian> findAll() throws ServiceException;

    Librarian findById(int id) throws ServiceException;
}
