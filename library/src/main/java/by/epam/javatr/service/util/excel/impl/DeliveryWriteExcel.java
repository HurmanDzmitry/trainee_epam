package by.epam.javatr.service.util.excel.impl;

import by.epam.javatr.entity.BookDelivery;
import by.epam.javatr.entity.enumeration.DeliveryPlace;
import by.epam.javatr.entity.enumeration.DeliveryStatus;
import by.epam.javatr.service.util.excel.AbstractWriteExcel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.time.LocalDate;

public class DeliveryWriteExcel extends AbstractWriteExcel<BookDelivery> {

    private static final String[] TITLE = {"Книга", "Дата выдачи", "Дата возврата", "Место", "Примечание"};
    private static final String NAME = "выдачи";
    private static final String HOME = "домой";
    private static final String HERE = "на месте";
    private static final String EXPIRED = "просрочена";
    private static final String NOT_RETURNED = "не сдана";

    @Override
    public void writeData(BookDelivery delivery, Row row, XSSFWorkbook workbook) {
        CellStyle cellStyle = workbook.createCellStyle();
        setAroundBorderStyle(cellStyle);
        int counter = 0;
        Cell cell;
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(delivery.getBook().getTitle());
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(dateFormat(delivery.getDateOfIssue()));
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(getReturnDate(delivery.getDateOfReturnActual()));
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(placeToRussian(delivery.getDeliveryPlace()));
        cell = row.createCell(++counter);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(getNote(delivery.getDeliveryStatus()));
    }

    private String placeToRussian(DeliveryPlace deliveryPlace) {
        switch (deliveryPlace) {
            case HOME:
                return HOME;
            case HERE:
                return HERE;
            default:
                return EMPTY_STR;
        }
    }

    private String getNote(DeliveryStatus deliveryStatus) {
        if (deliveryStatus == DeliveryStatus.EXPIRED) {
            return EXPIRED;
        }
        return EMPTY_STR;
    }

    private String getReturnDate(LocalDate date) {
        if (date != null) {
            return dateFormat(date);
        } else {
            return NOT_RETURNED;
        }
    }

    public String[] getTitle() {
        return TITLE;
    }

    public String getName() {
        return NAME;
    }
}
