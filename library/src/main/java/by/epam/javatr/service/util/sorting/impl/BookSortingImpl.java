package by.epam.javatr.service.util.sorting.impl;

import by.epam.javatr.entity.Book;
import by.epam.javatr.entity.comporator.book.QuantityComparator;
import by.epam.javatr.entity.comporator.book.StatusComparator;
import by.epam.javatr.entity.comporator.book.TitleComparator;
import by.epam.javatr.entity.comporator.book.YearComparator;
import by.epam.javatr.service.util.sorting.Sorting;

import java.util.Collections;
import java.util.List;

public class BookSortingImpl implements Sorting<Book> {

    private static final String YEAR = "year";
    private static final String QUANTITY = "quantity";
    private static final String STATUS = "status";
    private static final String DESC = "desc";

    @Override
    public List<Book> sort(List<Book> books, String... specific) {
        String byWhatSort = specific[0];
        String howSort = specific[1];
        if (byWhatSort == null) {
            books.sort(new TitleComparator().thenComparing(new StatusComparator()));
        } else {
            switch (byWhatSort) {
                case YEAR:
                    books.sort(new YearComparator().thenComparing(new TitleComparator()));
                    break;
                case QUANTITY:
                    books.sort(new QuantityComparator().thenComparing(new TitleComparator()));
                    break;
                case STATUS:
                    books.sort(new StatusComparator().thenComparing(new TitleComparator()));
                    break;
                default:
                    books.sort(new TitleComparator().thenComparing(new StatusComparator()));
            }
        }
        if (howSort != null && howSort.equals(DESC)) {
            Collections.reverse(books);
        }
        return books;
    }
}
