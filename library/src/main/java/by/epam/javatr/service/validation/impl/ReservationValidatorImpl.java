package by.epam.javatr.service.validation.impl;

import by.epam.javatr.entity.BookReservation;
import by.epam.javatr.service.validation.Validator;
import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

import java.time.LocalDate;

public class ReservationValidatorImpl implements Validator<BookReservation> {

    @Override
    public InvalidKey validate(BookReservation bookReservation) {
        if (bookReservation.getDateOfBegin().isAfter(LocalDate.now())
                || bookReservation.getDateOfBegin().isAfter(bookReservation.getDateOfReset())) {
            return InvalidKey.WRONG_DATE;
        }
        return null;
    }
}
