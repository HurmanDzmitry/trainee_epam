package by.epam.javatr.service.util.filter.factory;

import by.epam.javatr.service.util.filter.Filtering;
import by.epam.javatr.service.util.filter.impl.BookFilteringImpl;

public class FilteringFactory {

    private static final FilteringFactory INSTANCE = new FilteringFactory();

    private final Filtering bookFiltering = new BookFilteringImpl();

    private FilteringFactory() {
    }

    public static synchronized FilteringFactory getInstance() {
        return INSTANCE;
    }

    public Filtering getBookFiltering() {
        return bookFiltering;
    }
}
