package by.epam.javatr.service.validation;

import by.epam.javatr.service.validation.invalidation_keys.InvalidKey;

public interface Validator<T> {

    InvalidKey validate(T entity);
}
