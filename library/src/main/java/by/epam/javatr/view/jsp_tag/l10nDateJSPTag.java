package by.epam.javatr.view.jsp_tag;

import by.epam.javatr.controller.command.util.ParameterName;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class l10nDateJSPTag extends TagSupport {

    private static final long serialVersionUID = 7321672390304475076L;

    private static final String RU_PATTERN = "dd.MM.yyyy";
    private static final String US_PATTERN = "M/d/yy";
    private static final String DEFAULT_LOCALE = "ru";

    private LocalDate value;

    public LocalDate getValue() {
        return value;
    }

    public void setValue(LocalDate value) {
        this.value = value;
    }

    @Override
    public int doStartTag() throws JspException {
        String locale;
        try {
            locale = pageContext.getSession().getAttribute(ParameterName.LOCALE).toString();
        } catch (NullPointerException e) {
            locale = DEFAULT_LOCALE;
        }

        String pattern;
        if (locale.equals(DEFAULT_LOCALE)) {
            pattern = RU_PATTERN;
        } else {
            pattern = US_PATTERN;
        }

        DateTimeFormatter deTimeFormatter = DateTimeFormatter.ofPattern(pattern, new Locale(locale));
        JspWriter out = pageContext.getOut();
        try {
            if (getValue() == null) {
                out.write("-");
            } else {
                out.write(getValue().format(deTimeFormatter));
            }
        } catch (IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }
}
