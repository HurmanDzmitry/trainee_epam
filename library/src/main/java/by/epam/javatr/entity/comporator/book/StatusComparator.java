package by.epam.javatr.entity.comporator.book;

import by.epam.javatr.entity.Book;

import java.util.Comparator;

public class StatusComparator implements Comparator<Book> {

    @Override
    public int compare(Book o1, Book o2) {
        return o1.getBookStatus().name().compareToIgnoreCase(o2.getBookStatus().name());
    }
}
