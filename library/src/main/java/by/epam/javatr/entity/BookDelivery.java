package by.epam.javatr.entity;

import by.epam.javatr.entity.enumeration.DeliveryPlace;
import by.epam.javatr.entity.enumeration.DeliveryStatus;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class BookDelivery implements Serializable {

    public static final long serialVersionUID = -8768741947875912678L;

    private int idDelivery;
    private Subscriber subscriber;
    private Librarian librarian;
    private Book book;
    private LocalDate dateOfIssue;
    private LocalDate dateOfReturnActual;
    private LocalDate dateOfReturnExpected;
    private DeliveryStatus deliveryStatus;
    private DeliveryPlace deliveryPlace;

    public BookDelivery() {
    }

    public BookDelivery(Subscriber subscriber, Librarian librarian, Book book,
                        LocalDate dateOfIssue, LocalDate dateOfReturnActual,
                        LocalDate dateOfReturnExpected, DeliveryStatus deliveryStatus,
                        DeliveryPlace deliveryPlace) {
        this.subscriber = subscriber;
        this.librarian = librarian;
        this.book = book;
        this.dateOfIssue = dateOfIssue;
        this.dateOfReturnActual = dateOfReturnActual;
        this.dateOfReturnExpected = dateOfReturnExpected;
        this.deliveryStatus = deliveryStatus;
        this.deliveryPlace = deliveryPlace;
    }

    public BookDelivery(int idDelivery, Subscriber subscriber, Book book,
                        LocalDate dateOfIssue, LocalDate dateOfReturnActual,
                        LocalDate dateOfReturnExpected, DeliveryStatus deliveryStatus,
                        DeliveryPlace deliveryPlace) {
        this.idDelivery = idDelivery;
        this.subscriber = subscriber;
        this.book = book;
        this.dateOfIssue = dateOfIssue;
        this.dateOfReturnActual = dateOfReturnActual;
        this.dateOfReturnExpected = dateOfReturnExpected;
        this.deliveryStatus = deliveryStatus;
        this.deliveryPlace = deliveryPlace;
    }

    public int getIdDelivery() {
        return idDelivery;
    }

    public void setIdDelivery(int idDelivery) {
        this.idDelivery = idDelivery;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public Librarian getLibrarian() {
        return librarian;
    }

    public void setLibrarian(Librarian librarian) {
        this.librarian = librarian;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LocalDate getDateOfIssue() {
        return dateOfIssue;
    }

    public void setDateOfIssue(LocalDate dateOfIssue) {
        this.dateOfIssue = dateOfIssue;
    }

    public LocalDate getDateOfReturnActual() {
        return dateOfReturnActual;
    }

    public void setDateOfReturnActual(LocalDate dateOfReturnActual) {
        this.dateOfReturnActual = dateOfReturnActual;
    }

    public LocalDate getDateOfReturnExpected() {
        return dateOfReturnExpected;
    }

    public void setDateOfReturnExpected(LocalDate dateOfReturnExpected) {
        this.dateOfReturnExpected = dateOfReturnExpected;
    }

    public DeliveryStatus getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public DeliveryPlace getDeliveryPlace() {
        return deliveryPlace;
    }

    public void setDeliveryPlace(DeliveryPlace deliveryPlace) {
        this.deliveryPlace = deliveryPlace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookDelivery that = (BookDelivery) o;
        return idDelivery == that.idDelivery &&
                Objects.equals(subscriber, that.subscriber) &&
                Objects.equals(librarian, that.librarian) &&
                Objects.equals(book, that.book) &&
                Objects.equals(dateOfIssue, that.dateOfIssue) &&
                Objects.equals(dateOfReturnActual, that.dateOfReturnActual) &&
                Objects.equals(dateOfReturnExpected, that.dateOfReturnExpected) &&
                deliveryStatus == that.deliveryStatus &&
                deliveryPlace == that.deliveryPlace;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDelivery, subscriber, librarian, book, dateOfIssue,
                dateOfReturnActual, dateOfReturnExpected, deliveryStatus, deliveryPlace);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "idDelivery=" + idDelivery +
                ", subscriber=" + subscriber +
                ", librarian=" + librarian +
                ", book=" + book +
                ", dateOfIssue=" + dateOfIssue +
                ", dateOfReturnActual=" + dateOfReturnActual +
                ", dateOfReturnExpected=" + dateOfReturnExpected +
                ", deliveryStatus=" + deliveryStatus +
                ", deliveryPlace=" + deliveryPlace +
                '}';
    }
}
