package by.epam.javatr.entity.enumeration;

public enum NameSubscriberStatus {

    USUAL, BLOCKED, LUXURY

}
