package by.epam.javatr.entity;

import by.epam.javatr.entity.enumeration.Role;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Librarian extends User implements Serializable {

    public static final long serialVersionUID = 5449224767635888275L;

    private LocalDate dateOfBeginningWork;
    private double salary;

    public Librarian() {
    }

    public Librarian(String nameUser, String surnameUser, LocalDate dateOfBirth, String login, String password,
                     String email, LocalDate dateOfBeginningWork, double salary) {
        super(nameUser, surnameUser, dateOfBirth, login,
                password, email, Role.LIBRARIAN);
        this.dateOfBeginningWork = dateOfBeginningWork;
        this.salary = salary;
    }

    public Librarian(int id, String nameUser, String surnameUser, LocalDate dateOfBirth, String login, String password,
                     String email, LocalDate dateOfBeginningWork, double salary) {
        super(id, nameUser, surnameUser, dateOfBirth, login,
                password, email, Role.LIBRARIAN);
        this.dateOfBeginningWork = dateOfBeginningWork;
        this.salary = salary;
    }

    public Librarian(User user, LocalDate dateOfBeginningWork, double salary) {
        super(user.getIdUser(), user.getNameUser(), user.getSurnameUser(), user.getDateOfBirth(), user.getLogin(),
                user.getPassword(), user.getEmail(), Role.LIBRARIAN);
        this.dateOfBeginningWork = dateOfBeginningWork;
        this.salary = salary;
    }

    public LocalDate getDateOfBeginningWork() {
        return dateOfBeginningWork;
    }

    public void setDateOfBeginningWork(LocalDate dateOfBeginningWork) {
        this.dateOfBeginningWork = dateOfBeginningWork;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Librarian librarian = (Librarian) o;
        return Double.compare(librarian.salary, salary) == 0 &&
                Objects.equals(dateOfBeginningWork, librarian.dateOfBeginningWork);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), dateOfBeginningWork, salary);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id=" + super.getIdUser() +
                ", name_user='" + super.getNameUser() + '\'' +
                ", surname_user='" + super.getSurnameUser() + '\'' +
                ", date_of_birth=" + super.getDateOfBirth() +
                ", login='" + super.getLogin() + '\'' +
                ", password='" + super.getPassword() + '\'' +
                ", email='" + super.getEmail() + '\'' +
                ", role=" + super.getRole() +
                ", dateOfBeginningWork=" + dateOfBeginningWork +
                ", salary=" + salary +
                '}';
    }
}
