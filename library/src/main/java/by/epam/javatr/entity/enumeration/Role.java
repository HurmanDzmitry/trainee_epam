package by.epam.javatr.entity.enumeration;

public enum Role {

    LIBRARIAN(1), SUBSCRIBER(2);

    private int id;

    Role(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "role=" + Role.values()[id - 1].name() +
                ", id=" + id +
                '}';
    }
}
