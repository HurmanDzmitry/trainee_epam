package by.epam.javatr.entity;

import by.epam.javatr.entity.enumeration.Role;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class User implements Serializable {

    public static final long serialVersionUID = -5788695150804893083L;

    private int idUser;
    private String nameUser;
    private String surnameUser;
    private LocalDate dateOfBirth;
    private String login;
    private String password;
    private String email;
    private Role role;

    public User() {
    }

    public User(String nameUser, String surnameUser, LocalDate dateOfBirth, String login,
                String password, String email, Role role) {
        this.nameUser = nameUser;
        this.surnameUser = surnameUser;
        this.dateOfBirth = dateOfBirth;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public User(int idUser, String nameUser, String surnameUser, LocalDate dateOfBirth,
                String login, String password, String email, Role role) {
        this.idUser = idUser;
        this.nameUser = nameUser;
        this.surnameUser = surnameUser;
        this.dateOfBirth = dateOfBirth;
        this.login = login;
        this.password = password;
        this.email = email;
        this.role = role;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }

    public String getSurnameUser() {
        return surnameUser;
    }

    public void setSurnameUser(String surnameUser) {
        this.surnameUser = surnameUser;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return idUser == user.idUser &&
                Objects.equals(nameUser, user.nameUser) &&
                Objects.equals(surnameUser, user.surnameUser) &&
                Objects.equals(dateOfBirth, user.dateOfBirth) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                Objects.equals(email, user.email) &&
                role == user.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idUser, nameUser, surnameUser, dateOfBirth, login, password,
                email, role);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "idUser=" + idUser +
                ", nameUser='" + nameUser + '\'' +
                ", surnameUser='" + surnameUser + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                ", role=" + role +
                '}';
    }
}
