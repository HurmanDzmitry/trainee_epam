package by.epam.javatr.entity.comporator.book;

import by.epam.javatr.entity.Book;

import java.util.Comparator;

public class QuantityComparator implements Comparator<Book> {

    @Override
    public int compare(Book o1, Book o2) {
        return Integer.compare(o1.getQuantity(), o2.getQuantity());
    }
}
