package by.epam.javatr.entity.enumeration;

public enum ReservationStatus {

    IN_ACTION, PERFORMED, NOT_DONE

}
