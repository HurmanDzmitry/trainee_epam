package by.epam.javatr.entity;

import by.epam.javatr.entity.enumeration.Role;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class Subscriber extends User implements Serializable {

    public static final long serialVersionUID = -1202702682263301707L;

    private SubscriberStatus subscriberStatus;

    public Subscriber() {
    }

    public Subscriber(String nameUser, String surnameUser, LocalDate dateOfBirth, String login,
                      String password, String email, SubscriberStatus subscriberStatus) {
        super(nameUser, surnameUser, dateOfBirth, login,
                password, email, Role.SUBSCRIBER);
        this.subscriberStatus = subscriberStatus;
    }

    public Subscriber(int id, String nameUser, String surnameUser, LocalDate dateOfBirth, String login,
                      String password, String email, SubscriberStatus subscriberStatus) {
        super(id, nameUser, surnameUser, dateOfBirth, login,
                password, email, Role.SUBSCRIBER);
        this.subscriberStatus = subscriberStatus;
    }

    public Subscriber(User user, SubscriberStatus subscriberStatus) {
        super(user.getIdUser(), user.getNameUser(), user.getSurnameUser(), user.getDateOfBirth(), user.getLogin(),
                user.getPassword(), user.getEmail(), Role.SUBSCRIBER);
        this.subscriberStatus = subscriberStatus;
    }

    public SubscriberStatus getSubscriberStatus() {
        return subscriberStatus;
    }

    public void setSubscriberStatus(SubscriberStatus subscriberStatus) {
        this.subscriberStatus = subscriberStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Subscriber subscriber = (Subscriber) o;
        return Objects.equals(subscriberStatus, subscriber.subscriberStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), subscriberStatus);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "id=" + super.getIdUser() +
                ", name_user='" + super.getNameUser() + '\'' +
                ", surname_user='" + super.getSurnameUser() + '\'' +
                ", date_of_birth=" + super.getDateOfBirth() +
                ", login='" + super.getLogin() + '\'' +
                ", password='" + super.getPassword() + '\'' +
                ", email='" + super.getEmail() + '\'' +
                ", role=" + super.getRole() +
                ", subscriberStatus=" + subscriberStatus +
                '}';
    }
}
