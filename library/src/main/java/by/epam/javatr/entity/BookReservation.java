package by.epam.javatr.entity;

import by.epam.javatr.entity.enumeration.ReservationStatus;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

public class BookReservation implements Serializable {

    public static final long serialVersionUID = 3314248410152685265L;

    private int idReservation;
    private Subscriber subscriber;
    private Book book;
    private LocalDate dateOfBegin;
    private LocalDate dateOfReset;
    private ReservationStatus reservationStatus;

    public BookReservation() {
    }

    public BookReservation(Subscriber subscriber, Book book, LocalDate dateOfBegin,
                           LocalDate dateOfReset, ReservationStatus reservationStatus) {
        this.subscriber = subscriber;
        this.book = book;
        this.dateOfBegin = dateOfBegin;
        this.dateOfReset = dateOfReset;
        this.reservationStatus = reservationStatus;
    }

    public BookReservation(int idReservation, Subscriber subscriber, Book book, LocalDate dateOfBegin,
                           LocalDate dateOfReset, ReservationStatus reservationStatus) {
        this.idReservation = idReservation;
        this.subscriber = subscriber;
        this.book = book;
        this.dateOfBegin = dateOfBegin;
        this.dateOfReset = dateOfReset;
        this.reservationStatus = reservationStatus;
    }

    public int getIdReservation() {
        return idReservation;
    }

    public void setIdReservation(int idReservation) {
        this.idReservation = idReservation;
    }

    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public LocalDate getDateOfBegin() {
        return dateOfBegin;
    }

    public void setDateOfBegin(LocalDate dateOfBegin) {
        this.dateOfBegin = dateOfBegin;
    }

    public LocalDate getDateOfReset() {
        return dateOfReset;
    }

    public void setDateOfReset(LocalDate dateOfReset) {
        this.dateOfReset = dateOfReset;
    }

    public ReservationStatus getReservationStatus() {
        return reservationStatus;
    }

    public void setReservationStatus(ReservationStatus reservationStatus) {
        this.reservationStatus = reservationStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BookReservation that = (BookReservation) o;
        return idReservation == that.idReservation &&
                Objects.equals(subscriber, that.subscriber) &&
                Objects.equals(book, that.book) &&
                Objects.equals(dateOfBegin, that.dateOfBegin) &&
                Objects.equals(dateOfReset, that.dateOfReset) &&
                reservationStatus == that.reservationStatus;
    }

    @Override
    public int hashCode() {
        return Objects.hash(idReservation, subscriber, book, dateOfBegin, dateOfReset, reservationStatus);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "idReservation=" + idReservation +
                ", subscriber=" + subscriber +
                ", book=" + book +
                ", dateOfBegin=" + dateOfBegin +
                ", dateOfReset=" + dateOfReset +
                ", reservationStatus=" + reservationStatus +
                '}';
    }
}
