package by.epam.javatr.entity;

import java.io.Serializable;
import java.util.Objects;

public class SubscriberStatus implements Serializable {

    public static final long serialVersionUID = 6504854985729762615L;

    private int idSubscriberStatus;
    private String nameStatus;
    private int limitReservation;
    private int limitTaking;
    private int daysForReservation;
    private int daysForReturn;

    public SubscriberStatus() {
    }

    public SubscriberStatus(String nameStatus, int limitReservation,
                            int limitTaking, int daysForReservation, int daysForReturn) {
        this.nameStatus = nameStatus;
        this.limitReservation = limitReservation;
        this.limitTaking = limitTaking;
        this.daysForReservation = daysForReservation;
        this.daysForReturn = daysForReturn;
    }

    public SubscriberStatus(int idSubscriberStatus, String nameStatus, int limitReservation,
                            int limitTaking, int daysForReservation, int daysForReturn) {
        this.idSubscriberStatus = idSubscriberStatus;
        this.nameStatus = nameStatus;
        this.limitReservation = limitReservation;
        this.limitTaking = limitTaking;
        this.daysForReservation = daysForReservation;
        this.daysForReturn = daysForReturn;
    }

    public int getIdSubscriberStatus() {
        return idSubscriberStatus;
    }

    public void setIdSubscriberStatus(int idSubscriberStatus) {
        this.idSubscriberStatus = idSubscriberStatus;
    }

    public String getNameStatus() {
        return nameStatus;
    }

    public void setNameStatus(String nameStatus) {
        this.nameStatus = nameStatus;
    }

    public int getLimitReservation() {
        return limitReservation;
    }

    public void setLimitReservation(int limitReservation) {
        this.limitReservation = limitReservation;
    }

    public int getLimitTaking() {
        return limitTaking;
    }

    public void setLimitTaking(int limitTaking) {
        this.limitTaking = limitTaking;
    }

    public int getDaysForReservation() {
        return daysForReservation;
    }

    public void setDaysForReservation(int daysForReservation) {
        this.daysForReservation = daysForReservation;
    }

    public int getDaysForReturn() {
        return daysForReturn;
    }

    public void setDaysForReturn(int daysForReturn) {
        this.daysForReturn = daysForReturn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SubscriberStatus that = (SubscriberStatus) o;
        return idSubscriberStatus == that.idSubscriberStatus &&
                limitReservation == that.limitReservation &&
                limitTaking == that.limitTaking &&
                daysForReservation == that.daysForReservation &&
                daysForReturn == that.daysForReturn &&
                Objects.equals(nameStatus, that.nameStatus);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idSubscriberStatus, nameStatus, limitReservation,
                limitTaking, daysForReservation, daysForReturn);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "idSubscriberStatus=" + idSubscriberStatus +
                ", nameStatus='" + nameStatus + '\'' +
                ", limitReservation=" + limitReservation +
                ", limitTaking=" + limitTaking +
                ", daysForReservation=" + daysForReservation +
                ", daysForReturn=" + daysForReturn +
                '}';
    }
}
