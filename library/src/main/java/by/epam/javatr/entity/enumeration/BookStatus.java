package by.epam.javatr.entity.enumeration;

public enum BookStatus {

    ONLY_HERE, EVERYWHERE, NOT_ISSUED

}
