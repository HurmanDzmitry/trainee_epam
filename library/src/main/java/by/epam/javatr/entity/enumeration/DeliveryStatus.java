package by.epam.javatr.entity.enumeration;

public enum DeliveryStatus {

    IN_ACTION, ON_TIME, EXPIRED

}
