package by.epam.javatr.entity;

import by.epam.javatr.entity.enumeration.BookStatus;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Book implements Serializable {

    public static final long serialVersionUID = -8128074322412695836L;

    private int idBook;
    private String title;
    private int year;
    private int quantity;
    private BookStatus bookStatus;
    private String imageLink;
    private List<Author> authors;

    public Book() {
    }

    public Book(String title, int year, int quantity,
                BookStatus bookStatus, String imageLink, List<Author> authors) {
        this.title = title;
        this.year = year;
        this.quantity = quantity;
        this.bookStatus = bookStatus;
        this.imageLink = imageLink;
        this.authors = authors;
    }

    public Book(int idBook, String title, int year, int quantity,
                BookStatus bookStatus, String imageLink, List<Author> authors) {
        this.idBook = idBook;
        this.title = title;
        this.year = year;
        this.quantity = quantity;
        this.bookStatus = bookStatus;
        this.imageLink = imageLink;
        this.authors = authors;
    }

    public int getIdBook() {
        return idBook;
    }

    public void setIdBook(int idBook) {
        this.idBook = idBook;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public BookStatus getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(BookStatus bookStatus) {
        this.bookStatus = bookStatus;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return idBook == book.idBook &&
                year == book.year &&
                quantity == book.quantity &&
                Objects.equals(title, book.title) &&
                bookStatus == book.bookStatus &&
                Objects.equals(imageLink, book.imageLink) &&
                Objects.equals(authors, book.authors);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idBook, title, year, quantity, bookStatus, imageLink, authors);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "idBook=" + idBook +
                ", title='" + title + '\'' +
                ", year=" + year +
                ", quantity=" + quantity +
                ", bookStatus=" + bookStatus +
                ", imageLink='" + imageLink + '\'' +
                ", authors=" + authors +
                '}';
    }
}