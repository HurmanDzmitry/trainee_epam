package by.epam.javatr.entity;

import java.io.Serializable;
import java.util.Objects;

public class Author implements Serializable {

    public static final long serialVersionUID = -2156684887004564368L;

    private int idAuthor;
    private String nameAuthor;

    public Author() {
    }

    public Author(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    public Author(int idAuthor, String nameAuthor) {
        this.idAuthor = idAuthor;
        this.nameAuthor = nameAuthor;
    }

    public int getIdAuthor() {
        return idAuthor;
    }

    public void setIdAuthor(int idAuthor) {
        this.idAuthor = idAuthor;
    }

    public String getNameAuthor() {
        return nameAuthor;
    }

    public void setNameAuthor(String nameAuthor) {
        this.nameAuthor = nameAuthor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Author author = (Author) o;
        return idAuthor == author.idAuthor &&
                Objects.equals(nameAuthor, author.nameAuthor);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idAuthor, nameAuthor);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "idAuthor=" + idAuthor +
                ", nameAuthor='" + nameAuthor + '\'' +
                '}';
    }
}
