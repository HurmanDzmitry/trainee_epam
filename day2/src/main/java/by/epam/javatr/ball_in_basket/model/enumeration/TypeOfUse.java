package by.epam.javatr.ball_in_basket.model.enumeration;

public enum TypeOfUse {
    FOOTBALL("football"),
    BASKETBALL("basketball"),
    BASEBALL("baseball"),
    VOLLEYBALL("volleyball"),
    RUGBY("rugby"),
    UNIDENTIFIED("unidentified");

    private String type;

    TypeOfUse(String type) {
        this.type = type;
    }

    public static TypeOfUse searchType(String typeInput) {
        TypeOfUse type;
        switch (typeInput.toLowerCase()) {
            case "football":
                type = FOOTBALL;
                break;
            case "basketball":
                type = BASKETBALL;
                break;
            case "baseball":
                type = BASEBALL;
                break;
            case "volleyball":
                type = VOLLEYBALL;
                break;
            case "rugby":
                type = RUGBY;
                break;
            default:
                type = UNIDENTIFIED;
        }
        return type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " = " + type + ';';
    }
}
