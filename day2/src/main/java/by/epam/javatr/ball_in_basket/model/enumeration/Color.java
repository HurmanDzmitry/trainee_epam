package by.epam.javatr.ball_in_basket.model.enumeration;

public enum Color {
    RED("red"),
    BLUE("blue"),
    WHITE("white"),
    BLACK("black"),
    PINK("pink"),
    YELLOW("yellow"),
    GREEN("green"),
    BROWN("brown"),
    PURPLE("purple"),
    UNIDENTIFIED("unidentified");

    private String color;

    Color(String color) {
        this.color = color;
    }

    public static Color searchColor(String colorInput) {
        Color color;
        switch (colorInput.toLowerCase()) {
            case "red":
                color = RED;
                break;
            case "blue":
                color = BLUE;
                break;
            case "white":
                color = WHITE;
                break;
            case "black":
                color = BLACK;
                break;
            case "pink":
                color = PINK;
                break;
            case "yellow":
                color = YELLOW;
                break;
            case "green":
                color = GREEN;
                break;
            case "brown":
                color = BROWN;
                break;
            case "purple":
                color = PURPLE;
                break;
            default:
                color = UNIDENTIFIED;
        }
        return color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + " = " + color + ';';
    }
}
