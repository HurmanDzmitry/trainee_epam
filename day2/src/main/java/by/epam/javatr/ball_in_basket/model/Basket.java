package by.epam.javatr.ball_in_basket.model;

import by.epam.javatr.ball_in_basket.check.CheckValid;
import by.epam.javatr.ball_in_basket.exception.NotPositiveNumberException;
import by.epam.javatr.ball_in_basket.exception.NullReferenceException;
import by.epam.javatr.ball_in_basket.exception.OverflowPlaceException;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Basket {

    private final int capacity = 16;

    private List<Ball> balls;
    private int numberBallsInBasket;

    public Basket() {
        balls = new ArrayList<>();
        numberBallsInBasket = 0;
    }

    public Basket(List<Ball> balls) throws NullReferenceException, OverflowPlaceException, NotPositiveNumberException {
        setBalls(balls);
    }

    public boolean add(Ball ball) throws NullReferenceException, OverflowPlaceException {
        CheckValid.checkIsPossibleMore(getNumberBallsInBasket() + 1, capacity);
        CheckValid.checkNotNull(ball);
        numberBallsInBasket++;
        balls.add(ball);
        return true;
    }

    public List<Ball> getBalls() {
        return new ArrayList<>(balls);
    }

    public void setBalls(List<Ball> balls) throws OverflowPlaceException, NullReferenceException, NotPositiveNumberException {
        this.numberBallsInBasket = balls.size();
        CheckValid.checkIsPossibleMore(numberBallsInBasket, capacity);
        for (Ball ball : balls) {
            CheckValid.checkNotNull(ball);
            CheckValid.checkNotNull(ball.getColor());
            CheckValid.checkForPositiveValues(ball.getWeight());
            CheckValid.checkForPositiveValues(ball.getDiameter());
            CheckValid.checkNotNull(ball.getTypeOfUse());
            CheckValid.checkNotNull(ball.isWindblown());
        }
        this.balls = balls;
    }

    public int getNumberBallsInBasket() {
        return numberBallsInBasket;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Basket basket = (Basket) o;
        return numberBallsInBasket == basket.numberBallsInBasket &&
                Objects.equals(balls, basket.balls);
    }

    @Override
    public int hashCode() {
        return Objects.hash(capacity, balls, numberBallsInBasket);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '{' +
                "balls=" + balls +
                '}';
    }
}