package by.epam.javatr.ball_in_basket.scanner;

import java.util.Scanner;

public class InputValues {

    private InputValues() {
    }

    public static final Scanner SCANNER = new Scanner(System.in);
}
