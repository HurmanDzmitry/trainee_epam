package by.epam.javatr.ball_in_basket.service;

import by.epam.javatr.ball_in_basket.model.Ball;
import by.epam.javatr.ball_in_basket.model.Basket;
import by.epam.javatr.ball_in_basket.model.enumeration.Color;

import java.util.List;

public class BasketParameters {

    private List<Ball> balls;

    public BasketParameters() {
    }

    public BasketParameters(Basket basket) {
        this.balls = basket.getBalls();
    }

    public double weightAllBalls() {
        double weight = 0;
        if (!balls.isEmpty())
            for (Ball b : balls) {
                weight += b.getWeight();
            }
        return weight;
    }

    public int quantityBallsByColor(String color) {
        int quantity = 0;
        if (!balls.isEmpty())
            for (Ball b : balls) {
                if (b.getColor().equals(Color.searchColor(color)))
                    quantity++;
            }
        return quantity;
    }
}
