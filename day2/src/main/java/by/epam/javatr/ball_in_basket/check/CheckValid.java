package by.epam.javatr.ball_in_basket.check;

import by.epam.javatr.ball_in_basket.exception.NotPositiveNumberException;
import by.epam.javatr.ball_in_basket.exception.NullReferenceException;
import by.epam.javatr.ball_in_basket.exception.OverflowPlaceException;

public class CheckValid {

    private CheckValid() {
    }

    public static boolean checkForPositiveValues(double value) throws NotPositiveNumberException {
        if (value > 0)
            return true;
        else throw new NotPositiveNumberException("Number is not positive!");
    }

    public static boolean checkNotNull(Object value) throws NullReferenceException {
        if (value != null)
            return true;
        else throw new NullReferenceException("Reference to null!");
    }

    public static boolean checkIsPossibleMore(int size, int capacity) throws OverflowPlaceException {
        if (size <= capacity)
            return true;
        else throw new OverflowPlaceException("Out of space!");
    }

}
