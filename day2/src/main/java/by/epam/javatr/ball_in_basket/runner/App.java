package by.epam.javatr.ball_in_basket.runner;

import by.epam.javatr.ball_in_basket.exception.NotPositiveNumberException;
import by.epam.javatr.ball_in_basket.exception.NullReferenceException;
import by.epam.javatr.ball_in_basket.exception.OverflowPlaceException;


public class App {
    public static void main(String[] args) {
        execute();
    }

    private static void execute() {
        try {
            UserInputOut.inputValues();
            UserInputOut.outputInfo();
        } catch (NullReferenceException | NotPositiveNumberException | OverflowPlaceException e) {
            System.out.println(e.getMessage() + "\nTry again.");
            execute();
        }
    }
}