package by.epam.javatr.ball_in_basket.model;

import by.epam.javatr.ball_in_basket.check.CheckValid;
import by.epam.javatr.ball_in_basket.exception.NotPositiveNumberException;
import by.epam.javatr.ball_in_basket.exception.NullReferenceException;
import by.epam.javatr.ball_in_basket.model.enumeration.Color;
import by.epam.javatr.ball_in_basket.model.enumeration.TypeOfUse;

import java.util.Objects;

public class Ball {

    private Color color;
    private double weight;
    private double diameter;
    private TypeOfUse typeOfUse;
    private boolean isWindblown;

    public Ball() {
    }

    public Ball(Color color, double weight, double diameter, TypeOfUse typeOfUse, boolean isWindblown) throws NotPositiveNumberException, NullReferenceException {
        setColor(color);
        setWeight(weight);
        setDiameter(diameter);
        setTypeOfUse(typeOfUse);
        setWindblown(isWindblown);
    }

    public Ball(String color, double weight, double diameter, String typeOfUse, boolean isWindblown) throws NotPositiveNumberException, NullReferenceException {
        setColor(Color.searchColor(color));
        setWeight(weight);
        setDiameter(diameter);
        setTypeOfUse(TypeOfUse.searchType(typeOfUse));
        setWindblown(isWindblown);
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) throws NullReferenceException {
        CheckValid.checkNotNull(color);
        this.color = color;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) throws NotPositiveNumberException, NullReferenceException {
        CheckValid.checkForPositiveValues(weight);
        CheckValid.checkNotNull(weight);
        this.weight = weight;

    }

    public double getDiameter() {
        return diameter;
    }

    public void setDiameter(double diameter) throws NotPositiveNumberException, NullReferenceException {
        CheckValid.checkForPositiveValues(diameter);
        CheckValid.checkNotNull(diameter);
        this.diameter = diameter;
    }

    public TypeOfUse getTypeOfUse() {
        return typeOfUse;
    }

    public void setTypeOfUse(TypeOfUse typeOfUse) throws NullReferenceException {
        CheckValid.checkNotNull(typeOfUse);
        this.typeOfUse = typeOfUse;
    }

    public boolean isWindblown() {
        return isWindblown;
    }

    public void setWindblown(boolean windblown) throws NullReferenceException {
        CheckValid.checkNotNull(windblown);
        isWindblown = windblown;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ball ball = (Ball) o;
        return Double.compare(ball.weight, weight) == 0 &&
                Double.compare(ball.diameter, diameter) == 0 &&
                isWindblown == ball.isWindblown &&
                color == ball.color &&
                typeOfUse == ball.typeOfUse;
    }

    @Override
    public int hashCode() {
        return Objects.hash(color, weight, diameter, typeOfUse, isWindblown);
    }

    @Override
    public String toString() {
        return '\n' + getClass().getSimpleName() + '{' +
                "color=" + color.getColor() +
                ", weight=" + weight +
                ", diameter=" + diameter +
                ", typeOfUse=" + typeOfUse.getType() +
                ", isWindblown=" + isWindblown +
                '}';
    }
}
