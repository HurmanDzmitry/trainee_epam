package by.epam.javatr.ball_in_basket.exception;

public class NotPositiveNumberException extends Exception {

    public NotPositiveNumberException() {
    }

    public NotPositiveNumberException(String message) {
        super(message);
    }

    public NotPositiveNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotPositiveNumberException(Throwable cause) {
        super(cause);
    }
}
