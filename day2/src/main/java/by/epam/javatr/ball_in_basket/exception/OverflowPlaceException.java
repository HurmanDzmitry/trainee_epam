package by.epam.javatr.ball_in_basket.exception;

public class OverflowPlaceException extends Exception {

    public OverflowPlaceException() {
    }

    public OverflowPlaceException(String message) {
        super(message);
    }

    public OverflowPlaceException(String message, Throwable cause) {
        super(message, cause);
    }

    public OverflowPlaceException(Throwable cause) {
        super(cause);
    }
}
