package by.epam.javatr.ball_in_basket.runner;

import by.epam.javatr.ball_in_basket.exception.NotPositiveNumberException;
import by.epam.javatr.ball_in_basket.exception.NullReferenceException;
import by.epam.javatr.ball_in_basket.exception.OverflowPlaceException;
import by.epam.javatr.ball_in_basket.model.Ball;
import by.epam.javatr.ball_in_basket.model.Basket;
import by.epam.javatr.ball_in_basket.model.enumeration.Color;
import by.epam.javatr.ball_in_basket.scanner.InputValues;
import by.epam.javatr.ball_in_basket.service.BasketParameters;

public class UserInputOut {

    private static Basket basket = new Basket();

    private UserInputOut() {
    }

    public static void inputValues() throws NullReferenceException, NotPositiveNumberException, OverflowPlaceException {
        System.out.println("Enter number balls in basket (1-16 range)");
        int quantityBallsInBasket = InputValues.SCANNER.nextInt();

        if (quantityBallsInBasket > 0) {

            for (int i = 0; i < quantityBallsInBasket; i++) {
                System.out.println("Enter all balls parameters: " +
                        " color(blue,black..); weight; diameter;" +
                        " type(football, rugby..); is windblown?(true or false)");
                basket.add(new Ball(
                        InputValues.SCANNER.next(),
                        InputValues.SCANNER.nextDouble(),
                        InputValues.SCANNER.nextDouble(),
                        InputValues.SCANNER.next(),
                        InputValues.SCANNER.nextBoolean()));
            }
        } else {
            System.out.println("Introduced illegal number");
            inputValues();
        }
    }

    public static void outputInfo() {
        BasketParameters basketParameters = new BasketParameters(basket);
        System.out.println("Enter colors to find out their quantity");
        String color = InputValues.SCANNER.next();
        System.out.println("Quantity " + Color.searchColor(color).getColor() + " balls:\n" +
                basketParameters.quantityBallsByColor(color));
        System.out.printf("Weight all balls:\n" + "%.3f\n",
                basketParameters.weightAllBalls());
        System.out.println(basket.getBalls());
        System.out.println(basket);
    }
}
