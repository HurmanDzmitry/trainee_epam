package by.epam.javatr.tasks.task5.service;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;

public class Maths {

    private Maths() {
    }

    public static int amountNumber(String[] array) throws EmptyArrayException, NumberFormatException {
        CheckValid.checkEmptyArray(array);
        int amountNumber = 0;
        for (String s : array) {
            int i;
            i = Integer.parseInt(s);
            amountNumber += i;
        }
        return amountNumber;
    }

    public static int productNumber(String[] array) throws EmptyArrayException, NumberFormatException {
        CheckValid.checkEmptyArray(array);
        int productNumber = 1;
        for (String s : array) {
            int i;
            i = Integer.parseInt(s);
            productNumber *= i;
        }
        return productNumber;
    }
}