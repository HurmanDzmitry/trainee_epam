package by.epam.javatr.tasks.task7.service;

import by.epam.javatr.tasks.task7.model.Birthday;

import java.time.Duration;
import java.util.Calendar;

public class BirthdayInfo {
    private Calendar dayNow = Calendar.getInstance();
    private Calendar birthday;

    public BirthdayInfo(Birthday birthday) {
        this.dayNow = birthday.getDayNow();
        this.birthday = birthday.getBirthday();
    }

    public String getDayOfWeekBithDay() {
        return getDayOfWeek(birthday.get(Calendar.DAY_OF_WEEK));
    }

    public int getCurrencyAge() {
        Duration duration = Duration.between(birthday.toInstant(), dayNow.toInstant());
        int days = (int) (duration.toDays() - (dayNow.get(Calendar.YEAR) - (birthday.get(Calendar.YEAR))) / 4);
        return days / 365;
    }

    public boolean isTodayBithDay() {
        return birthday.get(Calendar.MONTH) == (dayNow.get(Calendar.MONTH))
                && birthday.get(Calendar.DAY_OF_MONTH) == (dayNow.get(Calendar.DAY_OF_MONTH));
    }

    private static String getDayOfWeek(int day) {
        String dayOfWeek = "";
        switch (day) {
            case 1:
                dayOfWeek = "sunday";
                break;
            case 2:
                dayOfWeek = "monday";
                break;
            case 3:
                dayOfWeek = "tuesday";
                break;
            case 4:
                dayOfWeek = "wednesday";
                break;
            case 5:
                dayOfWeek = "thursday";
                break;
            case 6:
                dayOfWeek = "friday";
                break;
            case 7:
                dayOfWeek = "saturday";
                break;
        }
        return dayOfWeek;
    }
}
