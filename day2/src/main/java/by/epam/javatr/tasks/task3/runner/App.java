package by.epam.javatr.tasks.task3.runner;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.NotPositiveNumberException;
import by.epam.javatr.tasks.task3.service.Array;

import java.util.Random;

public class App {
    public static void main(String[] args) {
        try {
            for (Integer i : Array.setRandomElement(args)) {
                if (i > (new Random().nextInt(10) + 1)) {
                    System.out.println(i + " ");
                } else {
                    System.out.print(i + " ");
                }
            }
        } catch (EmptyArrayException | NotPositiveNumberException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Invalid input format");
            System.out.println(e.getMessage());
        }
    }
}