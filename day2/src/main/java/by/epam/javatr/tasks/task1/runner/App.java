package by.epam.javatr.tasks.task1.runner;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.task1.service.Array;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Hello " + Array.getFirstElement(args) + "!!!");
        } catch (EmptyArrayException e) {
            System.out.println(e.getMessage());
        }
    }
}