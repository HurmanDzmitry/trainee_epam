package by.epam.javatr.tasks.task4.runner;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.task4.service.Security;

public class App {
    public static void main(String[] args) {
        try {
            CheckValid.checkEmptyArray(args);
            for (String actual : args) {
                if (Security.checkPassword(actual))
                    System.out.println("Hello user");
                else
                    System.out.println("Sorry, \"" + actual + "\" is invalid password");
            }
        } catch (EmptyArrayException e) {
            System.out.println(e.getMessage());
        }
    }
}