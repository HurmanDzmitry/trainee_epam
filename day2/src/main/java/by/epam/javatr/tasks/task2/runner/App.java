package by.epam.javatr.tasks.task2.runner;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.task2.service.Array;

public class App {
    public static void main(String[] args) {
        try {
            for (String s : Array.reverseArray(args)) {
                System.out.println(s);
            }
        } catch (EmptyArrayException e) {
            System.out.println(e.getMessage());
        }
    }
}