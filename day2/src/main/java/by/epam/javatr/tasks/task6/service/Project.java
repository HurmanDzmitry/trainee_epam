package by.epam.javatr.tasks.task6.service;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.IllegalValueException;
import by.epam.javatr.tasks.exception.NotPositiveNumberException;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Project {

    private Project() {
    }

    public static String getDateFinish(String[] array) throws EmptyArrayException, NumberFormatException,
            NotPositiveNumberException, IllegalValueException {
        CheckValid.checkEmptyArray(array);
        CheckValid.checkIllegalValues(array.length, 2);
        int daysForWork = Integer.parseInt(array[1]);
        CheckValid.checkForPositiveValues(daysForWork);
        Calendar calendar = Calendar.getInstance();
        Calendar dateEnd = new GregorianCalendar(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH) + daysForWork);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        return simpleDateFormat.format(dateEnd.getTime());
    }
}
