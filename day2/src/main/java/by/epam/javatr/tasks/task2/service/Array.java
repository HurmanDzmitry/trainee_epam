package by.epam.javatr.tasks.task2.service;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;

public class Array {

    private Array() {
    }

    public static String[] reverseArray(String[] array) throws EmptyArrayException {
        CheckValid.checkEmptyArray(array);
        String[] reverseArray = new String[array.length];
        int j = 1;
        for (String s : array) {
            reverseArray[array.length - j] = s;
            ++j;
        }
        return reverseArray;
    }
}
