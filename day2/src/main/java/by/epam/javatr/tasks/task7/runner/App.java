package by.epam.javatr.tasks.task7.runner;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.IllegalValueException;
import by.epam.javatr.tasks.task7.model.Birthday;
import by.epam.javatr.tasks.task7.service.BirthdayInfo;

public class App {
    public static void main(String[] args) {
        try {
            BirthdayInfo birthdayInfo = new BirthdayInfo(new Birthday(args));
            System.out.println("You were born on " + birthdayInfo.getDayOfWeekBithDay());
            System.out.println("You are " + birthdayInfo.getCurrencyAge() + " years old");
            if (birthdayInfo.isTodayBithDay()) {
                System.out.println("Happy birthday!!!");
            }
        } catch (EmptyArrayException | IllegalValueException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Invalid input format");
            System.out.println(e.getMessage());
        }
    }
}
