package by.epam.javatr.tasks.task1.service;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;

public class Array {

    private Array() {
    }

    public static String getFirstElement(String[] array) throws EmptyArrayException {
        CheckValid.checkEmptyArray(array);
        return array[0];
    }
}
