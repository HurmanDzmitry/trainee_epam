package by.epam.javatr.tasks.task5.runner;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.task5.service.Maths;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Amount: " + Maths.amountNumber(args));
            System.out.println("Product: " + Maths.productNumber(args));
        } catch (EmptyArrayException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Invalid input format");
            System.out.println(e.getMessage());
        }
    }
}