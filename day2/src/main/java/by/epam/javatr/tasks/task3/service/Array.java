package by.epam.javatr.tasks.task3.service;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.NotPositiveNumberException;

public class Array {

    Array() {
    }

    public static int[] setRandomElement(String[] array) throws EmptyArrayException, NotPositiveNumberException, NumberFormatException {
        CheckValid.checkEmptyArray(array);
        int quantityNumber = Integer.parseInt(array[0]);
        CheckValid.checkForPositiveValues(quantityNumber);
        int[] randomArray = new int[quantityNumber];
        for (int i = 0; i < quantityNumber; i++) {
            randomArray[i] = (int) (Math.random() * 10 + 1);
        }
        return randomArray;
    }
}
