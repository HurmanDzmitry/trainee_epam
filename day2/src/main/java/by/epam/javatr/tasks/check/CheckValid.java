package by.epam.javatr.tasks.check;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.IllegalValueException;
import by.epam.javatr.tasks.exception.NotPositiveNumberException;

import java.util.Calendar;

public class CheckValid {

    private CheckValid() {
    }

    public static boolean checkForPositiveValues(double value) throws NotPositiveNumberException {
        if (value > 0)
            return true;
        else throw new NotPositiveNumberException("Number is not positive!");
    }

    public static boolean checkEmptyArray(String[] array) throws EmptyArrayException {
        if (array.length > 0)
            return true;
        else throw new EmptyArrayException("Input is empty!");
    }

    public static boolean checkIllegalValues(int border, int values) throws IllegalValueException {
        if (border >= values)
            return true;
        else throw new IllegalValueException("Illegal value, check condition!");
    }
    public static boolean checkIllegalValues(Calendar border, Calendar values) throws IllegalValueException {
        if (border.after(values))
            return true;
        else throw new IllegalValueException("You have not been born yet!?!");
    }
}
