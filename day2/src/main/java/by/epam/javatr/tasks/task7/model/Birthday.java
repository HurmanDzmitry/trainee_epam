package by.epam.javatr.tasks.task7.model;

import by.epam.javatr.tasks.check.CheckValid;
import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.IllegalValueException;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Objects;

public class Birthday {
    private Calendar dayNow = Calendar.getInstance();
    private Calendar birthday;

    public Birthday(String[] array) throws IllegalValueException, EmptyArrayException {
        setBirthday(array);
    }

    public Calendar getDayNow() {
        return dayNow;
    }

    public void setDayNow(Calendar dayNow) {
        this.dayNow = dayNow;
    }

    public Calendar getBirthday() {
        return birthday;
    }

    public void setBirthday(Calendar birthday) {
        this.birthday = birthday;
    }

    public void setBirthday(String[] array) throws EmptyArrayException, NumberFormatException, IllegalValueException {
        CheckValid.checkEmptyArray(array);
        CheckValid.checkIllegalValues(array.length, 3);
        int day;
        int month;
        int year;
        day = Integer.parseInt(array[0]);
        month = Integer.parseInt(array[1]);
        year = Integer.parseInt(array[2]);
        CheckValid.checkIllegalValues(day, 1);
        CheckValid.checkIllegalValues(31, day);
        CheckValid.checkIllegalValues(month, 1);
        CheckValid.checkIllegalValues(12, month);
        CheckValid.checkIllegalValues(year, 1900);
        birthday = new GregorianCalendar(year, month - 1, day);
        CheckValid.checkIllegalValues(dayNow, birthday);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Birthday birthday1 = (Birthday) o;
        return Objects.equals(dayNow, birthday1.dayNow) &&
                Objects.equals(birthday, birthday1.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dayNow, birthday);
    }

    @Override
    public String toString() {
        return "Birthday{" +
                "dayNow=" + dayNow +
                ", birthday=" + birthday +
                '}';
    }
}
