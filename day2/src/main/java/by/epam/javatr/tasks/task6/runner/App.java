package by.epam.javatr.tasks.task6.runner;

import by.epam.javatr.tasks.exception.EmptyArrayException;
import by.epam.javatr.tasks.exception.IllegalValueException;
import by.epam.javatr.tasks.exception.NotPositiveNumberException;
import by.epam.javatr.tasks.task6.service.Project;

public class App {
    public static void main(String[] args) {
        try {
            System.out.println("Name worker: " + args[0]);
            System.out.println("Days for work: " + args[1]);
            System.out.println("Date end work: " + Project.getDateFinish(args));
        } catch (EmptyArrayException | NotPositiveNumberException | IllegalValueException e) {
            System.out.println(e.getMessage());
        } catch (NumberFormatException e) {
            System.out.println("Invalid input format");
            System.out.println(e.getMessage());
        }
    }
}