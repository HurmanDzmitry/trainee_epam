package by.epam.javatr.tasks.task4.service;

public class Security {
    private final static String password = "1234";

    private Security() {
    }

    public static boolean checkPassword(String inputPassword) {
        return inputPassword.equals(password);
    }
}
