package by.epam.javatr.example;

public class Example2 {

	public static void main(String[] args) {
		
		char ch1 = 'a';
		System.out.println("ch1 = \'" + ch1 + "\'");
		System.out.println("��� ������� \'" + ch1 + "\' ����� " + (int)ch1);
		
		char ch2 = 98;
		System.out.println("\nch2 = \'" + ch2 + "\'");
		System.out.println("��� ������� \'" + ch2 + "\' ����� " + (int)ch2);
		
		char ch3 = '\141';
		System.out.println("\nch3 = \'" + ch3 + "\'");
		System.out.println("��� ������� \'" + ch3 + "\' ����� " + (int)ch3);
		
		
	    char ch4 = '\u0062';
	    System.out.println("\nch4 = \'" + ch4 + "\'");
		System.out.println("\ufffd\ufffd\ufffd \ufffd\ufffd\ufffd\ufffd\ufffd\ufffd\ufffd \'" + ch4 + "\' ����� " + (int)ch4);
		
	    ch4 = 0x63;
		System.out.println("\nch4 = \'" + ch4 + "\'");
		System.out.println("��� ������� \'" + ch4 + "\' ����� " + (int)ch4);
		
		 char ch5 = '\b';
		 byte x = (byte)(int)ch5;
		System.out.println("\nch5=" + (char)x + "\'");
	}

}
