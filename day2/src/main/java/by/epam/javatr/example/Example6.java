package by.epam.javatr.example;

public class Example6 {

	public static void main(String[] args) {
		//1.
		/*int x = 5, y;
		
		y = ++x;
		
		System.out.println("x = " + x);
		System.out.println("y = " + y);*/
		
		//2.
		/*int x = 5, y;
		
		y = x++;
		
		System.out.println("x = " + x);
		System.out.println("y = " + y);*/
		
		//3.
		/*int b = 7, n = 1, c;
		
		c = b * ++n;
		
		System.out.println("b = " + b);
		System.out.println("n = " + n);
		System.out.println("c = " + c);*/
		
		//4.
		int b = 7, n = 1, c;
		
		c = b * n++;
		
		System.out.println("b = " + b);
		System.out.println("n = " + n);
		System.out.println("c = " + c);
		
		/*int a = 3, b = 7, x;
		
		x = ++b * a + b * a--;
		
		System.out.print("\na = " + a);
		System.out.print("\tb = " + b);
		System.out.println("\tx = " + x);*/
		
		
		/*int a = 3, b = 7, x;
		
		x = --b * a-- +  ++b * --a ;
		
		System.out.print("\na = " + a);
		System.out.print("\tb = " + b);
		System.out.println("\tx = " + x);*/
	}

}
