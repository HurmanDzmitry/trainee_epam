package by.epam.javatr.example;

public class Example3 {

	public static void main(String[] args) {
		
	
		double javaVar = 45.67;
		double salary = 1.234e3;
		
		System.out.println("javaVar = " + javaVar);
		System.out.println("salary = " + salary);
		
		float itemCost = 445.401f;
		System.out.println("itemCost = " + itemCost);
		
		
		
		float num1 = 45.679897543987738758347834f;
		float num2 = 45.679897543987738758347834f;
		
		System.out.println(num1 == num2);
		System.out.println(Float.floatToIntBits(num1) ==  
				Float.floatToIntBits(num2));
		
	
		
		
	}

}
