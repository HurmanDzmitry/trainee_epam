package by.epam.javatr.example;

public class Example1 {

	public static void main(String[] args) {
		
		int i = 0xFd45;
		
		int itemsSold = 0b1010;
		
		int simple = (int)1_000_000_000_000L;
		
		long k = 42; 
		
		long simpleVar = 1_000_000_000_000L;
		
		System.out.println("i = " + i);
		System.out.println("itemsSold = " + itemsSold);
		System.out.println("simple = " + simple);
		
		System.out.println("k = " + k);
		System.out.println("simpleVar = " + simpleVar);
	}
}















