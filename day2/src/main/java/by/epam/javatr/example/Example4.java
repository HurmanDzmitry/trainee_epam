package by.epam.javatr.example;

public class Example4 {

	public static void main(String[] args) {
		//1.
		/*byte b1 = 50, b2 = 20, b3 = 127;
		
		byte b4 = (byte)( b1 * b2 * b3);		
		System.out.println("b4 = " + b4);
		
		int x2 = b1 * b2 * b3;
		System.out.println("x2 = " + x2);*/

		//2.
	
		/*int x1 = 123456789;
		int x2 = 99999999;
		
		float f1 = x1;
		float f2 = x2;
		
		System.out.println("f1 = " + f1);
		System.out.println("f2 = " + f2);*/
		
		//3.
		
		/*int x1 = 1234567890;
		float f1 = x1;
		double d1 = x1;
		
		System.out.println("f1 = " + f1);
		System.out.println("d1 = " + d1);*/
		
		//4.
		
		/*long l1 = 123_456_789_000L;
		float f2 = l1;
		double d2 = l1;
		
		System.out.println("f2 = " + f2);
		System.out.println("d2 = " + d2);*/
		
		//5.
		/*long longVar = 1_000_000_000_000L;
		int intVar = longVar;
		
		System.out.println("longVar = " + longVar);
		System.out.println("intVar = " + intVar);*/
		
		//6.
		
		/*int intVar = 100;
		long longVar = 1_000_000_000_000L;
		
		intVar += longVar;
		System.out.println(intVar);
		*/
		
		
		/*int intVar = 456789;
		short shVar = (short)intVar;
		
		System.out.println("intVar = " + intVar);
		System.out.println("shVar = " + shVar);*/
		
		int intVar = 100;
		long longVar = 1000000000000L;
				
		intVar += longVar;
		System.out.println("intVar = " + intVar);
		System.out.println("longVar = " + longVar);
		
		/*int x = 5, y = 0;
		
		double z = (double)x / y;
		
		System.out.println("z = " + z);*/
	}
}

